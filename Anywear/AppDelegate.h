//
//  AppDelegate.h
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{

    ViewController *viewobj;
    UINavigationController *navobj;
    UIWindow *window;
    
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navobj;
@property (strong, nonatomic) ViewController *viewobj;

@end
