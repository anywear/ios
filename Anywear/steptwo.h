//
//  steptwo.h
//  Anywear
//
//  Created by Pradip on 8/13/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "stepthree.h"

@class stepthree;

@interface steptwo : UIViewController
{
    int i;
    int j;
    
    //top
    UIButton *btns;
    UIButton *btnm;
    UIButton *btnl;
    UIButton *btnxl;
    UIButton *btnxxl;
    
    UIButton *btnspink;
    UIButton *btnmpink;
    UIButton *btnlpink;
    UIButton *btnxlpink;
    UIButton *btnxxlpink;
    
    //bottom
    UIButton *btns1;
    UIButton *btnm1;
    UIButton *btnl1;
    UIButton *btnxl1;
    UIButton *btnxxl1;
    
    UIButton *btnspink1;
    UIButton *btnmpink1;
    UIButton *btnlpink1;
    UIButton *btnxlpink1;
    UIButton *btnxxlpink1;

    stepthree *stthtreeobj;
    UIView *smilyobj;
    
    
    
    //New
    NSMutableArray *store;
    NSMutableArray *store1;
    
    
    NSMutableArray *topsize;
    NSMutableArray *bottomsize;
    
    NSString *value11;
    
    
    UILabel *fid;
    UILabel *sid;
    UILabel *tid;
    UILabel *foid;
    UILabel *fiid;
    
    UILabel *bfid;
    UILabel *bsid;
    UILabel *btid;
    UILabel *bfoid;
    UILabel *bfiid;
    
    
    int k;
    int m;
    
    
}

@property ( nonatomic) int k;
@property ( nonatomic) int m;



@property (strong, nonatomic) IBOutlet  UILabel *fid;
@property (strong, nonatomic) IBOutlet  UILabel *sid;
@property (strong, nonatomic) IBOutlet  UILabel *tid;
@property (strong, nonatomic) IBOutlet  UILabel *foid;
@property (strong, nonatomic) IBOutlet  UILabel *fiid;


@property (strong, nonatomic) IBOutlet  UILabel *bfid;
@property (strong, nonatomic) IBOutlet  UILabel *bsid;
@property (strong, nonatomic) IBOutlet  UILabel *btid;
@property (strong, nonatomic) IBOutlet  UILabel *bfoid;
@property (strong, nonatomic) IBOutlet  UILabel *bfiid;



@property (strong, nonatomic) NSString *value11;
@property (strong, nonatomic) stepthree *stthtreeobj;
@property (strong, nonatomic) IBOutlet UIView *popUpView;
@property (strong, nonatomic) IBOutlet UIView *popUpView1;
@property (strong, nonatomic) IBOutlet UIView *smilyobj;
@property ( nonatomic) int i;
@property ( nonatomic) int j;


//top
@property (strong, nonatomic) IBOutlet UIButton *btns;
@property (strong, nonatomic) IBOutlet UIButton *btnm;
@property (strong, nonatomic) IBOutlet UIButton *btnl;
@property (strong, nonatomic) IBOutlet UIButton *btnxl;
@property (strong, nonatomic) IBOutlet UIButton *btnxxl;

@property (strong, nonatomic) IBOutlet UIButton *btnspink;
@property (strong, nonatomic) IBOutlet UIButton *btnmpink;
@property (strong, nonatomic) IBOutlet UIButton *btnlpink;
@property (strong, nonatomic) IBOutlet UIButton *btnxlpink;
@property (strong, nonatomic) IBOutlet UIButton *btnxxlpink;

//bottom
@property (strong, nonatomic) IBOutlet UIButton *btns1;
@property (strong, nonatomic) IBOutlet UIButton *btnm1;
@property (strong, nonatomic) IBOutlet UIButton *btnl1;
@property (strong, nonatomic) IBOutlet UIButton *btnxl1;
@property (strong, nonatomic) IBOutlet UIButton *btnxxl1;

@property (strong, nonatomic) IBOutlet UIButton *btnspink1;
@property (strong, nonatomic) IBOutlet UIButton *btnmpink1;
@property (strong, nonatomic) IBOutlet UIButton *btnlpink1;
@property (strong, nonatomic) IBOutlet UIButton *btnxlpink1;
@property (strong, nonatomic) IBOutlet UIButton *btnxxlpink1;


-(IBAction)help:(id)sender;
-(IBAction)help1:(id)sender;


//New
@property (strong, nonatomic) NSMutableArray *store;
@property (strong, nonatomic) NSMutableArray *store1;


@property (strong, nonatomic) NSMutableArray *topsize;
@property (strong, nonatomic) NSMutableArray *bottomsize;


//top

-(IBAction)s:(id)sender;
-(IBAction)m:(id)sender;
-(IBAction)l:(id)sender;
-(IBAction)xl:(id)sender;
-(IBAction)xxl:(id)sender;

-(IBAction)spink:(id)sender;
-(IBAction)mpink:(id)sender;
-(IBAction)lpink:(id)sender;
-(IBAction)xlpink:(id)sender;
-(IBAction)xxlpink:(id)sender;


//bottom

-(IBAction)s1:(id)sender;
-(IBAction)m1:(id)sender;
-(IBAction)l1:(id)sender;
-(IBAction)xl1:(id)sender;
-(IBAction)xxl1:(id)sender;

-(IBAction)spink1:(id)sender;
-(IBAction)mpink1:(id)sender;
-(IBAction)lpink1:(id)sender;
-(IBAction)xlpink1:(id)sender;
-(IBAction)xxlpink1:(id)sender;

-(IBAction)btnback:(id)sender;
-(IBAction)next:(id)sender;
-(IBAction)ok:(id)sender;





@end
