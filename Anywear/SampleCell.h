//
//  MVYTableViewCell.h
//  MVYSideMenuExample
//
//  Created by Pradip on 6/3/14.
//  Copyright (c) 2014 Mobivery. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SampleCell : UITableViewCell
{
   
}
@property (nonatomic,strong) IBOutlet UILabel *labelForPlace;
@property (nonatomic,strong) IBOutlet UIButton *btnnext;
@property (nonatomic,strong) IBOutlet UIImageView *imgicon;
@end
