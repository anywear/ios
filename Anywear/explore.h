//
//  explore.h
//  Anywear
//
//  Created by Pradip on 9/4/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "exploreinfo.h"
#import "mainpage.h"
#import "ALScrollViewPaging.h"
#import "myprofile.h"
#import "style.h"


@class mainpage;
@class style;

@interface explore : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
   
    UIImageView *img;
    UIImageView *imgbac;
    UIImageView *imgmapback;
    
    UIView *viewObj;
    
    UIButton *btngalary;
    UIButton *btngalary1;
    
    NSMutableArray *arrData;
    UITableView *tblobj;
    
    UIButton *start;
    
    
    exploreinfo *explobj;
    mainpage *mainobj;
    UIScrollView *scrollobj;
    
    
    UIImageView *imagephoto;
    NSMutableArray *image1;
    NSMutableArray *image2;
    
  //  mainsignup *signupexploreobj;
    
    int tag;
    
    
    UITableView * storetabble;
    NSMutableArray *arraystore;
    
    NSArray *thumbnails;
    
    UIImageView *imgpic;
    
    myprofile *mypro;
    style *styobj;
    
    
}

@property (nonatomic,strong)  style *styobj;

@property (nonatomic,strong)  myprofile *mypro;

@property (nonatomic,strong) NSArray *thumbnails;
@property (nonatomic,strong) NSMutableArray *arraystore;
@property (nonatomic,strong) IBOutlet  UITableView * storetabble;

@property (nonatomic,strong) NSMutableArray *image1;
@property (nonatomic,strong) NSMutableArray *image2;

@property (nonatomic,strong) IBOutlet UIImageView *imagephoto;
@property (nonatomic,strong) IBOutlet UIImageView *imgpic;


@property(nonatomic)int tag;

@property (nonatomic,strong) IBOutlet UIScrollView *scrollobj;

@property(nonatomic,strong)IBOutlet UIImageView *img;
@property(nonatomic,strong)IBOutlet UIImageView *imgbac;
@property(nonatomic,strong)IBOutlet UIImageView *imgmapback;
@property (nonatomic,strong)IBOutlet UIView *viewObj;
@property (nonatomic,strong)IBOutlet UIButton *btngalary;
@property (nonatomic,strong)IBOutlet UIButton *btngalary1;

@property (nonatomic,strong)IBOutlet UIButton *start;
@property (nonatomic,strong)IBOutlet UIButton *start1;


@property(strong,nonatomic) IBOutlet UITableView *tblobj;
@property (nonatomic,strong)NSMutableArray *arrData;
@property (nonatomic,strong) exploreinfo *explobj;
@property (nonatomic,strong)  mainpage *mainobj;


-(IBAction)btninfo:(id)sender;
-(IBAction)btngalaryclick:(id)sender;
-(IBAction)btngalary1click:(id)sender;


-(IBAction)start:(id)sender;



@end
