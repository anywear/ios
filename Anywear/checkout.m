//
//  checkout.m
//  Anywear
//
//  Created by Pradip on 8/30/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "checkout.h"

@interface checkout ()

@end

@implementation checkout
@synthesize thnsobj;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    
    thnsobj=[[thankyou alloc]initWithNibName:@"thankyou" bundle:nil];
    }
    else{
    
     thnsobj=[[thankyou alloc]initWithNibName:@"thankyouiphone" bundle:nil];
    }
    
    
}
-(IBAction)pay:(id)sender
{
[self.navigationController pushViewController:thnsobj animated:YES];

}
-(IBAction)back:(id)sender
{
[self.navigationController popViewControllerAnimated:YES];

}

-(IBAction)paypal:(id)sender
{
[self.navigationController pushViewController:thnsobj animated:YES];

}
-(IBAction)cell:(id)sender
{
[self.navigationController pushViewController:thnsobj animated:YES];

}

-(IBAction)visa:(id)sender
{
    
[self.navigationController pushViewController:thnsobj animated:YES];

}
-(IBAction)secure:(id)sender
{
[self.navigationController pushViewController:thnsobj animated:YES];
}
-(IBAction)master:(id)sender
{
[self.navigationController pushViewController:thnsobj animated:YES];

}
-(IBAction)google:(id)sender
{
[self.navigationController pushViewController:thnsobj animated:YES];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
