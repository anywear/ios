//
//  shopping.h
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "shoppingone.h"

@class shoppingone;

@interface shopping : UIViewController
{

    UITableView *tblobj;
    NSMutableArray *arrdata;
    shoppingone *shopingoneobj;
    

    BOOL isSelectAllBtnClicked;
    int tempFlag;
    
    UIImageView *aImageView;
    
    UIButton *btn;
    UIButton *btn1;
    
    UILabel *loc;
    
    NSMutableArray *locationarray;
    NSMutableArray *locationarray1;
    
    NSString *address;

    NSString *allobj;
    UIImageView *imgView;
    
    int arrtag1;
    NSString *strall;
    int tag;
    
    BOOL isActivDeactivButton;
    
}

@property (nonatomic) int tag;

@property(nonatomic,strong)   NSString *strall;

@property (nonatomic) int arrtag1;

@property(nonatomic,strong)   UIImageView *imgView;

@property(nonatomic,strong) NSString *address;
@property(nonatomic,strong) NSString *allobj;


@property(nonatomic,strong)   UIButton *btn;
@property(nonatomic,strong)   UIButton *btn1;

@property(nonatomic,strong)   IBOutlet  UILabel *loc;

@property(nonatomic,strong) UIImageView *aImageView;
@property(nonatomic) BOOL isSelectAllBtnClicked;
@property(nonatomic)  int tempFlag;
@property(nonatomic,strong) IBOutlet UITableView *tblobj;
@property(nonatomic,strong)  NSMutableArray *arrdata;
@property(nonatomic,strong)   shoppingone *shopingoneobj;
@property(nonatomic,strong) NSMutableArray *locationarray;
@property(nonatomic,strong) NSMutableArray *locationarray1;




-(IBAction)next:(id)sender;
-(IBAction)back:(id)sender;



@end
