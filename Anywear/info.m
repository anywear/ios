//
//  info.m
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "info.h"
#import "infocell.h"
#import <FacebookSDK/FacebookSDK.h>

@interface info ()

@end

@implementation info
@synthesize tblinfo,infoarray,thumbnails,aboutobj,faqobj,privacyobj,termsobj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (g_IS_IPHONE_4_SCREEN)
    {

    thumbnails = [NSArray arrayWithObjects:@"info.png",@"note.png",@"comment.png",@"book.png",@"mail.png",@"star.png",@"pen.png",@"pen-1.png",  nil];
    
    infoarray =[[NSMutableArray alloc] init];
    [infoarray addObject:@"BOUTIQALL"];
    [infoarray addObject:@"CONTACT US"];
    [infoarray addObject:@"FAQ"];
    [infoarray addObject:@"HELP IMPROVE THE APP"];
    [infoarray addObject:@"SHARE SPREE"];
    [infoarray addObject:@"RATE THE APP"];
    [infoarray addObject:@"PRIVACY POLICY"];
    [infoarray addObject:@"TERMS OF SERVICE"];
    }
    else{
    
        thumbnails = [NSArray arrayWithObjects:@"info.png",@"note.png",@"comment.png",@"book.png",@"mail.png",@"star.png",@"pen.png",@"pen-1.png",  nil];
        
        infoarray =[[NSMutableArray alloc] init];
        [infoarray addObject:@"BOUTIQALL"];
        [infoarray addObject:@"CONTACT US"];
        [infoarray addObject:@"FAQ"];
        [infoarray addObject:@"HELP IMPROVE THE APP"];
        [infoarray addObject:@"SHARE SPREE"];
        [infoarray addObject:@"RATE THE APP"];
        [infoarray addObject:@"PRIVACY POLICY"];
        [infoarray addObject:@"TERMS OF SERVICE"];
    
    
    }
    
   }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [infoarray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifre =@"infocell";
    
    infocell *cell =(infocell *) [self.tblinfo dequeueReusableCellWithIdentifier:cellIdentifre];
    
    if (cell==nil) {
        NSArray *arrNib=[[NSBundle mainBundle] loadNibNamed:cellIdentifre owner:self options:nil];
        cell= (infocell *)[arrNib objectAtIndex:0];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        cell.Choose.tag=indexPath.row;
        
        [cell.Choose addTarget:self action:@selector(btnDetailClick:) forControlEvents:UIControlEventTouchUpInside];

        
    }
    cell.imgicon.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
    
    cell.labelForPlace.text=[infoarray objectAtIndex:indexPath.row];
    
 return cell;
}

-(IBAction)btnDetailClick:(id)sender
{
    
    
    
    UIButton *btnTemp =(UIButton *)sender;
    
    NSLog(@"tag=%d",btnTemp.tag);
    
    if(btnTemp.tag==0)
    {
        aboutobj.hidden=NO;
        
        
        [UIView animateWithDuration:0.0f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^
         {
             aboutobj.frame = CGRectMake(aboutobj.frame.origin.x,0, aboutobj.bounds.size.width, aboutobj.bounds.size.height);
         }completion:^(BOOL finished)
         {
             NSLog(@"Animation is complete");
             [self.view addSubview:aboutobj];
         }];


    }
    else if (btnTemp.tag==1)
    {
    
        if(![MFMessageComposeViewController canSendText]) {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            return;
        }
        if([MFMessageComposeViewController canSendText])
        {
            NSArray *recipents = @[@"", @""];
            NSString *message = [NSString stringWithFormat:@"%@",@""];
            
            MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
            messageController.messageComposeDelegate = self;
            [messageController setRecipients:recipents];
            [messageController setBody:message];
            
            [self presentViewController:messageController animated:YES completion:nil];
        }

    
    }
    else if (btnTemp.tag==2)
    {
        faqobj.hidden=NO;
        [UIView animateWithDuration:0.0f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^
         {
             faqobj.frame = CGRectMake(faqobj.frame.origin.x,0, faqobj.bounds.size.width, faqobj.bounds.size.height);
         }completion:^(BOOL finished)
         {
             NSLog(@"Animation is complete");
             [self.view addSubview:faqobj];
         }];

        
    }
    else if (btnTemp.tag==3)
    {
        if(![MFMessageComposeViewController canSendText]) {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            return;
        }
        if([MFMessageComposeViewController canSendText])
        {
            NSArray *recipents = @[@"", @""];
            NSString *message = [NSString stringWithFormat:@"%@",@""];
            
            MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
            messageController.messageComposeDelegate = self;
            [messageController setRecipients:recipents];
            [messageController setBody:message];
            
            [self presentViewController:messageController animated:YES completion:nil];
        }

        
    }
    else if (btnTemp.tag==4)
    {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        SLComposeViewControllerCompletionHandler myBlock =
        ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled)
            {
                NSLog(@"Cancelled");
            }
            else
            {
                NSLog(@"Done");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Send Status" message:@"Your Status Has Been Sent Successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }
            [controller dismissViewControllerAnimated:YES completion:nil];
        };
        controller.completionHandler =myBlock;
        
        [controller setInitialText:@""];
       
        [controller addImage:[UIImage imageNamed:@"icon_100x100.png"]];
        [self presentViewController:controller animated:YES completion:nil];

        
    }
    else if (btnTemp.tag==5)
    {
        
        
    }
    else if (btnTemp.tag==6)
    {
         privacyobj.hidden=NO;
        
        [UIView animateWithDuration:0.0f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^
         {
             privacyobj.frame = CGRectMake(privacyobj.frame.origin.x,0, privacyobj.bounds.size.width, privacyobj.bounds.size.height);
         }completion:^(BOOL finished)
         {
             NSLog(@"Animation is complete");
             [self.view addSubview:privacyobj];
         }];
        
    }
    else if (btnTemp.tag==7)
    {
          termsobj.hidden=NO;
        [UIView animateWithDuration:0.0f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^
         {
             termsobj.frame = CGRectMake(termsobj.frame.origin.x,0, termsobj.bounds.size.width, termsobj.bounds.size.height);
         }completion:^(BOOL finished)
         {
             NSLog(@"Animation is complete");
             [self.view addSubview:termsobj];
         }];

        
    }


    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Message was cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];             break;
        case MessageComposeResultFailed:
            NSLog(@"Message failed");
            [self dismissViewControllerAnimated:YES completion:NULL];             break;
        case MessageComposeResultSent:
            NSLog(@"Message was sent");
            [self dismissViewControllerAnimated:YES completion:NULL];             break;
        default:
            break;
    }
}
-(void)sendSMS:(NSString *)bodyOfMessage recipientList:(NSArray *)recipients {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
    if([MFMessageComposeViewController canSendText]) {
        
        controller.body = bodyOfMessage;
        controller.recipients = recipients;
        controller.messageComposeDelegate = self;
        //        [self presentModalViewController:controller animated:YES];
        [self presentViewController:controller animated:YES completion:nil];
    }
}


- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    if (result)
    {
        NSLog(@"Result : %d",result);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Send Mail" message:@"Your Mail Has Been Sent Successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    if (error)
    {
        NSLog(@"Error : %@",error);
    }
    [self dismissModalViewControllerAnimated:YES];
}


-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}
-(IBAction)abtback:(id)sender
{
    aboutobj.hidden=YES;
    
}

-(IBAction)faqback:(id)sender
{
    faqobj.hidden=YES;
}

-(IBAction)privacyback:(id)sender
{
    privacyobj.hidden=YES;
    
}
-(IBAction)termsback:(id)sender
{
    termsobj.hidden=YES;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
