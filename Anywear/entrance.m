//
//  entrance.m
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "entrance.h"

@interface entrance ()

@end

@implementation entrance
@synthesize bothscroll;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    
    [bothscroll setScrollEnabled:YES];
    [bothscroll setContentSize:CGSizeMake(320,600)];
    }
    else{
    
        [bothscroll setScrollEnabled:YES];
        [bothscroll setContentSize:CGSizeMake(320,600)];
    
    }

    // Do any additional setup after loading the view from its nib.
}
-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
