//
//  Aboutshop.m
//  Anywear
//
//  Created by Pradip on 8/27/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "Aboutshop.h"

@interface Aboutshop ()

@end

@implementation Aboutshop
@synthesize scrollobj,itemsobj,desobj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    
    [scrollobj setScrollEnabled:YES];
    [scrollobj setContentSize:CGSizeMake(320,1375)];
    
    itemsobj=[[items alloc]initWithNibName:@"items" bundle:nil];
    desobj=[[description alloc]initWithNibName:@"description" bundle:nil];
    }
    else{
    
        [scrollobj setScrollEnabled:YES];
        [scrollobj setContentSize:CGSizeMake(320,1475)];
        
        itemsobj=[[items alloc]initWithNibName:@"itemsiphone" bundle:nil];
        desobj=[[description alloc]initWithNibName:@"descriptioniphone" bundle:nil];
    
    
    }

    // Do any additional setup after loading the view from its nib.
}

-(IBAction)mapbtn:(id)sender
{
[self.navigationController pushViewController:desobj animated:YES];

}
-(IBAction)onebtn:(id)sender
{
[self.navigationController pushViewController:itemsobj animated:YES];
}

-(IBAction)twobtn:(id)sender
{
[self.navigationController pushViewController:itemsobj animated:YES];

}
-(IBAction)threebtn:(id)sender
{
[self.navigationController pushViewController:itemsobj animated:YES];

}
-(IBAction)fourbtn:(id)sender
{
    
[self.navigationController pushViewController:itemsobj animated:YES];
}
-(IBAction)fivebtn:(id)sender
{
[self.navigationController pushViewController:itemsobj animated:YES];

}
-(IBAction)sixbtn:(id)sender
{

[self.navigationController pushViewController:itemsobj animated:YES];
}
-(IBAction)sevenbtn:(id)sender
{

[self.navigationController pushViewController:itemsobj animated:YES];
}
-(IBAction)eightbtn:(id)sender
{
[self.navigationController pushViewController:itemsobj animated:YES];

}
-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
