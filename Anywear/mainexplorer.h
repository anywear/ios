//
//  mainexplorer.h
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "exploreinfo.h"
//#import "mainsignup.h"
@interface mainexplorer : UIViewController
{

    exploreinfo *obj;
    
    UIImageView *img;
    UIImageView *imgbac;
    UIImageView *imgmapback;
    
    UIView *viewObj;
    
    UIButton *btngalary;
    UIButton *btngalary1;
    
    NSMutableArray *arrData;
    UITableView *tblobj;
    
    UIButton *start;
    UIButton *start1;
    
    
    //mainsignup *signupexploreobj;
    
    int tag;


}
@property(nonatomic)int tag;
@property(nonatomic,strong)IBOutlet UIImageView *img;
@property(nonatomic,strong)IBOutlet UIImageView *imgbac;
@property(nonatomic,strong)IBOutlet UIImageView *imgmapback;
@property (nonatomic,strong)IBOutlet UIView *viewObj;
@property (nonatomic,strong)IBOutlet UIButton *btngalary;
@property (nonatomic,strong)IBOutlet UIButton *btngalary1;

@property (nonatomic,strong)IBOutlet UIButton *start;
@property (nonatomic,strong)IBOutlet UIButton *start1;


@property(strong,nonatomic) IBOutlet UITableView *tblobj;
@property (nonatomic,strong)NSMutableArray *arrData;
//@property (nonatomic,strong) mainsignup *signupexploreobj;
@property (nonatomic,strong) exploreinfo *obj;

-(IBAction)btninfo:(id)sender;
-(IBAction)btngalaryclick:(id)sender;
-(IBAction)btngalary1click:(id)sender;


-(IBAction)start:(id)sender;
-(IBAction)start1:(id)sender;


@end
