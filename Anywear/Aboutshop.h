//
//  Aboutshop.h
//  Anywear
//
//  Created by Pradip on 8/27/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "items.h"
#import "description.h"

@interface Aboutshop : UIViewController
{

    UIScrollView *scrollobj;
    items *itemsobj;
    description *desobj;
    
}

@property(nonatomic,strong) IBOutlet UIScrollView *scrollobj;
@property(nonatomic,strong) items *itemsobj;
@property(nonatomic,strong)  description *desobj;


-(IBAction)onebtn:(id)sender;
-(IBAction)twobtn:(id)sender;
-(IBAction)threebtn:(id)sender;
-(IBAction)fourbtn:(id)sender;
-(IBAction)fivebtn:(id)sender;
-(IBAction)sixbtn:(id)sender;
-(IBAction)sevenbtn:(id)sender;
-(IBAction)eightbtn:(id)sender;
-(IBAction)back:(id)sender;
-(IBAction)mapbtn:(id)sender;


@end
