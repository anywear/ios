//
//  steptwo.m
//  Anywear
//
//  Created by Pradip on 8/13/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "steptwo.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"

@interface steptwo ()

@end

@implementation steptwo
@synthesize btns,btnxl,btnl,btnlpink,btnm,btnmpink,btnspink,btnxlpink,btnxxl,btnxxlpink,i,btnl1,btnlpink1,btnm1,btnmpink1,btns1,btnspink1,btnxl1,btnxlpink1,btnxxl1,btnxxlpink1,j,popUpView1,stthtreeobj,smilyobj,store,store1,topsize,bottomsize,value11,fid,sid,tid,foid,fiid,bfid,bsid,btid,bfoid,bfiid,k,m;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    
    fid.hidden=YES;
    sid.hidden=YES;
    tid.hidden=YES;
    foid.hidden=YES;
    fiid.hidden=YES;
    
    bfid.hidden=YES;
    bsid.hidden=YES;
    btid.hidden=YES;
    bfoid.hidden=YES;
    bfiid.hidden=YES;
    
    
    store=[[NSMutableArray alloc]init];
    store1=[[NSMutableArray alloc]init];
    
    topsize=[[NSMutableArray alloc]init];
    bottomsize=[[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"3" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"3" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-3" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-3" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"4" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete1:) name:@"4" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-4" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail1:) name:@"-4" object:nil];
    
    [self RegisterCall];
    
    btns1.hidden=YES;
    //btnspink1.hidden=YES;
    btnm1.hidden=YES;
   // btnmpink1.hidden=YES;
    btnl1.hidden=YES;
   // btnlpink1.hidden=YES;
    btnxl1.hidden=YES;
   // btnxlpink1.hidden=YES;
    btnxxl1.hidden=YES;
    //btnxxlpink1.hidden=YES;
    
    btns.hidden=YES;
    btnm.hidden=YES;
    btnl.hidden=YES;
    btnxl.hidden=YES;
    btnxxl.hidden=YES;
    
    if (g_IS_IPHONE_4_SCREEN)
    {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"A few more steps and you are done" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        smilyobj.hidden=YES;
        //[self showAnimate];
        i=0;
        self.popUpView.hidden=YES;
        j=0;
        self.popUpView1.hidden=YES;
        
        btnspink.hidden=YES;
        btnmpink.hidden=YES;
        btnlpink.hidden=YES;
        btnxlpink.hidden=YES;
        btnxxlpink.hidden=YES;
        
        
        btnspink1.hidden=YES;
        btnmpink1.hidden=YES;
        btnlpink1.hidden=YES;
        btnxlpink1.hidden=YES;
        btnxxlpink1.hidden=YES;
        
        stthtreeobj=[[stepthree alloc]initWithNibName:@"stepthree" bundle:nil];
    }
    
    else{
        
        smilyobj.hidden=YES;

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"A few more steps and you are done" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        //[self showAnimate];
        
        i=0;
        self.popUpView.hidden=YES;
        j=0;
        self.popUpView1.hidden=YES;
        
        btnspink.hidden=YES;
        btnmpink.hidden=YES;
        btnlpink.hidden=YES;
        btnxlpink.hidden=YES;
        btnxxlpink.hidden=YES;
        
        
        btnspink1.hidden=YES;
        btnmpink1.hidden=YES;
        btnlpink1.hidden=YES;
        btnxlpink1.hidden=YES;
        btnxxlpink1.hidden=YES;
        
        stthtreeobj=[[stepthree alloc]initWithNibName:@"stepthreeiphone" bundle:nil];
    }
    

}
-(void)RegisterCall {
    
    
//    NSString *myRequestString = [NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getGlobalSize&eSizeType=top"];
    
    NSString *myRequestString = [NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getStoreSize"];

    [[HttpQueue sharedSingleton] getItems:myRequestString:3];
}

- (void) registerCallComplete : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSArray *dictResult =(NSArray *)result;
    NSLog(@"re=%@",dictResult);
    
    for (int k=0; k<[dictResult count]; k++)
    {
        NSDictionary *dict_steps=[dictResult objectAtIndex:k];
        
        NSString *value=[dict_steps objectForKey:@"vSizeTitle"];
        NSLog(@"vSizeTitle id== %@",value);
        NSString *value1=[dict_steps objectForKey:@"iStoreSizeId"];
        NSLog(@"iSizeId id== %@",value1);
        
        [store addObject:value];
        [topsize addObject:value1];
        
        
               
    }
    NSUInteger idx2 = 0;
    
    for ( NSString *string2 in topsize )
    {
        switch ( idx2++ ) {
            case 0:
                NSLog(@"Size Id=%@",string2);
                fid.text=string2;
                break;
            case 1:
                NSLog(@"Size Id=%@",string2);
                sid.text=string2;
                
                break;
            case 2:
                NSLog(@"Size Id=%@",string2);
                tid.text=string2;
                break;
            case 3:
                NSLog(@"Size Id=%@",string2);
                foid.text=string2;
                break;
            case 4:
                NSLog(@"Size Id=%@",string2);
                fiid.text=string2;
                break;
        }
    }

    NSUInteger idx = 0;
    
    for ( NSString *string in store )
    {
        switch ( idx++ ) {
            case 0:
                btns.hidden=NO;
                [btns setTitle:string forState:UIControlStateNormal];
                [btnspink setTitle:string forState:UIControlStateNormal];
                break;
            case 1:
                btnm.hidden=NO;
                [btnm setTitle:string forState:UIControlStateNormal];
                [btnmpink setTitle:string forState:UIControlStateNormal];
                break;
            case 2:
                btnl.hidden=NO;
                [btnl setTitle:string forState:UIControlStateNormal];
                [btnlpink setTitle:string forState:UIControlStateNormal];
                break;
            case 3:
                btnxl.hidden=NO;
                [btnxl setTitle:string forState:UIControlStateNormal];
                [btnxlpink setTitle:string forState:UIControlStateNormal];
                break;
            case 4:
                btnxxl.hidden=NO;
                [btnxxl setTitle:string forState:UIControlStateNormal];
                [btnxxlpink setTitle:string forState:UIControlStateNormal];
                break;
        }
    }
[self RegisterCall1];
}
- (void) registerCallFail : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Fail=%@",dict);
    return;
}

-(void)RegisterCall1 {
    
    
    NSString *myRequestString1 = [NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getStoreSize"];
    [[HttpQueue sharedSingleton] getItems:myRequestString1:4];
}

- (void) registerCallComplete1 : (NSNotification *)notification {
    
    NSDictionary* dict1 = [notification userInfo];
    ASIHTTPRequest *response1 = [dict1 objectForKey:@"index"];
    NXJsonParser* parser1 = [[NXJsonParser alloc] initWithData:[response1 responseData]];
    id result1  = [parser1 parse:nil ignoreNulls:NO];
    
    NSArray *dictResult1 =(NSArray *)result1;
    NSLog(@"re=%@",dictResult1);
    
    for (int l=0; l<[dictResult1 count]; l++)
    {
        NSDictionary *dict_steps1=[dictResult1 objectAtIndex:l];
        
        NSString *value1=[dict_steps1 objectForKey:@"vSizeTitle"];
        NSLog(@"vSizeTitle id== %@",value1);
        
        value11=[dict_steps1 objectForKey:@"iStoreSizeId"];
        NSLog(@"iSizeId id== %@",value11);
        
        [store1 addObject:value1];
        [bottomsize addObject:value11];
    }
    
    
    NSUInteger idx2 = 0;
    
    for ( NSString *string2 in bottomsize )
    {
        switch ( idx2++ ) {
            case 0:
                NSLog(@"Size Id=%@",string2);
                bfid.text=string2;
                break;
            case 1:
                NSLog(@"Size Id=%@",string2);
                bsid.text=string2;
                
                break;
            case 2:
                NSLog(@"Size Id=%@",string2);
                btid.text=string2;
                break;
            case 3:
                NSLog(@"Size Id=%@",string2);
                bfoid.text=string2;
                break;
            case 4:
                NSLog(@"Size Id=%@",string2);
                bfiid.text=string2;
                break;
        }
    }

    
    
    
    NSUInteger idx1 = 0;
    
    for ( NSString *string1 in store1 )
    {
        switch ( idx1++ ) {
            case 0:
                btns1.hidden=NO;
               // btnspink1.hidden=NO;
                [btns1 setTitle:string1 forState:UIControlStateNormal];
                [btnspink1 setTitle:string1 forState:UIControlStateNormal];
                break;
            case 1:
                btnm1.hidden=NO;
                //btnmpink1.hidden=NO;
                [btnm1 setTitle:string1 forState:UIControlStateNormal];
                [btnmpink1 setTitle:string1 forState:UIControlStateNormal];
                break;
            case 2:
                btnl1.hidden=NO;
               // btnlpink1.hidden=NO;
                [btnl1 setTitle:string1 forState:UIControlStateNormal];
                [btnlpink1 setTitle:string1 forState:UIControlStateNormal];
                break;
            case 3:
                btnxl1.hidden=NO;
                //btnxlpink1.hidden=NO;
                [btnxl1 setTitle:string1 forState:UIControlStateNormal];
                [btnxlpink1 setTitle:string1 forState:UIControlStateNormal];
                break;
            case 4:
                btnxxl1.hidden=NO;
                //btnxxlpink1.hidden=NO;
                [btnxxl1 setTitle:string1 forState:UIControlStateNormal];
                [btnxxlpink1 setTitle:string1 forState:UIControlStateNormal];
                break;
        }
        
        
    }
    
}
- (void) registerCallFail1 : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Fail=%@",dict);
    return;
}



-(void)viewWillAppear
{
    



}

-(IBAction)help:(id)sender
{
    
    if (i==0)
    {
        i=1;
        
        self.popUpView.hidden=NO;
        //[self showAnimate];
        self.popUpView1.hidden=YES;

    }
    else
    {
        i=0;
        self.popUpView.hidden=YES;
        j=0;
    }
    
}


-(IBAction)help1:(id)sender
{
    if (j==0)
    {
        j=1;
        
        self.popUpView1.hidden=NO;
       // [self showAnimate1];
        self.popUpView.hidden=YES;
        
    }
    else
    {
        j=0;
        self.popUpView1.hidden=YES;
    }
    
    
}
//- (void)showAnimate
//{
//    smilyobj.hidden=NO;
//    smilyobj.transform = CGAffineTransformMakeScale(2.3, 2.3);
//    smilyobj.alpha = 0;
//    [UIView animateWithDuration:.30 animations:^{
//        smilyobj.alpha = 1;
//        smilyobj.transform = CGAffineTransformMakeScale(1, 1);
//        
//    }];
//    [self.view addSubview:smilyobj];
//    
//    
//}
//-(IBAction)ok:(id)sender
//{
//    smilyobj.hidden=YES;
//
//}

-(IBAction)s:(id)sender
{
    
    if(k==0)
    {
    btnspink.hidden=NO;
    [topsize removeAllObjects];
    k=1;
    [topsize addObject:fid.text];
    NSLog(@"id=%@",topsize);
    }
    
    else
    {
    btnspink.hidden=NO;
    [topsize addObject:fid.text];
    NSLog(@"id=%@",topsize);
    }
}
-(IBAction)m:(id)sender
{
    if(k==0)
    {
    btnmpink.hidden=NO;
    [topsize removeAllObjects];
    k=1;
    [topsize addObject:sid.text];
    NSLog(@"id=%@",topsize);
    }
    else
    {
    btnmpink.hidden=NO;
    [topsize addObject:sid.text];
    NSLog(@"id=%@",topsize);
    }
}
-(IBAction)l:(id)sender
{
    if(k==0)
    {
    btnlpink.hidden=NO;
    [topsize removeAllObjects];
    k=1;
    [topsize addObject:tid.text];
    NSLog(@"id=%@",topsize);
    }
    else
    {
    btnlpink.hidden=NO;
    [topsize addObject:tid.text];
    NSLog(@"id=%@",topsize);
    }

}
-(IBAction)xl:(id)sender
{
    if(k==0)
    {
    btnxlpink.hidden=NO;
    [topsize removeAllObjects];
    k=1;
    [topsize addObject:foid.text];
    NSLog(@"id=%@",topsize);
    }
    else{
    btnxlpink.hidden=NO;
    [topsize addObject:foid.text];
    NSLog(@"id=%@",topsize);
    }
}

-(IBAction)xxl:(id)sender
{
    if(k==0)
    {
    btnxxlpink.hidden=NO;
    [topsize removeAllObjects];
    k=1;
    [topsize addObject:fiid.text];
    NSLog(@"id=%@",topsize);
    }
    else
    {
    btnxxlpink.hidden=NO;
    [topsize addObject:fiid.text];
    NSLog(@"id=%@",topsize);
    }
}

-(IBAction)s1:(id)sender
{
    if(m==0)
    {
    btnspink1.hidden=NO;
    [bottomsize removeAllObjects];
    m=1;
    [bottomsize addObject:bfid.text];
    NSLog(@"id=%@",bottomsize);
    }
    else
    {
    btnspink1.hidden=NO;
    [bottomsize addObject:bfid.text];
    NSLog(@"id=%@",bottomsize);
    }
}
-(IBAction)m1:(id)sender
{
    if(m==0)
    {
    btnmpink1.hidden=NO;
    [bottomsize removeAllObjects];
    m=1;
    [bottomsize addObject:bsid.text];
    NSLog(@"id=%@",bottomsize);
    }
    else
    {
    btnmpink1.hidden=NO;
    [bottomsize addObject:bsid.text];
    NSLog(@"id=%@",bottomsize);
    }
}
-(IBAction)l1:(id)sender
{
    if(m==0)
    {
    btnlpink1.hidden=NO;
    [bottomsize removeAllObjects];
    m=1;
    [bottomsize addObject:btid.text];
    NSLog(@"id=%@",bottomsize);
    }
    else
    {
    btnlpink1.hidden=NO;
    [bottomsize addObject:btid.text];
    NSLog(@"id=%@",bottomsize);
    }

}
-(IBAction)xl1:(id)sender
{
    if(m==0)
    {
    btnxlpink1.hidden=NO;
    [bottomsize removeAllObjects];
    m=1;
    [bottomsize addObject:foid.text];
    NSLog(@"id=%@",bottomsize);
    }
    else
    {
    btnxlpink1.hidden=NO;
    [bottomsize addObject:foid.text];
    NSLog(@"id=%@",bottomsize);
    }

}
-(IBAction)xxl1:(id)sender
{
    if(m==0)
    {
    btnxxlpink1.hidden=NO;
    [bottomsize removeAllObjects];
    m=1;
    [bottomsize addObject:fiid.text];
    NSLog(@"id=%@",bottomsize);
    }
    else
    {
    btnxxlpink1.hidden=NO;
    [bottomsize addObject:fiid.text];
    NSLog(@"id=%@",bottomsize);
    }
}

-(IBAction)spink:(id)sender
{
    btnspink.hidden=YES;
}
-(IBAction)mpink:(id)sender
{
   btnmpink.hidden=YES;
}
-(IBAction)lpink:(id)sender
{
   btnlpink.hidden=YES;
}
-(IBAction)xlpink:(id)sender
{
    btnxlpink.hidden=YES;

}
-(IBAction)xxlpink:(id)sender
{
    btnxxlpink.hidden=YES;
}


-(IBAction)spink1:(id)sender
{
    btnspink1.hidden=YES;

}
-(IBAction)mpink1:(id)sender
{
    btnmpink1.hidden=YES;
}
-(IBAction)lpink1:(id)sender
{
    btnlpink1.hidden=YES;

}
-(IBAction)xlpink1:(id)sender
{
    btnxlpink1.hidden=YES;
}                                                                                                                                                                                                          
-(IBAction)xxlpink1:(id)sender
{
     btnxxlpink1.hidden=YES;

}


-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)next:(id)sender
{
     [[Singleton sharedSingleton]setsize:topsize];
     [[Singleton sharedSingleton]setbsize:bottomsize];
    
   if ((btnspink.hidden==YES && btnmpink.hidden==YES && btnlpink.hidden==YES && btnxlpink.hidden==YES && btnxxlpink.hidden==YES))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SELECT SIZE" message:@"Please Select Tops Size" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if((btnspink1.hidden==YES && btnmpink1.hidden==YES && btnlpink1.hidden==YES && btnxlpink1.hidden==YES && btnxxlpink1.hidden==YES))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SELECT SIZE" message:@"Please Select Bottoms Size" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        [self.navigationController pushViewController:stthtreeobj animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
