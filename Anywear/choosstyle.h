//
//  choosstyle.h
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "step.h"
@class step;

@interface choosstyle : UIViewController
{
    step *stepobj;
    
    int redius1;
    
}
@property(nonatomic,strong)step *stepobj;
@property (nonatomic) int redius1;

-(IBAction)btnback:(id)sender;
-(IBAction)btnday:(id)sender;
-(IBAction)btnnight:(id)sender;

-(void)setRedius1:(int)red1;


@end
