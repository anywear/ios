//
//  choosestyleone.m
//  Anywear
//
//  Created by Pradip on 8/28/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "choosestyleone.h"
#import "Singleton.h"

@interface choosestyleone ()

@end

@implementation choosestyleone
@synthesize imgone,imgtwo,tag,stepobj,redius1,check;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (g_IS_IPHONE_4_SCREEN)
    {
    
     stepobj=[[steptwo alloc]initWithNibName:@"steptwo" bundle:nil];
    }
    else{
    
    stepobj=[[steptwo alloc]initWithNibName:@"steptwoiphone" bundle:nil];
    
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    
    
    imgone.hidden=YES;
    imgtwo.hidden=YES;
    
    i=0;
    
   
    
    
    NSString *val=[[Singleton sharedSingleton]getMyvalue];
    
    if([val isEqualToString:@"0"])
    {
        
        imgone.hidden=NO;
        
    }
    else if([val isEqualToString:@"1"])
    {
        NSString *chk=[[Singleton sharedSingleton]getMycheck];
        
        if([chk isEqualToString:@"0"])
        {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.titleLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:22];
        button.tintColor = [UIColor blackColor];
        button.tintColor= [UIColor colorWithRed:(230/255.0) green:(128/255.0) blue:(139/255.0) alpha:1] ;
        [button addTarget:self
                   action:@selector(next:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Next" forState:UIControlStateNormal];
        button.frame = CGRectMake(261,21,46,30);
        [self.view addSubview:button];
        
        
        imgone.hidden=NO;
        imgtwo.hidden=NO;
        }
        imgone.hidden=NO;
        
    }
    else if([val isEqualToString:@"2"])
    {
        
        
        imgtwo.hidden=NO;
        
    }
}
-(IBAction)night:(id)sender
{
    
    [[Singleton sharedSingleton]setMyvalue:@"1"];
    [[Singleton sharedSingleton]setcategory:@"1"];

    [self.navigationController popViewControllerAnimated:YES];
   
}

-(IBAction)day:(id)sender
{
    [[Singleton sharedSingleton]setMyvalue:@"1"];
    [[Singleton sharedSingleton]setcategory:@"2"];

    [self.navigationController popViewControllerAnimated:YES];
    
    
}


-(IBAction)next:(id)sender
{
    
[self.navigationController pushViewController:stepobj animated:YES];
    
}
-(IBAction)back:(id)sender
{
        //[[Singleton sharedSingleton]setMyvalue:@"0"];
    
        NSArray *arr=self.navigationController.viewControllers;
        [self.navigationController popToViewController:[arr objectAtIndex:7] animated:YES];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
