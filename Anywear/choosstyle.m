//
//  choosstyle.m
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "choosstyle.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"

@interface choosstyle ()

@end

@implementation choosstyle
@synthesize stepobj,redius1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"4" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"4" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-4" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-4" object:nil];
        [self RegisterCall];

    stepobj=[[step alloc]initWithNibName:@"step" bundle:nil];
    }
    
    else{
    
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"4" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"4" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-4" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-4" object:nil];
        [self RegisterCall];

        stepobj=[[step alloc]initWithNibName:@"stepiphone" bundle:nil];

    }
 
    // Do any additional setup after loading the view from its nib.
}
-(void)RegisterCall {
    
    //http://techiestownhosting.com/php/anywear/webservice?action=getGlobalSize&eSizeType=top
    NSString *myRequestString = [NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getStyleCategories"];
    [[HttpQueue sharedSingleton] getItems:myRequestString:4];
}

- (void) registerCallComplete : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSArray *dictResult =(NSArray *)result;
    NSLog(@"re=%@",dictResult);
    
    
    for (int k=0; k<[dictResult count]; k++)
    {
        NSDictionary *dict_steps1=[dictResult objectAtIndex:k];
        
        NSString *value=[dict_steps1 objectForKey:@"iCategoryId"];
        NSLog(@"iCategoryId== %@",value);
        NSString *value1=[dict_steps1 objectForKey:@"vCategoryName"];
        NSLog(@"vCategoryName== %@",value1);
    }
}
- (void) registerCallFail : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Fail=%@",dict);
    return;
}

-(void) viewWillAppear:(BOOL)animated
{
    
//    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"You select three styles. Do you want to change any picks?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles: @"No", nil];
//    
//    [alert show];
    

}

-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}




-(IBAction)btnday:(id)sender
{
    
    
    [[Singleton sharedSingleton]setMyvalue:@"0"];
    [[Singleton sharedSingleton]setcategory:@"2"];
    [self.navigationController pushViewController:stepobj animated:YES];
}
-(IBAction)btnnight:(id)sender
{
    
     [[Singleton sharedSingleton]setMyvalue:@"2"];
     [[Singleton sharedSingleton]setcategory:@"1"];
     [self.navigationController pushViewController:stepobj animated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
