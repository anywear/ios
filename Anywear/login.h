//
//  login.h
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "style.h"
@class style;

@interface login : UIViewController
{
    style *styleobj;
    UITextField *username;
    UITextField *password;
    UITextField *frpass;
    UIScrollView *scroll;
    
    NSOperationQueue *_queue;
    NSString *suc;
    int tag;
    UIView *forgotpass;
    
    int chk;
   

}
@property (nonatomic) int chk;
@property (nonatomic,strong) IBOutlet  UIView *forgotpass;
@property (nonatomic) int tag;
@property (nonatomic,strong) NSString *suc;
@property (retain) NSOperationQueue *queue;
@property(nonatomic,strong)  style *styleobj;
@property (nonatomic,retain) IBOutlet UIScrollView *scroll;
@property(strong,nonatomic) IBOutlet UITextField *username;
@property(strong,nonatomic) IBOutlet UITextField *password;
@property(strong,nonatomic) IBOutlet UITextField *frpass;

-(IBAction)cancel:(id)sender;
-(IBAction)btnback:(id)sender;
-(IBAction)btnnext:(id)sender;
-(IBAction)forgotpasswrd:(id)sender;
-(IBAction)sendnow:(id)sender;


@end

