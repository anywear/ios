//
//  map.m
//  Anywear
//
//  Created by Pradip on 8/26/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "map.h"
#import "SampleCell.h"
#import <GoogleMaps/GoogleMaps.h>


#define myURL [NSURL URLWithString: @"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=20.7563771,75.6717191&mode=driving&sensor=false",locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude]


@interface map ()<GMSMapViewDelegate>
@end

@implementation map
@synthesize viewObj,tblobj,arrData,btngalary,btngalary1,mapviewobj,locationManager,currentPlaceMarker,proobj,newex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    
        proobj=[[myprofile alloc]initWithNibName:@"myprofile" bundle:nil];
      newex=[[explore alloc]initWithNibName:@"explore" bundle:nil];
        
    btngalary1.hidden=YES;
    arrData =[[NSMutableArray alloc] init];
    [arrData addObject:@"Explore"];
    [arrData addObject:@"Change Personal Settings"];
    [arrData addObject:@"Boutiqall"];
    [arrData addObject:@"Notifications"];
    [arrData addObject:@"My Profile"];
    [arrData addObject:@"Sign Out"];


    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude
                                                            longitude:locationManager.location.coordinate.longitude
                                                                 zoom:6];
    mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, 320, 415) camera:camera];
    
    
    
    mapView.myLocationEnabled = YES;
    [self.mapviewobj addSubview:mapView];
    self->mapView.settings.myLocationButton = YES;
    mapView.myLocationEnabled = YES;
    mapView.mapType = kGMSTypeNormal;
    mapView.autoresizingMask =
    UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    

    NSLog(@"%f %f", locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude);
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    }
    
    else
    {
        
        proobj=[[myprofile alloc]initWithNibName:@"myprofileiphone" bundle:nil];

      newex=[[explore alloc]initWithNibName:@"exploreiphone" bundle:nil];
        
        btngalary1.hidden=YES;
        arrData =[[NSMutableArray alloc] init];
        [arrData addObject:@"Explore"];
        [arrData addObject:@"Change Personal Settings"];
        [arrData addObject:@"Boutiqall"];
        [arrData addObject:@"Notifications"];
        [arrData addObject:@"My Profile"];
        [arrData addObject:@"Sign Out"];
        
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate=self;
        locationManager.desiredAccuracy=kCLLocationAccuracyBest;
        locationManager.distanceFilter=kCLDistanceFilterNone;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude
                                                                longitude:locationManager.location.coordinate.longitude
                                                                     zoom:6];
        
        mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, 320, 515) camera:camera];
        
        
        
        mapView.myLocationEnabled = YES;
        [self.mapviewobj addSubview:mapView];
        self->mapView.settings.myLocationButton = YES;
        mapView.myLocationEnabled = YES;
        
        mapView.mapType = kGMSTypeNormal;
        mapView.autoresizingMask =
        UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        NSLog(@"%f %f", locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude);
        locationManager = [CLLocationManager new];
        locationManager.delegate = self;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locationManager startUpdatingLocation];
}


}
-(void)viewWillAppear:(BOOL)animated
{
    
    btngalary.hidden=NO;
    btngalary1.hidden=YES;
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}
-(IBAction)btngalaryclick:(id)sender;
{
    btngalary.hidden=YES;
    btngalary1.hidden=NO;
    viewObj.hidden=NO;
    

    [UIView animateWithDuration:0.10f delay:0.10f options:UIViewAnimationOptionTransitionFlipFromRight animations:^
     {
         viewObj.frame = CGRectMake(0,100, viewObj.bounds.size.width, viewObj.bounds.size.height);
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Animation is complete");
     }];
}
-(IBAction)btngalary1click:(id)sender
{
    btngalary.hidden=NO;
    btngalary1.hidden=YES;
    viewObj.hidden=NO;
    
    [UIView animateWithDuration:0.10f delay:0.10f options:UIViewAnimationOptionTransitionFlipFromRight animations:^
     {
         viewObj.frame = CGRectMake(320,100, viewObj.bounds.size.width, viewObj.bounds.size.height);
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Animation is complete");
     }];
}


- (void)updateMapView:(CLLocation *)location {
    
    // create a region and pass it to the Map View
    MKCoordinateRegion region;
    region.center.latitude = location.coordinate.latitude;
    region.center.longitude = location.coordinate.longitude;
    region.span.latitudeDelta = 0.001;
    region.span.longitudeDelta = 0.001;
    
    
}

-(void)setmymarker :(NSString *)Lati :(NSString *)Longi :(NSString *)Address :(NSString *)markername
{
    
    longitude=Longi;
    latitude=Lati;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrData count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifre =@"SampleCell";
    
    SampleCell *cell =(SampleCell *) [self.tblobj dequeueReusableCellWithIdentifier:cellIdentifre];
    
    if (cell==nil) {
        NSArray *arrNib=[[NSBundle mainBundle] loadNibNamed:cellIdentifre owner:self options:nil];
        cell= (SampleCell *)[arrNib objectAtIndex:0];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    cell.labelForPlace.text=[arrData objectAtIndex:indexPath.row];
    
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0)
    {
//        NSArray *arr=self.navigationController.viewControllers;
//        [self.navigationController popToViewController:[arr objectAtIndex:2] animated:YES];
         [self.navigationController pushViewController:newex animated:YES];
    }
    else if (indexPath.row==1)
    {
        
        
    }
    else if (indexPath.row==2)
    {
        
        viewObj.hidden=YES;
        btngalary.hidden=NO;
        btngalary1.hidden=YES;
        
    }
    else if (indexPath.row==3)
    {
        
    }
    else if (indexPath.row==4)
    {
        [self.navigationController pushViewController:proobj animated:YES];
        
        
    }
    else if (indexPath.row==5)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"uname"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"pass"];
        viewObj.hidden=YES;
    }
    
}

-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
