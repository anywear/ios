//
//  shoppingone.h
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "indicator.h"
@class indicator;
@interface shoppingone : UIViewController
{
    UITableView *stblobj;
    NSMutableArray *sarrdata;
    indicator *indicatorobj;
    UIButton *btn;
    
    UILabel *loc;
    
    NSMutableArray *categoryarray;
    NSMutableArray *categoryarray1;
    
}

@property(nonatomic,strong)   IBOutlet  UILabel *loc;
@property(nonatomic,strong) NSMutableArray *categoryarray;
@property(nonatomic,strong) NSMutableArray *categoryarray1;

@property(nonatomic,strong)     UIButton *btn;
@property(nonatomic,strong) IBOutlet UITableView *stblobj;
@property(nonatomic,strong)  NSMutableArray *sarrdata;

@property(nonatomic,strong)    indicator *indicatorobj;



-(IBAction)next:(id)sender;
-(IBAction)btnback:(id)sender;

@end
