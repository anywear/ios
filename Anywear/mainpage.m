//
//  mainpage.m
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "mainpage.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Singleton.h"
#import "DejalActivityView.h"

@interface mainpage ()
@property (strong, nonatomic) IBOutlet FBProfilePictureView *profilePictureView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@end

@implementation mainpage
@synthesize infoobj,loginobj,signupobj,entobj,mainexobj,lblexplore,btnexplore,signupexpobj,loginButton,imgPhoto,stobj;

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    if (g_IS_IPHONE_4_SCREEN)
    {
      
        lblexplore.hidden=YES;
        
        
     stobj=[[style alloc]initWithNibName:@"style" bundle:nil];
     infoobj=[[info alloc]initWithNibName:@"info" bundle:nil];
     signupobj=[[signup alloc]initWithNibName:@"signup" bundle:nil];
     loginobj=[[login alloc]initWithNibName:@"login" bundle:nil];
     entobj=[[entrance alloc]initWithNibName:@"entrance" bundle:nil];
     mainexobj=[[mainexplorer alloc]initWithNibName:@"mainexplorer" bundle:nil];
     signupexpobj=[[signupexplore alloc]initWithNibName:@"signupexplore" bundle:nil];
        
        FBLoginView *loginView = [[FBLoginView alloc] initWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]];
        loginView.delegate = self;
        loginView.center = self.view.center;
       
        loginView.frame=CGRectMake(20, 208, 280, 42);
        [self.view addSubview:loginView];
        
    }
    else
    {
         lblexplore.hidden=YES;
    
      stobj=[[style alloc]initWithNibName:@"styleiphone" bundle:nil];
      infoobj=[[info alloc]initWithNibName:@"infoiphone" bundle:nil];
      signupobj=[[signup alloc]initWithNibName:@"signupiphone" bundle:nil];
      loginobj=[[login alloc]initWithNibName:@"loginiphone" bundle:nil];
      entobj=[[entrance alloc]initWithNibName:@"entranceiphone" bundle:nil];
      mainexobj=[[mainexplorer alloc]initWithNibName:@"mainexploreiphone" bundle:nil];
      signupexpobj=[[signupexplore alloc]initWithNibName:@"signupexploreiphone" bundle:nil];
        
        FBLoginView *loginView = [[FBLoginView alloc] initWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]];
        loginView.delegate = self;
        loginView.center = self.view.center;
        
        loginView.frame=CGRectMake(20, 210, 280, 42);
        [self.view addSubview:loginView];
  }
    
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        FBLoginView *loginView = [[FBLoginView alloc] initWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]];
        loginView.delegate = self;
        loginView.center = self.view.center;
        
        loginView.frame=CGRectMake(20, 208, 280, 42);
        [self.view addSubview:loginView];
        
        
        
        FBRequest* friendsRequest = [FBRequest requestForMyFriends];
        [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                      NSDictionary* result,
                                                      NSError *error) {
            NSArray* friends = [result objectForKey:@"data"];
            NSLog(@"Found: %i friends", friends.count);
            for (NSDictionary<FBGraphUser>* friend in friends) {
                NSLog(@"I have a friend named %@ with id %@", friend.name, friend.objectID);
            }
        }];
        
        
        
    }
    return self;
}
- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
}
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    NSLog(@"Use=%@",user);
    
    
    if (FBSession.activeSession.isOpen)
    {
        UIView *viewToUse = self.view;
        viewToUse = self.navigationController.navigationBar.superview;
        [DejalBezelActivityView activityViewForView:viewToUse];
        
        
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error)
         {
             if (!error)
             {
                 NSLog (@"FB:          id: %@", user.objectID);
                 [[Singleton sharedSingleton]setFBid:user.objectID];
                 NSLog (@"FB:    username: %@", user.username);
                 NSLog (@"FB:        link: %@", user.link);
                 NSLog (@"FB:        name: %@", user.name);
                 NSLog (@"FB:  first_name: %@", user.first_name);
                 NSLog (@"FB:   last_name: %@", user.last_name);
                 NSLog (@"FB: middle_name: %@", user.middle_name);
                 NSLog (@"FB:    birthday: %@", user.birthday);
                 NSLog (@"FB:       email: %@",[user objectForKey:@"email"]);
                 NSString *fbname=[user objectForKey:@"email"];
                 NSLog (@"FB:      gender: %@",[user objectForKey:@"gender"]);
                 [[Singleton sharedSingleton]setFBname:fbname];
                 [[Singleton sharedSingleton]settag:@"0"];
                 [self log];
             }
         }];
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(IBAction)btninfo:(id)sender
{
   [self.navigationController pushViewController:infoobj animated:YES];

   
}
-(void)log
{
    [[Singleton sharedSingleton]setMyfbcheck:@"0"];
    [self.navigationController pushViewController:signupobj animated:YES];
    //[self.navigationController pushViewController:stobj animated:YES];
    [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
}
-(IBAction)btnlogin:(id)sender
{
    [self.navigationController pushViewController:loginobj animated:YES];
    


}




//-(IBAction)btnfacebook:(id)sender
//{
//    NSLog(@"rweqr");
//    if (![self openSessionWithAllowLoginUI:YES]) {
//        [self showLoginView];
//
//}
//}
//#pragma mark -
//#pragma mark - Facebook Method
//
//- (void) performPublishAction:(void (^)(void)) action {
//    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
//        [FBSession.activeSession reauthorizeWithPublishPermissions:[NSArray arrayWithObject:@"publish_actions"] defaultAudience:FBSessionDefaultAudienceFriends completionHandler:^(FBSession *session, NSError *error) {
//            if (!error) {
//                action();
//            }
//        }];
//        
//    }
//    else {
//        action();
//    }
//}
//
//#pragma mark -
//#pragma mark Facebook Login Code
//
//- (void)showLoginView {
//   // [self dismissViewControllerAnimated:NO completion:nil];
//    [self performSelector:@selector(showLoginView1) withObject:nil afterDelay:1.5f];
//}
//
//- (void)showLoginView1 {
//}
//
//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error {
//    switch (state) {
//        case FBSessionStateOpen: {
//            if (self != nil) {
//                [[FBRequest requestForMe] startWithCompletionHandler: ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
//                    if (error) {
//                        //error
//                    }else{
//                        if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
//                            NSArray *permissions = [[NSArray alloc] initWithObjects: @"publish_actions", @"publish_stream", nil];
//                            
//                            
//                            
//                            [FBSession.activeSession reauthorizeWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends completionHandler:^(FBSession *session, NSError *error) {
//                                if (!error) {
//                                    [self uploadVideoOnFacebook];
//                                }
//                                else {
//                                    NSLog(@"%@",error);
//                                }
//                            }];
//                        }
//                        else {
//                            [self uploadVideoOnFacebook];
//                        }
//                    }
//                }];
//            }
//            FBCacheDescriptor *cacheDescriptor = [FBFriendPickerViewController cacheDescriptor];
//            [cacheDescriptor prefetchAndCacheForSession:session];
//        }
//            break;
//        case FBSessionStateClosed: {
//            //[self StopSpinner];
//            UIViewController *topViewController = [self.navigationController topViewController];
//            UIViewController *modalViewController = topViewController;
//            if (modalViewController != nil) {
//                [topViewController dismissViewControllerAnimated:YES completion:nil];
//            }
//            //[self.navigationController popToRootViewControllerAnimated:NO];
//            
//            [FBSession.activeSession closeAndClearTokenInformation];
//            
//            [self performSelector:@selector(showLoginView) withObject:nil afterDelay:0.5f];
//        }
//            break;
//        case FBSessionStateClosedLoginFailed: {
//            // [self StopSpinner];
//            [self performSelector:@selector(showLoginView) withObject:nil afterDelay:0.5f];
//        }
//            break;
//        default:
//            break;
//    }
//    
//    //[[NSNotificationCenter defaultCenter] postNotificationName:SCSessionStateChangedNotificationCamera object:session];
//    
//    if (error) {
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error: %@", [mainpage FBErrorCodeDescription:error.code]] message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
//        // [alertView release];
//    }
//}
//
//
//- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
//    NSArray *permissions = [[NSArray alloc] initWithObjects: @"publish_actions", @"publish_stream", nil];
//    return [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:allowLoginUI completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//        if (!error) {
//            [self sessionStateChanged:session state:state error:error];
//        }
//        else {
//            NSLog(@"%@",error);
//        }
//    }];
//}
//
//+ (NSString *)FBErrorCodeDescription:(FBErrorCode) code {
//    switch(code){
//        case FBErrorInvalid :{
//            return @"FBErrorInvalid";
//        }
//        case FBErrorOperationCancelled:{
//            return @"FBErrorOperationCancelled";
//        }
//        case FBErrorLoginFailedOrCancelled:{
//            return @"FBErrorLoginFailedOrCancelled";
//        }
//        case FBErrorRequestConnectionApi:{
//            return @"FBErrorRequestConnectionApi";
//        }case FBErrorProtocolMismatch:{
//            return @"FBErrorProtocolMismatch";
//        }
//        case FBErrorHTTPError:{
//            return @"FBErrorHTTPError";
//        }
//        case FBErrorNonTextMimeTypeReturned:{
//            return @"FBErrorNonTextMimeTypeReturned";
//        }
//        default:
//            return @"[Unknown]";
//    }
//}
//

//-(void) uploadVideoOnFacebook {
//    NSURL *pathURL =[[NSBundle mainBundle] URLForResource:@"IMG_0055" withExtension:@"MOV"];
//    //NSURL *pathURL =;
//    NSData *videoData=[NSData dataWithContentsOfURL:pathURL];
//    NSString *strDesc=@"This is Sharing";
//    
//    NSDictionary *videoObject = @{@"title": @"Photo Studeio",@"description": strDesc,[pathURL absoluteString]: videoData};
//    FBRequest *uploadRequest = [FBRequest requestWithGraphPath:@"me/videos" parameters:videoObject HTTPMethod:@"POST"];
//    //[self.view setUserInteractionEnabled:NO];
//
//    [uploadRequest startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//        if (!error)
//            NSLog(@"Video uploaded successfully");
//        else
//            NSLog(@"ideo uploaded error");
//        // [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(popViewAfterMKInfo) userInfo:nil repeats:NO];
//    }];
//}




-(IBAction)btnsignup:(id)sender
{
    [[Singleton sharedSingleton]settag:@"1"];
    [self.navigationController pushViewController:signupobj animated:YES];

}
-(IBAction)btnentrance:(id)sender
{
[self.navigationController pushViewController:entobj animated:YES];

}
-(IBAction)btnexplore:(id)sender
{
    
    btnexplore.hidden=YES;
    lblexplore.hidden=NO;
    [self.navigationController pushViewController:mainexobj animated:YES];

}
- (void)removeActivityView;
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    [[self class] cancelPreviousPerformRequestsWithTarget:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
