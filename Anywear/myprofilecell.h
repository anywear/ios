//
//  myprofilecell.h
//  Boutiqall
//
//  Created by Pradip on 10/6/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myprofilecell : UITableViewCell
{

    UIImageView *photo;
    UILabel *lbladdress;
    UILabel *lblprice;
    

}
@property(nonatomic,strong) IBOutlet UIImageView *photo;
@property(nonatomic,strong) IBOutlet UILabel *lbladdress;
@property(nonatomic,strong) IBOutlet UILabel *lblprice;


@end
