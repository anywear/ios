//
//  style.m
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "style.h"


@interface style ()

@end

@implementation style
@synthesize choseobj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    
    choseobj=[[choosstyle alloc]initWithNibName:@"choosstyle" bundle:nil];
    }
    else{
    
        choseobj=[[choosstyle alloc]initWithNibName:@"choosestyleihone" bundle:nil];

    
    }

    // Do any additional setup after loading the view from its nib.
}

-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(IBAction)later:(id)sender
{
     NSArray *arr=self.navigationController.viewControllers;
     [self.navigationController popToViewController:[arr objectAtIndex:2] animated:YES];
}
-(IBAction)letsjoin:(id)sender
{
  [self.navigationController pushViewController:choseobj animated:YES];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
