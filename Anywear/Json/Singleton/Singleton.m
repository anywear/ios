//
//  Singleton.m
//  PocketRealty
//
//  Created by TISMobile on 5/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

//A singleton is a special kind of class where only one instance of the class exists for the current process or entire application.

#import "Singleton.h"


@implementation Singleton
@synthesize val,check,uname,paswrd,imgPhoto,img,fbusername,fcbid,t,category,fbcheck,size,bsize,strspmoney,strlocation,arrcat,arrloc,dayarr,nightarr,allstring;

+ (Singleton*) sharedSingleton  {
	static Singleton* theInstance = nil;
	if (theInstance == nil) {
		theInstance = [[self alloc] init];
	}
	return theInstance;
}



- (id)init
{
    self = [super init];
    if ( self )
    {
        size = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)setMyvalue : (NSString *)values
{
    
    self.val=values;
    
}
-(NSString *)getMyvalue
{
    return self.val;
    
    
}

-(void)setMycheck : (NSString *)chk
{
    self.check=chk;
    
}
-(NSString *)getMycheck
{
    return self.check;
    
}




- (NSString *) getBaseURL
{
    //return @"http://techiestownhosting.com/php/anywear/webservice?";
	return @"http://192.168.1.41/php/anywear/webservice/";
   // http://techiestownhosting.com/php/anywear/webservice?action=getUserLogin
}

-(void)setMyuname : (NSString *)unm;
{
    self.uname=unm;
}
-(NSString *)getMyuname  {
    return self.uname;
}

-(void)setMypasswrd : (NSString *)psw
{
    self.paswrd=psw;
}
-(NSString *)getMypasswrd {
    return self.paswrd;
}
-(void)setMyPhotoImage : (UIImageView *)iimg {
    imgPhoto=iimg;
}
-(UIImageView *)getMyPhotoImage  {
    return imgPhoto;
}

-(void)setMyImage :(UIImageView *)img1 {
    img=img1;
}
-(UIImageView *)getMyImage {
    return img;
}

-(void)setFBname : (NSString *)fbnm
{
    self.fbusername=fbnm;
}
-(NSString *)getFBname
{
    return self.fbusername;
}
-(void)setFBid : (NSString *)fbud
{
    self.fcbid=fbud;
}
-(NSString *)getFBid
{
    return self.fcbid;
}

-(void)settag : (NSString *)tg
{
    self.t=tg;
}
-(NSString *)gettag
{
    return self.t;
}
-(void)setcategory:(NSString *)cat
{
    self.category=cat;
}
-(NSString *)getcategory
{
    return self.category;
}



-(void)setMyfbcheck : (NSString *)fchk
{
    self.fbcheck=fchk;


}
-(NSString *)getMyfbcheck
{
return self.fbcheck;

}

//style

-(void)setdayarr:(NSMutableArray *)da
{
   self.dayarr=da;

}
-(NSMutableArray *)getdayarr
{
    return self.dayarr;

}

-(void)setnightarr:(NSMutableArray *)na
{
     self.nightarr=na;

}
-(NSMutableArray *)getnightarr
{
    return self.nightarr;

}

//Size

-(void)setsize:(NSMutableArray *)sz
{
    self.size=sz;
}
-(NSMutableArray *)getsize
{
    return self.size;
}

-(void)setbsize:(NSMutableArray *)bsz
{
    self.bsize=bsz;

}
-(NSMutableArray *)getbsize
{
   return self.bsize;

}

//money

-(void)setstrspmoney:(NSString *)spm
{
   self.strspmoney=spm;

}
-(NSString *)getstrspmoney
{

  return self.strspmoney;

}

//location

-(void)setarrloc:(NSMutableArray *)lc
{
 self.arrloc=lc;

}
-(NSMutableArray *)getarrloc
{

 return self.arrloc;
}


-(void)setallstring : (NSString *)all
{
    self.allstring=all;

}
-(NSString *)getallstring
{
    return self.allstring;


}



//category

-(void)setarrcat:(NSMutableArray *)ac
{
   self.arrcat=ac;
}
-(NSMutableArray *)getarrcat
{
    return self.arrcat;

}



@end

