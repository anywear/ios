//
//  step.m
//  Anywear
//
//  Created by Pradip on 8/12/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "step.h"
#import "Singleton.h"
#import <QuartzCore/QuartzCore.h>
#import "CollectionMenuCell.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"
#import "DejalActivityView.h"



#define BIG_IMG_WIDTH  250.0
#define BIG_IMG_HEIGHT 382.0

@interface step ()
{
    UIView *bgView;
    UIView *background;
    UIImageView *img;
    UIView *border;
    UIButton *btn;
    UIButton *btn1;
}
@end

@implementation step
@synthesize scrollobj,tag,oneview,secondview,thirdview,imgView,zoomimg,tag1,choneobj,oneview1,secondview1,thirdview1,i,redius,btntag,removeone,removetwo,removethree,oneview2,secondview2,thirdview2,j,remove,button,msgtag,bgview,prsnlsize,styleid,dayarray,nightarray,nstyleid,myString1,myString2,myString3,myString4,myString5,myString6;

//new collectionview
@synthesize arrMenus,arrimg,collectionObj,imgpic,imgView1,arrtag,coutimg,getctgrid,tag11,arrtag1,getarr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    
    
    if (g_IS_IPHONE_4_SCREEN)
    {
        
    button.hidden=YES;
    
    choneobj=[[choosestyleone alloc]initWithNibName:@"choosestyleone" bundle:nil];
    arrimg=[[NSMutableArray alloc] init];
    prsnlsize=[[NSMutableArray alloc] init];
    dayarray=[[NSMutableArray alloc] init];
    nightarray=[[NSMutableArray alloc] init];
    styleid.hidden=YES;
    nstyleid.hidden=YES;

        getarr=[[Singleton sharedSingleton]getdayarr];
        NSLog(@"%@",getarr);
        
    imgpic.hidden=YES;
    getctgrid=[[Singleton sharedSingleton]getcategory];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"1" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-1" object:nil];
    [self RegisterCall];
    
    UINib *cellNib = [UINib nibWithNibName:@"CollectionMenuCell" bundle:nil];
    
    [self.collectionObj registerNib:cellNib forCellWithReuseIdentifier:@"MyMenuCell"];
    }
    else{
    
        button.hidden=YES;
        
        choneobj=[[choosestyleone alloc]initWithNibName:@"choosestyleoneiphone" bundle:nil];
        arrimg=[[NSMutableArray alloc] init];
        prsnlsize=[[NSMutableArray alloc] init];
        dayarray=[[NSMutableArray alloc] init];
        nightarray=[[NSMutableArray alloc] init];
        styleid.hidden=YES;
        nstyleid.hidden=YES;
       
        getarr=[[Singleton sharedSingleton]getdayarr];
        NSLog(@"%@",getarr);
        
        imgpic.hidden=YES;
        getctgrid=[[Singleton sharedSingleton]getcategory];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"1" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"1" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-1" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-1" object:nil];
        [self RegisterCall];
        
        UINib *cellNib = [UINib nibWithNibName:@"CollectionMenuCell" bundle:nil];
        
        [self.collectionObj registerNib:cellNib forCellWithReuseIdentifier:@"MyMenuCell"];

    
    
    
    }
    
    
}
-(void)RegisterCall {
    
    NSString *myRequestString = [NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getStyleByCategoryId&iCategoryId=%@",getctgrid];
    [[HttpQueue sharedSingleton] getItems:myRequestString:1];
    
    UIView *viewToUse = self.view;
    viewToUse = self.navigationController.navigationBar.superview;
    [DejalBezelActivityView activityViewForView:viewToUse];
}


- (void) registerCallComplete : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSArray *dictResult =(NSArray *)result;
    NSLog(@"re=%@",dictResult);
    for (int k=0; k<[dictResult count]; k++)
    {
        NSDictionary *dict_steps=[dictResult objectAtIndex:k];
        NSString *value=[dict_steps objectForKey:@"iCategoryId"];
        NSLog(@"iCategoryId id== %@",value);
        NSString *value1=[dict_steps objectForKey:@"vImage"];
        NSLog(@"vImage id== %@",value1);
        UIImage *myImage = [UIImage imageWithData:
                            [NSData dataWithContentsOfURL:
                             [NSURL URLWithString:value1]]];
        
        imgpic.image=myImage;
        [arrimg addObject:imgpic.image];
        
        
        NSString *prsnid=[dict_steps objectForKey:@"iPersonalId"];
        NSLog(@"prsnal id== %@",prsnid);
        [prsnlsize addObject:prsnid];
        
        
        
    }
    [collectionObj reloadData];
    [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];

}
- (void)removeActivityView;
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    [[self class] cancelPreviousPerformRequestsWithTarget:self];
    
}

- (void) registerCallFail : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Fail=%@",dict);
    return;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    // return arrMenus.count;
    return [arrimg count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionObj dequeueReusableCellWithReuseIdentifier:@"MyMenuCell" forIndexPath:indexPath];
    UIImageView *imgObj = (UIImageView *) [cell viewWithTag:101];
    
    imgObj.image=[arrimg objectAtIndex:indexPath.row];
    coutimg=arrimg.count;
    NSLog(@"%d",coutimg);
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (g_IS_IPHONE_4_SCREEN)
    {
    if (msgtag==0)
    {
        
        [[Singleton sharedSingleton]setMycheck:@"1"];
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"V means you like this style - X means you don't." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: @"Cancel", nil];
        
        
        [alert show];
        
        
        
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.titleLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:22];
        button.tintColor = [UIColor blackColor];
        button.tintColor= [UIColor colorWithRed:(230/255.0) green:(128/255.0) blue:(139/255.0) alpha:1] ;
        [button addTarget:self
                   action:@selector(done:)
        forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Done" forState:UIControlStateNormal];
        button.frame = CGRectMake(268,20,46,30);
        [self.view addSubview:button];

       NSString *cellData = [arrMenus objectAtIndex:indexPath.row];
        NSLog(@"msg=%@",cellData);

        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, 320, 450)];
        [bgView setBackgroundColor:[UIColor colorWithRed:0
                                                   green:0
                                                    blue:0
                                                   alpha:0.7]];
        
        [self.view addSubview:bgView];
        NSLog(@"msg=%ld",(long)indexPath.row);
        
        imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 ,480 )];
        imgView1.transform = CGAffineTransformMakeScale(0.5,0.5);
        imgView1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            imgView1.alpha = 1;
        imgView1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        
        imgView1.image=[arrimg objectAtIndex:indexPath.row];
        arrtag=indexPath.row;
        arrtag=arrtag+1;
        NSLog(@"%d",arrtag);
        [bgView addSubview:imgView1];
        
        
        //cancel
        UIButton *closeBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn1=closeBtn1;
        [closeBtn1 setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
        [closeBtn1 addTarget:self action:@selector(canclebtn) forControlEvents:UIControlEventTouchUpInside];
        [closeBtn1 setFrame:CGRectMake(10,200,40,40)];
        [bgView addSubview:closeBtn1];
        
        //save
        
        UIButton *savebtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn=savebtn1;
        NSLog(@"btntag==%@",btn);
        [savebtn1 setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        [savebtn1 addTarget:self action:@selector(newsave) forControlEvents:UIControlEventTouchUpInside];
        [savebtn1 setFrame:CGRectMake(250,200,40,40)];
        [bgView addSubview:savebtn1];
        
        
        
        //Firstimageview
        oneview =[[UIImageView alloc] initWithFrame:(CGRectMake(10,350, 75, 75))];
        oneview.transform = CGAffineTransformMakeScale(0.5,0.5);
        oneview.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
        oneview.alpha = 1;
        oneview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        [bgView addSubview:oneview];
        
        
        //secondimagevie
        secondview =[[UIImageView alloc] initWithFrame:(CGRectMake(120, 350, 75, 75))];
        secondview.transform = CGAffineTransformMakeScale(0.5,0.5);
        secondview.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            secondview.alpha = 1;
            secondview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        [bgView addSubview:secondview];
        
        //thirdimagevie
        thirdview =[[UIImageView alloc] initWithFrame:(CGRectMake(230, 350, 75, 75))];
        thirdview.transform = CGAffineTransformMakeScale(0.5,0.5);
        thirdview.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            thirdview.alpha = 1;
            thirdview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        [bgView addSubview:thirdview];
        tag=1;
        msgtag=1;
        tag11=2;
        [self createimg];
        //[self scrolling];
        
          
    }
    else
    {
        
        [[Singleton sharedSingleton]setMycheck:@"1"];
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.titleLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:22];
        button.tintColor = [UIColor blackColor];
        button.tintColor= [UIColor colorWithRed:(230/255.0) green:(128/255.0) blue:(139/255.0) alpha:1] ;
        [button addTarget:self
                   action:@selector(done:)
        forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"Done" forState:UIControlStateNormal];
        button.frame = CGRectMake(268,20,46,30);
        [self.view addSubview:button];

        
        
        NSString *cellData = [arrMenus objectAtIndex:indexPath.row];
         NSLog(@"msg=%@",cellData);
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, 320, 450)];
    
    
        [bgView setBackgroundColor:[UIColor colorWithRed:0
                                               green:0
                                                blue:0
                                               alpha:0.7]];
    
        [self.view addSubview:bgView];
        NSLog(@"msg=%ld",(long)indexPath.row);
    
        imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 ,480 )];
        imgView1.transform = CGAffineTransformMakeScale(0.5,0.5);
        imgView1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            imgView1.alpha = 1;
            imgView1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
    
    
        imgView1.image=[arrimg objectAtIndex:indexPath.row];
        arrtag=indexPath.row;
        arrtag=arrtag+1;
        NSLog(@"%d",arrtag);
        [bgView addSubview:imgView1];

    
        //cancel
        UIButton *closeBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn1=closeBtn1;
        [closeBtn1 setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
        [closeBtn1 addTarget:self action:@selector(canclebtn) forControlEvents:UIControlEventTouchUpInside];
        //  NSLog(@"borderview is %@",borderView);
        [closeBtn1 setFrame:CGRectMake(10,200,40,40)];
        [bgView addSubview:closeBtn1];
    
        //save
    
        UIButton *savebtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn=savebtn1;
        NSLog(@"btntag==%@",btn);
        [savebtn1 setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        [savebtn1 addTarget:self action:@selector(newsave) forControlEvents:UIControlEventTouchUpInside];
        // NSLog(@"borderview is %@",borderView);
        [savebtn1 setFrame:CGRectMake(250,200,40,40)];
        [bgView addSubview:savebtn1];

    
    
        //Firstimageview
        oneview =[[UIImageView alloc] initWithFrame:(CGRectMake(10,350, 75, 75))];
        oneview.transform = CGAffineTransformMakeScale(0.5,0.5);
        oneview.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            oneview.alpha = 1;
            oneview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        [bgView addSubview:oneview];
    
    
        //secondimagevie
        secondview =[[UIImageView alloc] initWithFrame:(CGRectMake(120, 350, 75, 75))];
        secondview.transform = CGAffineTransformMakeScale(0.5,0.5);
        secondview.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            secondview.alpha = 1;
            secondview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        [bgView addSubview:secondview];
    
        //thirdimagevie
        thirdview =[[UIImageView alloc] initWithFrame:(CGRectMake(230, 350, 75, 75))];
        thirdview.transform = CGAffineTransformMakeScale(0.5,0.5);
        thirdview.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            thirdview.alpha = 1;
            thirdview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        [bgView addSubview:thirdview];
    
        tag=1;
        tag11=2;
        [self createimg];
    }
    }
    
else{
        if (msgtag==0)
        {
            
            [[Singleton sharedSingleton]setMycheck:@"1"];
            
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"V means you like this style - X means you don't." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: @"Cancel", nil];
            
            
            [alert show];
            button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            button.titleLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:22];
            button.tintColor = [UIColor blackColor];
            button.tintColor= [UIColor colorWithRed:(230/255.0) green:(128/255.0) blue:(139/255.0) alpha:1] ;
            [button addTarget:self
                       action:@selector(done:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:@"Done" forState:UIControlStateNormal];
            button.frame = CGRectMake(268,20,46,30);
            [self.view addSubview:button];
            
            
            
            NSString *cellData = [arrMenus objectAtIndex:indexPath.row];
             NSLog(@"msg=%@",cellData);
            bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, 320, 560)];
            
            
            [bgView setBackgroundColor:[UIColor colorWithRed:0
                                                       green:0
                                                        blue:0
                                                       alpha:0.7]];
            
            [self.view addSubview:bgView];
            NSLog(@"msg=%ld",(long)indexPath.row);
            
            imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 ,550 )];
            //[imgView setImage:[UIImage imageNamed:@"02.png"]];
            
            imgView1.transform = CGAffineTransformMakeScale(0.5,0.5);
            imgView1.alpha = 1;
            [UIView animateWithDuration:1.2 animations:^{
                imgView1.alpha = 1;
                imgView1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
            }];
            
            
            imgView1.image=[arrimg objectAtIndex:indexPath.row];
            arrtag=indexPath.row;
            arrtag=arrtag+1;
            NSLog(@"%d",arrtag);
            [bgView addSubview:imgView1];
            
            
            //cancel
            UIButton *closeBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            btn1=closeBtn1;
            [closeBtn1 setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
            [closeBtn1 addTarget:self action:@selector(canclebtn) forControlEvents:UIControlEventTouchUpInside];
            //  NSLog(@"borderview is %@",borderView);
            [closeBtn1 setFrame:CGRectMake(10,200,40,40)];
            [bgView addSubview:closeBtn1];
            
            //save
            
            UIButton *savebtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            btn=savebtn1;
            NSLog(@"btntag==%@",btn);
            [savebtn1 setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            [savebtn1 addTarget:self action:@selector(newsave) forControlEvents:UIControlEventTouchUpInside];
            // NSLog(@"borderview is %@",borderView);
            [savebtn1 setFrame:CGRectMake(250,200,40,40)];
            [bgView addSubview:savebtn1];
            
            
            
            //Firstimageview
            oneview =[[UIImageView alloc] initWithFrame:(CGRectMake(10,428, 75, 75))];
            oneview.transform = CGAffineTransformMakeScale(0.5,0.5);
            oneview.alpha = 1;
            [UIView animateWithDuration:1.2 animations:^{
                oneview.alpha = 1;
                oneview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
            }];
            [bgView addSubview:oneview];
            
            
            //secondimagevie
            secondview =[[UIImageView alloc] initWithFrame:(CGRectMake(120, 428, 75, 75))];
            secondview.transform = CGAffineTransformMakeScale(0.5,0.5);
            secondview.alpha = 1;
            [UIView animateWithDuration:1.2 animations:^{
                secondview.alpha = 1;
                secondview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
            }];
            [bgView addSubview:secondview];
            
            //thirdimagevie
            thirdview =[[UIImageView alloc] initWithFrame:(CGRectMake(230, 428, 75, 75))];
            thirdview.transform = CGAffineTransformMakeScale(0.5,0.5);
            thirdview.alpha = 1;
            [UIView animateWithDuration:1.2 animations:^{
                thirdview.alpha = 1;
                thirdview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
            }];
            [bgView addSubview:thirdview];
            
            tag=1;
            msgtag=1;
            tag11=2;
            [self createimg];
            
        }
        else
        {
            
            [[Singleton sharedSingleton]setMycheck:@"1"];
            button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            button.titleLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:22];
            button.tintColor = [UIColor blackColor];
            button.tintColor= [UIColor colorWithRed:(230/255.0) green:(128/255.0) blue:(139/255.0) alpha:1] ;
            [button addTarget:self
                       action:@selector(done:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:@"Done" forState:UIControlStateNormal];
            button.frame = CGRectMake(268,20,46,30);
            [self.view addSubview:button];
            
            
            
            NSString *cellData = [arrMenus objectAtIndex:indexPath.row];
             NSLog(@"msg=%@",cellData);
            
            bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, 320, 560)];
            
            
            [bgView setBackgroundColor:[UIColor colorWithRed:0
                                                       green:0
                                                        blue:0
                                                       alpha:0.7]];
            
            [self.view addSubview:bgView];
            NSLog(@"msg=%ld",(long)indexPath.row);
            
            imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 ,550 )];
            //[imgView setImage:[UIImage imageNamed:@"02.png"]];
            
            imgView1.transform = CGAffineTransformMakeScale(0.5,0.5);
            imgView1.alpha = 1;
            [UIView animateWithDuration:1.2 animations:^{
                imgView1.alpha = 1;
                imgView1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
            }];
            
            
            imgView1.image=[arrimg objectAtIndex:indexPath.row];
            arrtag=indexPath.row;
            arrtag=arrtag+1;
            NSLog(@"%d",arrtag);
            [bgView addSubview:imgView1];
            
            
            //cancel
            UIButton *closeBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            btn1=closeBtn1;
            [closeBtn1 setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
            [closeBtn1 addTarget:self action:@selector(canclebtn) forControlEvents:UIControlEventTouchUpInside];
            //  NSLog(@"borderview is %@",borderView);
            [closeBtn1 setFrame:CGRectMake(10,200,40,40)];
            [bgView addSubview:closeBtn1];
            
            //save
            
            UIButton *savebtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
            btn=savebtn1;
            NSLog(@"btntag==%@",btn);
            [savebtn1 setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            [savebtn1 addTarget:self action:@selector(newsave) forControlEvents:UIControlEventTouchUpInside];
            // NSLog(@"borderview is %@",borderView);
            [savebtn1 setFrame:CGRectMake(250,200,40,40)];
            [bgView addSubview:savebtn1];
            
            
            
            //Firstimageview
            oneview =[[UIImageView alloc] initWithFrame:(CGRectMake(10,428, 75, 75))];
            oneview.transform = CGAffineTransformMakeScale(0.5,0.5);
            oneview.alpha = 1;
            [UIView animateWithDuration:1.2 animations:^{
                oneview.alpha = 1;
                oneview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
            }];
            [bgView addSubview:oneview];
            
            
            //secondimagevie
            secondview =[[UIImageView alloc] initWithFrame:(CGRectMake(120, 428, 75, 75))];
            secondview.transform = CGAffineTransformMakeScale(0.5,0.5);
            secondview.alpha = 1;
            [UIView animateWithDuration:1.2 animations:^{
                secondview.alpha = 1;
                secondview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
            }];
            [bgView addSubview:secondview];
            
            //thirdimagevie
            thirdview =[[UIImageView alloc] initWithFrame:(CGRectMake(230, 428, 75, 75))];
            thirdview.transform = CGAffineTransformMakeScale(0.5,0.5);
            thirdview.alpha = 1;
            [UIView animateWithDuration:1.2 animations:^{
                thirdview.alpha = 1;
                thirdview.transform = CGAffineTransformMakeTranslation(0.7,0.7);
            }];
            [bgView addSubview:thirdview];
            
            
            
            tag=1;
            tag11=2;
            [self createimg];
        }
    }
}

-(void)newsave
{
    if (arrtag==coutimg)
    {
         if (oneview.image!=nil && secondview.image!=nil && thirdview.image!=nil)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You can choose 3 styles.\n Please remove one selection from the bottom." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return ;
            arrtag=0;
            arrtag=arrtag+1;
        }
        else
        {
            imgView1.image=[arrimg objectAtIndex:0];
            arrtag=0;
            arrtag=arrtag+1;
            
            if (oneview.image==nil)
            {
               oneview.image=[arrimg objectAtIndex:coutimg-1];
                styleid.text=[NSString stringWithFormat:@"%d",coutimg];
                NSString *getcat=[[Singleton sharedSingleton] getcategory];
                if ([getcat isEqualToString:@"2"])
                {
                    [dayarray addObject:styleid.text];
                }
                else
                {
                    [nightarray addObject:styleid.text];
                }
                
            }
            else if(secondview.image==nil)
            {
               secondview.image=[arrimg objectAtIndex:coutimg-1];
                styleid.text=[NSString stringWithFormat:@"%d",coutimg];
                NSString *getcat=[[Singleton sharedSingleton] getcategory];
                if ([getcat isEqualToString:@"2"])
                {
                    [dayarray addObject:styleid.text];
                }
                else
                {
                    [nightarray addObject:styleid.text];
                }
            }
            else if(thirdview.image==nil)
            {
               thirdview.image=[arrimg objectAtIndex:coutimg-1];
                styleid.text=[NSString stringWithFormat:@"%d",coutimg];
                NSString *getcat=[[Singleton sharedSingleton] getcategory];
                if ([getcat isEqualToString:@"2"])
                {
                    [dayarray addObject:styleid.text];
                }
                else
                {
                    [nightarray addObject:styleid.text];
                }
            }
        }
    }
    
//
    else
    {
        NSString *val=[[Singleton sharedSingleton]getcategory];
        if([val isEqualToString:@"2"])
        {
           imgView1.image=[arrimg objectAtIndex:arrtag];
            if (oneview.image==nil)
            {
            myString1 = [NSString stringWithFormat:@"%d",arrtag];
            styleid.text=myString1;
            oneview.image=[arrimg objectAtIndex:arrtag-1];
            arrtag=arrtag+1;
            }
            
            else if(secondview.image==nil)
            {
             myString2 = [NSString stringWithFormat:@"%d",arrtag];
             styleid.text=myString2;
             secondview.image=[arrimg objectAtIndex:arrtag-1];
             arrtag=arrtag+1;
            }
            
            else if(thirdview.image==nil)
            {
            myString3 = [NSString stringWithFormat:@"%d",arrtag];
            styleid.text=myString3;
            thirdview.image=[arrimg objectAtIndex:arrtag-1];
            arrtag=arrtag+1;
            }
        
            else if (oneview.image!=nil && secondview.image!=nil && thirdview.image!=nil)
            {
            imgView1.image=[arrimg objectAtIndex:arrtag-1];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You can choose 3 styles.\n Please remove one selection from the bottom." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            return ;
            }
             [dayarray addObject:styleid.text];
        }
        else
        {
            imgView1.image=[arrimg objectAtIndex:arrtag];
            if (oneview.image==nil)
            {
                myString4 = [NSString stringWithFormat:@"%d",arrtag];
                nstyleid.text=myString4;
                oneview.image=[arrimg objectAtIndex:arrtag-1];
                arrtag=arrtag+1;
            }
            else if(secondview.image==nil)
            {
                myString5 = [NSString stringWithFormat:@"%d",arrtag];
                nstyleid.text=myString5;
                secondview.image=[arrimg objectAtIndex:arrtag-1];
                arrtag=arrtag+1;
            }
            else if(thirdview.image==nil)
            {
                myString6 = [NSString stringWithFormat:@"%d",arrtag];
                nstyleid.text=myString6;
                thirdview.image=[arrimg objectAtIndex:arrtag-1];
                arrtag=arrtag+1;
            }
            
            else if (oneview.image!=nil && secondview.image!=nil && thirdview.image!=nil)
            {
                imgView1.image=[arrimg objectAtIndex:arrtag-1];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"You can choose 3 styles.\n Please remove one selection from the bottom." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                return ;
            }
            [nightarray addObject:nstyleid.text];
        }
        
       
    }
    
    
}
-(void)canclebtn
{
   
    if (arrtag==coutimg)
    {
        imgView1.image=[arrimg objectAtIndex:0];
        arrtag=0;
        arrtag=arrtag+1;
    }
    else
    {
        imgView1.image=[arrimg objectAtIndex:arrtag];
        arrtag=arrtag+1;
    }
    
}
- (void)fangda
{
    
    if (i==0)
    {
        j=0;
        if(j==0)
        {
            [self createimg];
        }
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"V means you like this style - X means you don't." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: @"Cancel", nil];
       [alert show];
        tag=1;
        
        
       bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 515)];
        
        background = bgView;
        [bgView setBackgroundColor:[UIColor colorWithRed:0
                                                   green:0
                                                    blue:0
                                                   alpha:0.70]];

        [scrollobj addSubview:bgView];
        UIView *borderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,BIG_IMG_WIDTH+16, BIG_IMG_HEIGHT+16)];
        border=borderView;
        [borderView setCenter:bgView.center];
        [scrollobj addSubview:borderView];
        
                  if (g_IS_IPHONE_4_SCREEN)
                   {
            
                   imgView = [[UIImageView alloc] initWithFrame:CGRectMake(-28, -59, 320 ,450 )];
                   img=imgView;
                   [imgView setImage:[UIImage imageNamed:@"1.png"]];
                   scrollobj.contentSize = CGSizeMake(0,0);
                   }
                   else
                   {
            
                   imgView = [[UIImageView alloc] initWithFrame:CGRectMake(-28, -60, 320 ,550 )];
                   img=imgView;
                   [imgView setImage:[UIImage imageNamed:@"1.png"]];
                   scrollobj.contentSize = CGSizeMake(0,0);
                   }
        
        oneview.hidden=NO;
        secondview.hidden=NO;
        thirdview.hidden=NO;
        
        
        //zoomimg=imgView;
        
        [borderView addSubview:imgView];
        imgView.transform = CGAffineTransformMakeScale(0.5,0.5);
        imgView.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            imgView.alpha = 1;
            imgView.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        
        
        
        //cancel
//        UIButton *closeBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//        btn1=closeBtn1;
//        [closeBtn1 setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
//        [closeBtn1 addTarget:self action:@selector(suoxiao1) forControlEvents:UIControlEventTouchUpInside];
//        NSLog(@"borderview is %@",borderView);
//        [closeBtn1 setFrame:CGRectMake(10,200,40,40)];
//        [scrollobj addSubview:closeBtn1];
//        
//        //save
//        
//        UIButton *savebtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//        btn=savebtn1;
//        [savebtn1 setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
//        [savebtn1 addTarget:self action:@selector(save1) forControlEvents:UIControlEventTouchUpInside];
//        NSLog(@"borderview is %@",borderView);
//        [savebtn1 setFrame:CGRectMake(250,200,40,40)];
//        [scrollobj addSubview:savebtn1];
        
        
        
        //backslide
        
        UIImageView *slide =[[UIImageView alloc] initWithFrame:(CGRectMake(10, 350, 280, 75))];
        slide.image=[UIImage imageNamed:@""];
        slide.transform = CGAffineTransformMakeScale(0.5,0.5);
        slide.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            slide.alpha = 1;
            slide.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        
        [imgView addSubview:slide];
        [self.view exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
    }
else
    {
        
        
        
        if(j==0)
        {
            
            [self createimg];
            
            
        }

        
        tag=1;
        
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 515)];
        
        background = bgView;
        [bgView setBackgroundColor:[UIColor colorWithRed:0
                                                   green:0
                                                    blue:0
                                                   alpha:0.70]];

        [scrollobj addSubview:bgView];
        UIView *borderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,BIG_IMG_WIDTH+16, BIG_IMG_HEIGHT+16)];
        border=borderView;
        [borderView setCenter:bgView.center];
        [scrollobj addSubview:borderView];
        
        if (g_IS_IPHONE_4_SCREEN)
        {
            
            imgView = [[UIImageView alloc] initWithFrame:CGRectMake(-28, -59, 320 ,450 )];
            img=imgView;
            [imgView setImage:[UIImage imageNamed:@"1.png"]];
            scrollobj.contentSize = CGSizeMake(0,0);
        }
        else
        {
            imgView = [[UIImageView alloc] initWithFrame:CGRectMake(-28, -60, 320 ,550 )];
            img=imgView;
            [imgView setImage:[UIImage imageNamed:@"1.png"]];
            scrollobj.contentSize = CGSizeMake(0,0);
        }
        
        oneview.hidden=NO;
        secondview.hidden=NO;
        thirdview.hidden=NO;
        
        
        //zoomimg=imgView;
        
        [borderView addSubview:imgView];
        imgView.transform = CGAffineTransformMakeScale(0.5,0.5);
        imgView.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            imgView.alpha = 1;
            imgView.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        
        
        
        //cancel
//        UIButton *closeBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//        btn1=closeBtn1;
//        [closeBtn1 setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
//        [closeBtn1 addTarget:self action:@selector(suoxiao1) forControlEvents:UIControlEventTouchUpInside];
//        NSLog(@"borderview is %@",borderView);
//        [closeBtn1 setFrame:CGRectMake(10,200,40,40)];
//        [scrollobj addSubview:closeBtn1];
//        
//        //save
//        
//        UIButton *savebtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
//        btn=savebtn1;
//        [savebtn1 setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
//        [savebtn1 addTarget:self action:@selector(save1) forControlEvents:UIControlEventTouchUpInside];
//        NSLog(@"borderview is %@",borderView);
//        [savebtn1 setFrame:CGRectMake(250,200,40,40)];
//        [scrollobj addSubview:savebtn1];
        
        
        
        //backslide
        
        UIImageView *slide =[[UIImageView alloc] initWithFrame:(CGRectMake(10, 350, 280, 75))];
        slide.image=[UIImage imageNamed:@""];
        slide.transform = CGAffineTransformMakeScale(0.5,0.5);
        slide.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            slide.alpha = 1;
            slide.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        
        [imgView addSubview:slide];
        [self.view exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
        
    }
    
   

}
-(IBAction)remove:(id)sender
{
    NSString *val=[[Singleton sharedSingleton]getcategory];
    if([val isEqualToString:@"2"])
    {
    [dayarray removeObject:myString1];
    oneview.image=[UIImage imageNamed:@""];
    }
    else
    {
      [nightarray removeObject:myString4];
      oneview.image=[UIImage imageNamed:@""];
    }
}

-(IBAction)remove1:(id)sender
{
    NSString *val=[[Singleton sharedSingleton]getcategory];
    if([val isEqualToString:@"2"])
    {
    [dayarray removeObject:myString2];
    secondview.image=[UIImage imageNamed:@""];
    }
    else
    {
    [nightarray removeObject:myString5];
    secondview.image=[UIImage imageNamed:@""];
    
    }
    
}
-(IBAction)remove2:(id)sender
{
    NSString *val=[[Singleton sharedSingleton]getcategory];
    if([val isEqualToString:@"2"])
    {
    [dayarray removeObject:myString3];
    thirdview.image=[UIImage imageNamed:@""];
    }
    else
    {
    [nightarray removeObject:myString6];
    thirdview.image=[UIImage imageNamed:@""];
 }
}

-(void)scrolling
{
    
    ALScrollViewPaging *scrollView = [[ALScrollViewPaging alloc] initWithFrame:CGRectMake(0,54,320,195)];
    
    
    NSMutableArray *views = [[NSMutableArray alloc] init];
    
    NSString *imge=[arrimg lastObject];
    NSLog(@"last=%@",imge);
    
    
    for (int k = 0; k < arrimg.count; k++)
    {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
        [views addObject:view];
        
        bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, 320, 450)];
        [bgView setBackgroundColor:[UIColor colorWithRed:0
                                                   green:0
                                                    blue:0
                                                   alpha:0.7]];
        
        imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 ,480 )];
        imgView1.transform = CGAffineTransformMakeScale(0.5,0.5);
        imgView1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            imgView1.alpha = 1;
            imgView1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
         [bgView addSubview:imgView1];
        
        
        UIButton *closeBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn1=closeBtn1;
        [closeBtn1 setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
        [closeBtn1 addTarget:self action:@selector(canclebtn) forControlEvents:UIControlEventTouchUpInside];
        [closeBtn1 setFrame:CGRectMake(10,200,40,40)];
        [bgView addSubview:closeBtn1];
        
        UIButton *savebtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn=savebtn1;
        NSLog(@"btntag==%@",btn);
        [savebtn1 setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        [savebtn1 addTarget:self action:@selector(newsave) forControlEvents:UIControlEventTouchUpInside];
        [savebtn1 setFrame:CGRectMake(250,200,40,40)];
        [bgView addSubview:savebtn1];

    }
    
    for (int k = 0; k < arrimg.count; k++) {
        
        
        imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320 ,480 )];
        imgView1.transform = CGAffineTransformMakeScale(0.5,0.5);
        imgView1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            imgView1.alpha = 1;
            imgView1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        [bgView addSubview:imgView1];

        CGRect frame;
        frame.origin.x = scrollView.frame.size.width * k;
        frame.origin.y = 0;
        frame.size = scrollView.frame.size;
        
        UIScrollView *subScrollView = [[UIScrollView alloc] initWithFrame:frame];
        
        UIImageView *subview = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
        
        UIImage *image = [arrimg objectAtIndex:i];
        subScrollView.contentSize = image.size;
        
        [subview setImage:image];
        [subScrollView addSubview:subview];
        [scrollView addSubview:bgView];
        [subview release];
        [subScrollView release];
    }
    [scrollView addPages:views];
    [self.view addSubview:scrollView];
    [scrollView setHasPageControl:YES];


}

-(void)createimg
{
    
    
    if (g_IS_IPHONE_4_SCREEN)
    {
        
        oneview1 =[[UIImageView alloc] initWithFrame:(CGRectMake(10, 400, 75, 75))];
        oneview1.image=[UIImage imageNamed:@"shadow21.png"];
        oneview1.transform = CGAffineTransformMakeScale(0.5,0.5);
        oneview1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            oneview1.alpha = 1;
            oneview1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        removeone = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        removeone.tintColor = [UIColor blackColor];
        [removeone addTarget:self
                      action:@selector(remove:) forControlEvents:UIControlEventTouchUpInside];
        [removeone setImage:[UIImage imageNamed:@"cancel-1.png"] forState:UIControlStateNormal];
        removeone.frame = CGRectMake(65,400,20,20);
        [self.view addSubview:removeone];
        [self.view addSubview:oneview1];
        
        
        
        secondview1 =[[UIImageView alloc] initWithFrame:(CGRectMake(120, 400, 75, 75))];
        secondview1.image=[UIImage imageNamed:@"shadow21.png"];
        secondview1.transform = CGAffineTransformMakeScale(0.5,0.5);
        secondview1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            secondview1.alpha = 1;
            secondview1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        removetwo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [removetwo addTarget:self
                      action:@selector(remove1:)
            forControlEvents:UIControlEventTouchUpInside];
        removetwo.tintColor = [UIColor blackColor];
        [removetwo setImage:[UIImage imageNamed:@"cancel-1.png"] forState:UIControlStateNormal];
        removetwo.frame = CGRectMake(175,400,20,20);
        [self.view addSubview:removetwo];
        [self.view addSubview:secondview1];
        
        
        
        thirdview1 =[[UIImageView alloc] initWithFrame:(CGRectMake(230, 400, 75, 75))];
        thirdview1.image=[UIImage imageNamed:@"shadow21.png"];
        thirdview1.transform = CGAffineTransformMakeScale(0.5,0.5);
        thirdview1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            thirdview1.alpha = 1;
            thirdview1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        removethree = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [removethree addTarget:self
                        action:@selector(remove2:)
              forControlEvents:UIControlEventTouchUpInside];
        removethree.tintColor = [UIColor blackColor];
        [removethree setImage:[UIImage imageNamed:@"cancel-1.png"] forState:UIControlStateNormal];
        
        removethree.frame = CGRectMake(285,400,20,20);
        [self.view addSubview:removethree];
        [self.view addSubview:thirdview1];
        
        oneview.hidden=NO;
        secondview.hidden=NO;
        thirdview.hidden=NO;
        j=1;
    }
    
    else
    {
        oneview1 =[[UIImageView alloc] initWithFrame:(CGRectMake(10, 480, 75, 75))];
        oneview1.image=[UIImage imageNamed:@"shadow21.png"];
        oneview1.transform = CGAffineTransformMakeScale(0.5,0.5);
        oneview1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            oneview1.alpha = 1;
            oneview1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        removeone = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        removeone.tintColor = [UIColor blackColor];
        [removeone addTarget:self
                      action:@selector(remove:) forControlEvents:UIControlEventTouchUpInside];
        
        [removeone setImage:[UIImage imageNamed:@"cancel-1.png"] forState:UIControlStateNormal];
        removeone.frame = CGRectMake(65,480,20,20);
        [self.view addSubview:removeone];
        [self.view addSubview:oneview1];
        
        
        
        secondview1 =[[UIImageView alloc] initWithFrame:(CGRectMake(120, 480, 75, 75))];
        secondview1.image=[UIImage imageNamed:@"shadow21.png"];
        secondview1.transform = CGAffineTransformMakeScale(0.5,0.5);
        secondview1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            secondview1.alpha = 1;
            secondview1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        removetwo = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [removetwo addTarget:self
                      action:@selector(remove1:)
            forControlEvents:UIControlEventTouchUpInside];
        removetwo.tintColor = [UIColor blackColor];
        [removetwo setImage:[UIImage imageNamed:@"cancel-1.png"] forState:UIControlStateNormal];
        
        removetwo.frame = CGRectMake(175,480,20,20);
        [self.view addSubview:removetwo];
        [self.view addSubview:secondview1];
        
        
        
        thirdview1 =[[UIImageView alloc] initWithFrame:(CGRectMake(230, 480, 75, 75))];
        thirdview1.image=[UIImage imageNamed:@"shadow21.png"];
        thirdview1.transform = CGAffineTransformMakeScale(0.5,0.5);
        thirdview1.alpha = 1;
        [UIView animateWithDuration:1.2 animations:^{
            thirdview1.alpha = 1;
            thirdview1.transform = CGAffineTransformMakeTranslation(0.7,0.7);
        }];
        removethree = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [removethree addTarget:self
                        action:@selector(remove2:)
              forControlEvents:UIControlEventTouchUpInside];
        removethree.tintColor = [UIColor blackColor];
        [removethree setImage:[UIImage imageNamed:@"cancel-1.png"] forState:UIControlStateNormal];
        
        removethree.frame = CGRectMake(285,480,20,20);
        [self.view addSubview:removethree];
        [self.view addSubview:thirdview1];
        
        oneview.hidden=NO;
        secondview.hidden=NO;
        thirdview.hidden=NO;
        
        j=1;
    }
}

- (void) shakeToShow:(UIView*)aView{
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = 0.5;
    
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [aView.layer addAnimation:animation forKey:nil];
}
#pragma -mark Orietation methods
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
        return YES;
}

- (BOOL)shouldAutorotate  // iOS 6 autorotation fix
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations // iOS 6 autorotation fix
{
       return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation // iOS 6 autorotation fix
{
    return UIInterfaceOrientationPortrait;
}


-(IBAction)btnbackclick:(id)sender
{
    NSString *val=[[Singleton sharedSingleton]getMyvalue];
    
    
    
    if(tag11==0)
    {
        if([val isEqualToString:@"0"])
        {
            oneview1.hidden=YES;
            secondview1.hidden=YES;
            thirdview1.hidden=YES;
            oneview.hidden=YES;
            secondview.hidden=YES;
            thirdview.hidden=YES;
            [bgView removeFromSuperview];
            
            [background removeFromSuperview];
            [img removeFromSuperview];
            [border removeFromSuperview];
            
        [self.navigationController popViewControllerAnimated:YES];
        }
        
        if([val isEqualToString:@"2"])
        {
            oneview1.hidden=YES;
            secondview1.hidden=YES;
            thirdview1.hidden=YES;
            oneview.hidden=YES;
            secondview.hidden=YES;
            thirdview.hidden=YES;
            [bgView removeFromSuperview];
            
            [background removeFromSuperview];
            [img removeFromSuperview];
            [border removeFromSuperview];
            
            [self.navigationController popViewControllerAnimated:YES];
        }


        
        
        if([val isEqualToString:@"1"])
        {
            [self.navigationController pushViewController:choneobj animated:YES];
        }
        
    }
    else if(tag11==1)
    {
        [self.navigationController pushViewController:choneobj animated:YES];
    }
    else
    {
        oneview1.hidden=YES;
        secondview1.hidden=YES;
        thirdview1.hidden=YES;
        oneview.hidden=YES;
        secondview.hidden=YES;
        thirdview.hidden=YES;
        [bgView removeFromSuperview];
    
        [background removeFromSuperview];
        [img removeFromSuperview];
        [border removeFromSuperview];
        [btn removeFromSuperview];
         [btn1 removeFromSuperview];
        scrollobj.contentSize = CGSizeMake(0, 800);
       
        tag11=0;
}
    removeone.hidden=YES;
    removetwo.hidden=YES;
    removethree.hidden=YES;
    
    button.hidden=YES;
 
    oneview.image=nil;
    secondview.image=nil;
    thirdview.image=nil;
    
    
    j=0;
    
    [dayarray removeAllObjects];
    [nightarray removeAllObjects];
    
}

-(IBAction)done:(id)sender
{
   
    
    
    if (arrtag1==0)
    {
        [[Singleton sharedSingleton]setdayarr:dayarray];
        [[Singleton sharedSingleton]setnightarr:nightarray];
    }
    else
    {
        [[Singleton sharedSingleton]setdayarr:getarr];
        [[Singleton sharedSingleton]setnightarr:nightarray];
    }
    

    if (oneview.image==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SELECT STYLE" message:@"Please Select 3 Styles" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if(secondview.image==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SELECT STYLE" message:@"Please Select 3 Styles" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    
    }
    else if(thirdview.image==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SELECT STYLE" message:@"Please Select 3 Styles" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    else{
        
        [[Singleton sharedSingleton]setMycheck:@"0"];
        tag11=1;
        [self.navigationController pushViewController:choneobj animated:YES];
        btn.hidden=YES;
        btn1.hidden=YES;
        removeone.hidden=YES;
        removetwo.hidden=YES;
        removethree.hidden=YES;
        [oneview1 removeFromSuperview];
        [secondview1 removeFromSuperview];
        [thirdview1 removeFromSuperview];
        [bgView removeFromSuperview];
        arrtag1=1;
        
        
    }

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (tag==0)
    {
        if(buttonIndex==0)
        {
        
        
        }
        else
        {
        
        }
        
    }
    else if(tag==1)
    {
        if(buttonIndex==0)
        {
           
            
        }
        else
        {
            
            
        }
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
