//
//  indicator.m
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "indicator.h"
#import "DejalActivityView.h"

@interface indicator ()

@end

@implementation indicator
@synthesize anywearobj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    
    anywearobj=[[anywear alloc]initWithNibName:@"anywear" bundle:nil];
    

    
    
    UIView *viewToUse = self.view;
    viewToUse = self.navigationController.navigationBar.superview;
    [DejalBezelActivityView activityViewForView:viewToUse];
    
    [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:5.0];
    }
    else{
    
        anywearobj=[[anywear alloc]initWithNibName:@"anyweariphone" bundle:nil];
        
        UIView *viewToUse = self.view;
        viewToUse = self.navigationController.navigationBar.superview;
        [DejalBezelActivityView activityViewForView:viewToUse];
        
        [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:5.0];
    }
    
}
- (void)removeActivityView;
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    [[self class] cancelPreviousPerformRequestsWithTarget:self];
    
    
    [self.navigationController pushViewController:anywearobj animated:YES];
    
}


    // Do any additional setup after loading the view from its nib.


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
