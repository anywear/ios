//
//  anywear.h
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "map.h"
#import "Aboutshop.h"
#import "search.h"
#import "myprofile.h"
#import "explore.h"

@class explore;
@class map;

@interface anywear : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UIView *viewObj;
    
    UIButton *btngalary;
    UIButton *btngalary1;
    
    NSMutableArray *arrData;
    UITableView *tblobj;
    map *mapobj;
    Aboutshop *aboutobj;
    search *searchobj;
    myprofile *myprofileobj;
    
    UIScrollView *scrollobj;
    
    UITableView *storetbl;
    NSMutableArray *image2;
    
    UIImageView *imgpic;
    
    NSMutableArray *addressarray;
    NSMutableArray *pricearray;
    
    explore *newex;
    
    NSMutableArray *daystyle;
    NSMutableArray *nightstyle;
    NSMutableArray *topsz;
    NSMutableArray *botmsz;
    NSMutableArray *locaarr;
    NSMutableArray *catarr;
    
    NSOperationQueue *_queue;



}
@property (retain) NSOperationQueue *queue;
@property (nonatomic,strong)  NSMutableArray *locaarr;
@property (nonatomic,strong)   NSMutableArray *catarr;
@property (nonatomic,strong) NSMutableArray *topsz;
@property (nonatomic,strong) NSMutableArray *botmsz;
@property (nonatomic,strong) NSMutableArray *daystyle;
@property (nonatomic,strong) NSMutableArray *nightstyle;
@property (nonatomic,strong) explore *newex;
@property (nonatomic,strong) NSMutableArray *pricearray;
@property (nonatomic,strong) NSMutableArray *image2;
@property (nonatomic,strong) IBOutlet UIImageView *imgpic;
@property (nonatomic,strong) NSMutableArray *addressarray;
@property (nonatomic,strong) IBOutlet UITableView *storetbl;


@property (nonatomic,strong) IBOutlet UIScrollView *scrollobj;
@property (nonatomic,strong)  search *searchobj;
@property (nonatomic,strong)   myprofile *myprofileobj;

@property (nonatomic,strong)IBOutlet UIView *viewObj;
@property (nonatomic,strong)IBOutlet UIButton *btngalary;
@property (nonatomic,strong)IBOutlet UIButton *btngalary1;
@property(strong,nonatomic) IBOutlet UITableView *tblobj;
@property (nonatomic,strong)NSMutableArray *arrData;
@property (nonatomic,strong) map *mapobj;
@property (nonatomic,strong) Aboutshop *aboutobj;


-(IBAction)btngalaryclick:(id)sender;
-(IBAction)btngalary1click:(id)sender;
-(IBAction)map:(id)sender;
-(IBAction)anywearbtn:(id)sender;
-(IBAction)shop:(id)sender;
-(IBAction)search:(id)sender;
-(IBAction)online:(id)sender;
-(IBAction)nearyou:(id)sender;








@end
