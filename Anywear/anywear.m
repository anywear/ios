//
//  anywear.m
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "anywear.h"
#import "SampleCell.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"
#import "storedetailcell.h"
#import "DejalActivityView.h"
#import "ASIFormDataRequest.h"



@interface anywear ()

@end

@implementation anywear
@synthesize viewObj,tblobj,arrData,btngalary,btngalary1,mapobj,aboutobj,searchobj,myprofileobj,scrollobj,storetbl,image2,imgpic,addressarray,pricearray,newex,daystyle,nightstyle,topsz,botmsz,locaarr,catarr;


@synthesize queue = _queue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.queue = [[NSOperationQueue alloc] init];
    
    if (g_IS_IPHONE_4_SCREEN)
    {
        UIView *viewToUse = self.view;
        viewToUse = self.navigationController.navigationBar.superview;
        [DejalBezelActivityView activityViewForView:viewToUse];
        
        
        
        //style
        daystyle=[[Singleton sharedSingleton]getdayarr];
        NSString *day =[NSString stringWithFormat:@"%@", daystyle];
        NSLog(@"day=%@",day);
        
        NSString *s = [day stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"day=%@",s);
        NSString *s1 = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"day=%@",s1);
        NSString *s2=[[s1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"day=%@",s2);
        
        
        
        nightstyle=[[Singleton sharedSingleton]getnightarr];
        NSString *night =[NSString stringWithFormat:@"%@",nightstyle];
        NSLog(@"night=%@",night);
        
        NSString *t = [night stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"night=%@",t);
        NSString *t1 = [t stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"night=%@",t1);
        NSString *t2=[[t1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"night=%@",t2);
        
        
        
        //size
        topsz=[[Singleton sharedSingleton]getsize];
        NSString *top =[NSString stringWithFormat:@"%@",topsz];
        NSLog(@"top size=%@",top);
        
        NSString *u = [top stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"top=%@",u);
        NSString *u1 = [u stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"top=%@",u1);
        NSString *u2=[[u1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"top=%@",u2);
        
        
        botmsz=[[Singleton sharedSingleton]getbsize];
        NSString *bottom =[NSString stringWithFormat:@"%@",botmsz];
        NSLog(@"bottom size=%@",bottom);
        
        NSString *v = [bottom stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"botom=%@",v);
        NSString *v1 = [v stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"bottom=%@",v1);
        NSString *v2=[[v1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"bottom=%@",v2);
        
        //money
        
        NSString *money=[[Singleton sharedSingleton]getstrspmoney];
        NSLog(@"money =%@",money);
        
        
        //location
        
        locaarr=[[Singleton sharedSingleton]getarrloc];
        NSString *loc =[NSString stringWithFormat:@"%@",locaarr];
        NSLog(@"location=%@",loc);
        
        NSString *w = [loc stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"location=%@",w);
        NSString *w1 = [w stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"location=%@",w1);
        NSString *w2=[[w1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"location=%@",w2);
        
        
        //category
        
        catarr=[[Singleton sharedSingleton]getarrcat];
        NSString *cat =[NSString stringWithFormat:@"%@",catarr];
        NSLog(@"category=%@",cat);
        
        NSString *x = [cat stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"category=%@",x);
        NSString *x1 = [x stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"category=%@",x1);
        NSString *x2=[[x1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"category=%@",x2);
       
        imgpic.hidden=YES;
        
        [scrollobj setScrollEnabled:YES];
        [scrollobj setContentSize:CGSizeMake(320,550)];

    newex=[[explore alloc]initWithNibName:@"explore" bundle:nil];
    mapobj=[[map alloc]initWithNibName:@"map" bundle:nil];
    aboutobj=[[Aboutshop alloc]initWithNibName:@"Aboutshop" bundle:nil];
    searchobj=[[search alloc]initWithNibName:@"search" bundle:nil];
    myprofileobj=[[myprofile alloc]initWithNibName:@"myprofile" bundle:nil];
        
    btngalary1.hidden=YES;
    arrData =[[NSMutableArray alloc] init];
    addressarray =[[NSMutableArray alloc] init];
    pricearray =[[NSMutableArray alloc] init];
        
    [arrData addObject:@"Explore"];
    [arrData addObject:@"Change Personal Settings"];
    [arrData addObject:@"Boutiqall"];
    [arrData addObject:@"Notifications"];
    [arrData addObject:@"My Profile"];
    [arrData addObject:@"Sign Out"];
        
        
        
        image2=[[NSMutableArray alloc]init];
        
        
       // [self RegisterCall];
       // ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:@"http://192.168.1.41/php/anywear_api/webservice?action=getSearchStore"]];
        
        ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:@"http://api.boutiqall.com/webservice?action=getSearchStore"]];
        
        request.tag =1;
        [request setUseKeychainPersistence:YES];
        [request addPostValue:s2 forKey:@"dayStyle"];
        [request addPostValue:t2 forKey:@"nightStyle"];
        [request addPostValue:u2 forKey:@"storeTopSize"];
        [request addPostValue:v2 forKey:@"storeBottomSize"];
        [request addPostValue:money forKey:@"moneySpend"];
        [request addPostValue:w2 forKey:@"storeLocations"];
        [request addPostValue:x2 forKey:@"storeCategory"];
        [request setDelegate:self];
        [_queue addOperation:request];
        
        
               
    }
    else{
    
        UIView *viewToUse = self.view;
        viewToUse = self.navigationController.navigationBar.superview;
        [DejalBezelActivityView activityViewForView:viewToUse];
        
        
        //style
        daystyle=[[Singleton sharedSingleton]getdayarr];
        NSString *day =[NSString stringWithFormat:@"%@", daystyle];
        NSLog(@"day=%@",day);
        
        NSString *s = [day stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"day=%@",s);
        NSString *s1 = [s stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"day=%@",s1);
        NSString *s2=[[s1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"day=%@",s2);
        
        
        
        nightstyle=[[Singleton sharedSingleton]getnightarr];
        NSString *night =[NSString stringWithFormat:@"%@",nightstyle];
        NSLog(@"night=%@",night);
        
        NSString *t = [night stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"night=%@",t);
        NSString *t1 = [t stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"night=%@",t1);
        NSString *t2=[[t1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"night=%@",t2);
        
        
        
        //size
        topsz=[[Singleton sharedSingleton]getsize];
        NSString *top =[NSString stringWithFormat:@"%@",topsz];
        NSLog(@"top size=%@",top);
        
        NSString *u = [top stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"top=%@",u);
        NSString *u1 = [u stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"top=%@",u1);
        NSString *u2=[[u1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"top=%@",u2);
        
        
        botmsz=[[Singleton sharedSingleton]getbsize];
        NSString *bottom =[NSString stringWithFormat:@"%@",botmsz];
        NSLog(@"bottom size=%@",bottom);
        
        NSString *v = [bottom stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"botom=%@",v);
        NSString *v1 = [v stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"bottom=%@",v1);
        NSString *v2=[[v1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"bottom=%@",v2);
        
        //money
        
        NSString *money=[[Singleton sharedSingleton]getstrspmoney];
        NSLog(@"money =%@",money);
        
        
        //location
        
        locaarr=[[Singleton sharedSingleton]getarrloc];
        NSString *loc =[NSString stringWithFormat:@"%@",locaarr];
        NSLog(@"location=%@",loc);
        
        NSString *w = [loc stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"location=%@",w);
        NSString *w1 = [w stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"location=%@",w1);
        NSString *w2=[[w1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"location=%@",w2);
        
        
        //category
        
        catarr=[[Singleton sharedSingleton]getarrcat];
        NSString *cat =[NSString stringWithFormat:@"%@",catarr];
        NSLog(@"category=%@",cat);
        
        NSString *x = [cat stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSLog(@"category=%@",x);
        NSString *x1 = [x stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        NSLog(@"category=%@",x1);
        NSString *x2=[[x1 stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSLog(@"category=%@",x2);

        imgpic.hidden=YES;

        [scrollobj setScrollEnabled:YES];
        [scrollobj setContentSize:CGSizeMake(320,650)];
        

        newex=[[explore alloc]initWithNibName:@"exploreiphone" bundle:nil];
        mapobj=[[map alloc]initWithNibName:@"mapiphone" bundle:nil];
        aboutobj=[[Aboutshop alloc]initWithNibName:@"aboutshopiphone" bundle:nil];
        
         searchobj=[[search alloc]initWithNibName:@"searchiphone" bundle:nil];
        myprofileobj=[[myprofile alloc]initWithNibName:@"myprofileiphone" bundle:nil];

        
        btngalary1.hidden=YES;
        arrData =[[NSMutableArray alloc] init];
        [arrData addObject:@"Explore"];
        [arrData addObject:@"Change Personal Settings"];
        [arrData addObject:@"Boutiqall"];
        [arrData addObject:@"Notifications"];
        [arrData addObject:@"My Profile"];
         [arrData addObject:@"Sign Out"];
        
        
        image2=[[NSMutableArray alloc]init];
        addressarray =[[NSMutableArray alloc] init];
        pricearray =[[NSMutableArray alloc] init];
        
        
        
        //[self RegisterCall];
        
       // ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:@"http://192.168.1.41/php/anywear_api/webservice?action=getSearchStore"]];
         ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:@"http://api.boutiqall.com/webservice?action=getSearchStore"]];
        request.tag =1;
        [request setUseKeychainPersistence:YES];
        [request addPostValue:s2 forKey:@"dayStyle"];
        [request addPostValue:t2 forKey:@"nightStyle"];
        [request addPostValue:u2 forKey:@"storeTopSize"];
        [request addPostValue:v2 forKey:@"storeBottomSize"];
        [request addPostValue:money forKey:@"moneySpend"];
        [request addPostValue:w2 forKey:@"storeLocations"];
        [request addPostValue:x2 forKey:@"storeCategory"];
        [request setDelegate:self];
        [_queue addOperation:request];
        
     
        
    }
    
}


- (void)requestFinished:(ASIHTTPRequest *)response {
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSDictionary *dictResult =(NSDictionary *)result;
    NSLog(@"re=%@",dictResult);
    
    NSMutableArray *dictResult1 =(NSMutableArray *)result;
        NSLog(@"re=%@",dictResult1);
    
      for (int i=0; i<[dictResult1 count]; i++)
        {
           NSDictionary *dict_steps=[dictResult1 objectAtIndex:i];
  
   
   
          NSString *img1=[dict_steps objectForKey:@"iStoreId"];
          NSLog(@"img== %@",img1);
  
          NSString *name=[dict_steps objectForKey:@"vStoreName"];
          NSLog(@"address== %@",name);
   
          NSString *address=[dict_steps objectForKey:@"tAddress"];
          NSLog(@"price== %@",address);

          NSString *price=[dict_steps objectForKey:@"eSign"];
          NSLog(@"price== %@",price);

            
          NSString *img2=[dict_steps objectForKey:@"vShopImage"];
          NSLog(@"img== %@",img2);

            
            
          UIImage* myImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:
                                                         [NSURL URLWithString:img2]]];
            
            imgpic.image=myImage;
            [image2 addObject:imgpic.image];
            [addressarray addObject:address];
            [pricearray addObject:price];
        }
    
     [storetbl reloadData];
    [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
    
}
- (void)removeActivityView;
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    [[self class] cancelPreviousPerformRequestsWithTarget:self];
    
}


-(void)viewWillAppear:(BOOL)animated
{

    btngalary.hidden=NO;
    btngalary1.hidden=YES;

}

-(IBAction)btngalaryclick:(id)sender;
{
    btngalary.hidden=YES;
    btngalary1.hidden=NO;
    viewObj.hidden=NO;
    
    
    
    [UIView animateWithDuration:0.10f delay:0.10f options:UIViewAnimationOptionTransitionFlipFromRight animations:^
     {
         viewObj.frame = CGRectMake(0,100, viewObj.bounds.size.width, viewObj.bounds.size.height);
         [self.view addSubview:viewObj];
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Animation is complete");
     }];
}
-(IBAction)btngalary1click:(id)sender
{
    btngalary.hidden=NO;
    btngalary1.hidden=YES;
    viewObj.hidden=NO;
  
    [UIView animateWithDuration:0.10f delay:0.10f options:UIViewAnimationOptionTransitionFlipFromRight animations:^
     {
         viewObj.frame = CGRectMake(320,100, viewObj.bounds.size.width, viewObj.bounds.size.height);
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Animation is complete");
     }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==tblobj)
    {
        return [arrData count];
    }
    else if (tableView==storetbl)
    {
        return [image2 count];
        return [addressarray count];
        return [pricearray count];
        
    }
    return YES;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView==storetbl)
    {
        
        NSString *cellIdentifre1 =@"storedetailcell";
        
        storedetailcell *cell1 =(storedetailcell *) [self.storetbl dequeueReusableCellWithIdentifier:cellIdentifre1];
        
        if (cell1==nil)
        {
            NSArray *arrNib1=[[NSBundle mainBundle] loadNibNamed:cellIdentifre1 owner:self options:nil];
            cell1= (storedetailcell *)[arrNib1 objectAtIndex:0];
            cell1.backgroundColor =[UIColor clearColor];
            cell1.selectionStyle=UITableViewCellSelectionStyleNone;
            
            cell1.address.text=[addressarray objectAtIndex:indexPath.row];
            cell1.price.text=[pricearray objectAtIndex:indexPath.row];

            cell1.choose.tag=indexPath.row;
            [cell1.choose addTarget:self action:@selector(btnDetailClick:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        if (image2.count==0)
        {
            NSLog(@"no Image");
        }
        else
        {
            cell1.imgphoto.image = [image2 objectAtIndex:indexPath.row];
        }
        
        
        return cell1;
        
    }
    
    else if(tableView==tblobj)
    {
        NSString *cellIdentifre =@"SampleCell";
        
        SampleCell *cell =(SampleCell *) [self.tblobj dequeueReusableCellWithIdentifier:cellIdentifre];
        
        if (cell==nil)
        {
            NSArray *arrNib=[[NSBundle mainBundle] loadNibNamed:cellIdentifre owner:self options:nil];
            cell= (SampleCell *)[arrNib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
        }
        
        
        cell.labelForPlace.text=[arrData objectAtIndex:indexPath.row];
        
        return cell;
    }
    return NO;
}

-(IBAction)btnDetailClick:(id)sender
{
    
     //UIButton * btn = (UIButton *)sender;
     [self.navigationController pushViewController:aboutobj animated:YES];
 
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row==0)
    {
//        NSArray *arr=self.navigationController.viewControllers;
//        [self.navigationController popToViewController:[arr objectAtIndex:2] animated:YES];
      
        [self.navigationController pushViewController:newex animated:YES];
    }
    else if (indexPath.row==1)
    {
    
    
    }
    else if (indexPath.row==2)
    {
        
        viewObj.hidden=YES;
        btngalary.hidden=NO;
        btngalary1.hidden=YES;

    }
    else if (indexPath.row==3)
    {
    
    }
    else if (indexPath.row==4)
    {
        [self.navigationController pushViewController:myprofileobj animated:YES];
        
        
    }
    else if (indexPath.row==5)
    {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"uname"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"pass"];
        viewObj.hidden=YES;

        [self.navigationController popToRootViewControllerAnimated:YES];
    }

    
}
-(IBAction)shop:(id)sender
{
 //[self.navigationController pushViewController:aboutobj animated:YES];
}


-(IBAction)map:(id)sender
{
    [self.navigationController pushViewController:mapobj animated:YES];
}
-(IBAction)anywearbtn:(id)sender
{
    
    NSArray *arr=self.navigationController.viewControllers;
    [self.navigationController popToViewController:[arr objectAtIndex:1] animated:YES];

}
-(IBAction)search:(id)sender
{

  [self.navigationController pushViewController:searchobj animated:YES];
}

-(IBAction)online:(id)sender
{


}
-(IBAction)nearyou:(id)sender
{



}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
