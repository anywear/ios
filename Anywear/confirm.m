//
//  confirm.m
//  Anywear
//
//  Created by Pradip on 8/14/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "confirm.h"

@interface confirm ()

@end

@implementation confirm
@synthesize shopobj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    shopobj=[[shopping alloc]initWithNibName:@"shopping" bundle:nil];
    }
    else{
    
        shopobj=[[shopping alloc]initWithNibName:@"shoppingiphone" bundle:nil];

    
    }
}

-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)next:(id)sender
{
    [self.navigationController pushViewController:shopobj animated:YES];


}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
