//
//  CollectionMenuCell.h
//  GrideView
//
//  Created by Parth Devmorari - PC on 9/23/14.
//  Copyright (c) 2014 Parth Devmorari - PC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionMenuCell : UICollectionViewCell

@property(strong,nonatomic) IBOutlet UIButton *btnone;
@property(strong,nonatomic) IBOutlet UIImageView *img;
@end
