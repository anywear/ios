//
//  step.h
//  Anywear
//
//  Created by Pradip on 8/12/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "choosestyleone.h"
#import "ALScrollViewPaging.h"

@class choosestyleone;
@interface step : UIViewController
{
   UIScrollView *scrollobj;
    int tag;
    UIImageView *oneview1;
    UIImageView *oneview;
    UIImageView *secondview;
    UIImageView *secondview1;
    UIImageView *thirdview;
    UIImageView *thirdview1;
    
    UIButton *button;

    UIImageView *imgView;
    UIImageView *zoomimg;

    
    int tag1;
    int i;
    choosestyleone *choneobj;
    
    int redius;
    int btntag;
    
    UIButton *removeone;
    UIButton *removetwo;
    UIButton *removethree;
    
    
    
    UIImageView *oneview2;
    UIImageView *secondview2;
    UIImageView *thirdview2;

    
    int j;
    int remove;
    
    
    
    //new UICollectionView
    UICollectionView *collectionObj;
    UIImageView *imgpic;
    UIImageView *imgView1;
    int arrtag;
    int coutimg;
    int msgtag;
    NSString *getctgrid;
    int tag11;
    UIView *bgview;
    
    
    
 //imageid
    
    NSMutableArray *prsnlsize;
    UILabel *styleid;
    UILabel *nstyleid;

    NSMutableArray *dayarray;
    NSMutableArray *nightarray;
    
    NSString *myString1;
    NSString *myString2;
    NSString *myString3;
    
    NSString *myString4;
    NSString *myString5;
    NSString *myString6;
    
    
    int *arrtag1;
    NSMutableArray *getarr;
    
    
    
}
@property (nonatomic) int tag11;
@property (retain, nonatomic) NSString *getctgrid;
@property (nonatomic) int msgtag;
@property (nonatomic)  int remove;
@property (nonatomic) int j;
@property (nonatomic) int redius;
@property (nonatomic) int i;
@property (nonatomic)int tag;
@property (nonatomic)int tag1;
@property (nonatomic)int btntag;

@property (retain, nonatomic) UIImageView *zoomimg;

@property (retain, nonatomic) UIButton *button;
@property (retain, nonatomic) UIButton *removeone;
@property (retain, nonatomic) UIButton *removetwo;
@property (retain, nonatomic) UIButton *removethree;
@property (strong, nonatomic) UIView *bgview;




@property (retain, nonatomic) UIImageView *imgView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollobj;

@property (retain, nonatomic) UIImageView *oneview1;
@property (retain, nonatomic) UIImageView *oneview;

@property (retain, nonatomic) UIImageView *secondview;
@property (retain, nonatomic) UIImageView *secondview1;

@property (retain, nonatomic) UIImageView *thirdview;
@property (retain, nonatomic) UIImageView *thirdview1;


@property (retain, nonatomic) choosestyleone *choneobj;


@property (retain, nonatomic) IBOutlet UIImageView *Picture;
@property (retain, nonatomic) IBOutlet UIImageView *Picture1;
@property (retain, nonatomic) IBOutlet UIImageView *Picture2;
@property (retain, nonatomic) IBOutlet UIImageView *Picture3;
@property (retain, nonatomic) IBOutlet UIImageView *Picture4;
@property (retain, nonatomic) IBOutlet UIImageView *Picture5;
@property (retain, nonatomic) IBOutlet UIImageView *Picture6;
@property (retain, nonatomic) IBOutlet UIImageView *Picture7;
@property (retain, nonatomic) IBOutlet UIImageView *Picture8;
@property (retain, nonatomic) IBOutlet UIImageView *Picture9;


@property (retain, nonatomic) UIImageView *oneview2;
@property (retain, nonatomic) UIImageView *secondview2;
@property (retain, nonatomic) UIImageView *thirdview2;
@property (retain, nonatomic) NSMutableArray *prsnlsize;
@property (retain, nonatomic) IBOutlet  UILabel *styleid;
@property (retain, nonatomic) IBOutlet  UILabel *nstyleid;
@property (retain, nonatomic) NSMutableArray *dayarray;
@property (retain, nonatomic) NSMutableArray *nightarray;



@property (retain, nonatomic) NSString *myString1;
@property (retain, nonatomic) NSString *myString2;
@property (retain, nonatomic) NSString *myString3;

@property (retain, nonatomic) NSString *myString4;
@property (retain, nonatomic) NSString *myString5;
@property (retain, nonatomic) NSString *myString6;




-(IBAction)btnbackclick:(id)sender;
-(IBAction)done:(id)sender;




-(void)setRedius:(int)red;



//new UICollectionView
@property(strong, nonatomic) IBOutlet UIImageView *imgpic;
@property(strong, nonatomic)IBOutlet UICollectionView *collectionObj;
@property(strong, nonatomic) NSMutableArray *arrMenus;
@property(strong, nonatomic) NSMutableArray *arrimg;
@property(strong, nonatomic) UIImageView *imgView1;
@property(nonatomic) int arrtag;
@property(nonatomic) int coutimg;

@property(nonatomic) int *arrtag1;
@property(strong, nonatomic) NSMutableArray *getarr;
@end
