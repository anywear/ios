//
//  login.m
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "login.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"
#import "ASIFormDataRequest.h"
#import "DejalActivityView.h"

@interface login ()

@end

@implementation login
@synthesize styleobj,username,password,scroll,suc,tag,forgotpass,frpass,chk;
@synthesize queue = _queue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (g_IS_IPHONE_4_SCREEN)
    {
    
        forgotpass.hidden=YES;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"7" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"7" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-7" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-7" object:nil];
        
        self.queue = [[NSOperationQueue alloc] init];

        styleobj=[[style alloc]initWithNibName:@"style" bundle:nil];
    }
    
    else
        
    {
        forgotpass.hidden=YES;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"7" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"7" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-7" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-7" object:nil];
        
        self.queue = [[NSOperationQueue alloc] init];

    
        styleobj=[[style alloc]initWithNibName:@"styleiphone" bundle:nil];

    
    }
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    username.text=@"";
    password.text=@"";
}


-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==username)
    {
        [textField setKeyboardType:UIKeyboardTypeEmailAddress];
        
        scroll.contentOffset=CGPointMake(0, 0);
    }
    else if(textField==password)
        
    {
        
        scroll.contentOffset=CGPointMake(0, 50);
    }
    else if(textField==frpass)
        
    {
        
        scroll.contentOffset=CGPointMake(0, 50);
    }

    
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField==username)
    {
        [username resignFirstResponder];
        scroll.contentOffset=CGPointMake(0, 0);
    }
    else if (textField==password)
    {
        [password resignFirstResponder];
        scroll.contentOffset=CGPointMake(0, 0);
    }
    else if (textField==frpass)
    {
        [frpass resignFirstResponder];
        scroll.contentOffset=CGPointMake(0, 0);
    }
    return YES;
}

-(IBAction)btnnext:(id)sender

{
    chk=0;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *user1 = self.username.text;
    NSString *password1 = self.password.text;
    [userDefaults setObject:user1 forKey:@"uname"];
    [userDefaults setObject:password1 forKey:@"pass"];
    [userDefaults synchronize];

    
    if ([username.text isEqualToString:@""] || username.text==nil )
    {
        UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Please Enter Your Email Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        tag=0;

        [alt1 show];
        return;
    }
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if ([emailTest evaluateWithObject:username.text] != YES && [username.text length]!=0)
    {
        UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Please Enter a Valid Email Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        tag=0;

        [alt1 show];
        return;
    }
    else if ([password.text isEqualToString:@""] || password.text==nil)
    {
        UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Password Error" message:@"Please Enter Your Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alt1 show];
        tag=0;

        return;
    }
    NSString *uname=self.username.text;
    NSString *pwd = self.password.text;
    NSString *post = [NSString stringWithFormat:@"vEmail=%@&vPassword=%@",uname,pwd];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getUserLogin"]]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    if(conn)
    {
        NSLog(@"Connection Successful");
    }
    else
    {
        NSLog(@"Connection could not be made");
    }
    
    UIView *viewToUse = self.view;
    viewToUse = self.navigationController.navigationBar.superview;
    [DejalBezelActivityView activityViewForView:viewToUse];


 //[self.navigationController pushViewController:styleobj animated:YES];

}
- (void)requestFinished:(ASIHTTPRequest *)response
{
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    NSDictionary *dictResult =(NSDictionary *)result;
    NSLog(@"re=%@",dictResult);
    
}
- (void)requestFailed:(ASIHTTPRequest *)response {
}
- (void) registerCallComplete : (NSNotification *)notification
{
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSDictionary *dictResult =(NSDictionary *)result;
    NSLog(@"re=%@",dictResult);
}
- (void) registerCallFail : (NSNotification *)notification {
}


#pragma mark Login Call -
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    if(chk==0)
    {
        NSLog(@"data=%@",data);
        NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"User Date=%@",str);
    
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"Result = %@",result);
    
    
        suc = [result objectForKey:@"message"];
        NSLog(@"Mess::%@",suc);
    
    
        NSDictionary *data1=[result objectForKey:@"data"];
        NSLog(@"data1=%@",data1);

    
        NSMutableArray *arr=[[NSMutableArray alloc] init];
    
        arr=[NSMutableArray arrayWithObject:data1];
    
        for (int i; i<[arr count]; i ++)
        {
            NSLog(@"mss=%@",arr);
        }
   
        if ([suc isEqualToString:@"Client Login Successfully"])
        {
            UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Login" message:@"Logged in Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alt1 show];
            tag=1;
            return;
        }
        else if ([suc isEqualToString:@"Check Your E-Mail Address and Password."])
        {
            UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Login Error" message:@"Please check your email id or password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alt1 show];
            tag=0;
        }
    }
    
    else if(chk==1)
    {
        NSLog(@"data=%@",data);
        NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"User Date=%@",str);
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"Result = %@",result);
        
        
        suc = [result objectForKey:@"message"];
        NSLog(@"Mess::%@",suc);

        if ([suc isEqualToString:@"New login detail is send to your email"])
        {
            
            
            UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Forgot Password" message:@"Please Check your Email Id to Reset Your Password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alt1 show];
            [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
            
            return;
            
            frpass.text=@"";

            
            
        }

    }
   
   

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(tag==0)
    {
        if (buttonIndex==0)
        {
            
            
        }
    }
    else if(tag==1)
    {
        if (buttonIndex==0)
        {
            [self.navigationController pushViewController:styleobj animated:YES];
        }
    }

}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription],[[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Intenet Connection Error" message:@"Please Check Your Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    return;
    [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"hello1");
    [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
    
}

- (void)removeActivityView;
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    [[self class] cancelPreviousPerformRequestsWithTarget:self];
}

-(IBAction)forgotpasswrd:(id)sender
{
    
    [self showAnimate];


}
- (void)showAnimate
{
    forgotpass.hidden=NO;
    forgotpass.transform = CGAffineTransformMakeScale(2.3, 2.3);
    forgotpass.alpha = 0;
    [UIView animateWithDuration:.30 animations:^{
        forgotpass.alpha = 1;
        forgotpass.transform = CGAffineTransformMakeScale(1, 1);

    }];
    [self.view addSubview:forgotpass];


}
-(IBAction)cancel:(id)sender
{
    forgotpass.hidden=YES;

}

-(IBAction)sendnow:(id)sender
{

    
    chk=1;
    
    UIView *viewToUse = self.view;
    viewToUse = self.navigationController.navigationBar.superview;
    [DejalBezelActivityView activityViewForView:viewToUse];
    
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if ([emailTest evaluateWithObject:frpass.text] != YES && [frpass.text length]!=0)
    {
        UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Please Enter a Valid Email Address" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
        
        [alt1 show];
        return;
    }
    
    if ([frpass.text isEqualToString:@""] || [frpass.text isEqualToString:nil])
    {
        UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Please Enter Your Email Address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
        [alt1 show];
        return;
        
        frpass.text=@"";
    }
    
    else
    {
        NSString *uname=self.frpass.text;
        NSString *post = [NSString stringWithFormat:@"vEmail=%@",uname];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%d",[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=forgotPassword"]]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        if(conn)
        {
            NSLog(@"Connection Successful");
        }
        else
        {
            NSLog(@"Connection could not be made");
        }
        
        
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
