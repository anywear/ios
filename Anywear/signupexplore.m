//
//  signupexplore.m
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "signupexplore.h"

@interface signupexplore ()

@end

@implementation signupexplore
@synthesize styleobj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Anywear is currently only available for women. Leave us you email and we will notify you once the app is available for men too! \n Email:__________________" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Ok", nil];
        // tag=0;
        
        [alert show];
    styleobj=[[style alloc]initWithNibName:@"style" bundle:nil];
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Anywear is currently only available for women. Leave us you email and we will notify you once the app is available for men too! \n Email:__________________" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Ok", nil];
        // tag=0;
        
        [alert show];
    
     styleobj=[[style alloc]initWithNibName:@"styleiphone" bundle:nil];
    }

    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear
{

}
-(IBAction)btnback:(id)sender
{
   
}
-(IBAction)signup:(id)sender
{
[self.navigationController pushViewController:styleobj animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
