//
//  Slider.h
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "explore.h"
#import "ALScrollViewPaging.h"




@interface Slider : UIViewController
{

    UIButton *skip;
    explore *obj1;
    UIImageView *imgobj;
    NSMutableData *responseData;
    NSString *suc;
    int tag;
    
}

@property (nonatomic) int tag;
@property (nonatomic,strong) NSString *suc;
@property(nonatomic,strong) NSMutableData *responseData;
@property (nonatomic,strong)  UIImageView *imgobj;



@property (nonatomic,strong)  explore *obj1;
@property (nonatomic,strong) IBOutlet  UIButton *skip;


-(IBAction)skip:(id)sender;

@end
