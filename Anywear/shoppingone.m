//
//  shoppingone.m
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "shoppingone.h"
#import "shoponecell.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"
#import "DejalActivityView.h"

@interface shoppingone ()

@end

@implementation shoppingone
@synthesize stblobj,sarrdata,indicatorobj,btn,loc,categoryarray,categoryarray1;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (g_IS_IPHONE_4_SCREEN)
    {
    indicatorobj=[[indicator alloc]initWithNibName:@"indicator" bundle:nil];
    //stblobj.hidden=NO;
    sarrdata =[[NSMutableArray alloc] init];
        
      categoryarray =[[NSMutableArray alloc] init];
        categoryarray1 =[[NSMutableArray alloc] init];
        loc.hidden=YES;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"10" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"10" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-10" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-10" object:nil];
        
        
        [self RegisterCall];
        

    }
    else{
    
        indicatorobj=[[indicator alloc]initWithNibName:@"indicatoriphone" bundle:nil];
        //stblobj.hidden=NO;
        sarrdata =[[NSMutableArray alloc] init];
        categoryarray =[[NSMutableArray alloc] init];
        categoryarray1 =[[NSMutableArray alloc] init];

    
        
        loc.hidden=YES;

        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"10" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"10" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-10" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-10" object:nil];
        
        
        [self RegisterCall];
        
        
        
    }
    
    // Do any additional setup after loading the view from its nib.
}

-(void)RegisterCall {
    
    
    NSString *myRequestString = [NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getStoreCategory"];
    
    [[HttpQueue sharedSingleton] getItems:myRequestString:10];
}

- (void) registerCallComplete : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSMutableArray *dictResult =(NSMutableArray *)result;
    NSLog(@"re=%@",dictResult);
    
    for (int i=0; i<[dictResult count]; i++)
    {
        NSDictionary *dict_steps=[dictResult objectAtIndex:i];
        
        NSString *address=[dict_steps objectForKey:@"vStoreCategoryName"];
        NSLog(@"address== %@",address);
        [sarrdata addObject:address];
        
        
        NSString *catid=[dict_steps objectForKey:@"iStoreCategoryId"];
        NSLog(@"address== %@",catid);
        [categoryarray addObject:catid];
        
        
    }
    
    [stblobj reloadData];
}

- (void) registerCallFail : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Fail=%@",dict);
    return;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [sarrdata count];
    return [categoryarray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell=nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
//        cell.textLabel.text=[sarrdata objectAtIndex:indexPath.row];
//        cell.textLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
        
        UILabel *lbl1 =  [[UILabel alloc] initWithFrame: CGRectMake(10,0,320,50)];
        [lbl1 setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:19]];
        [lbl1 setTextColor:[UIColor blackColor]];
        lbl1.text =[sarrdata objectAtIndex:indexPath.row];
        [cell addSubview:lbl1];
        [lbl1 release];
        
        UILabel *lbl2 =  [[UILabel alloc] initWithFrame: CGRectMake(0,0,320,50)];
        [lbl2 setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:19]];
        lbl2.hidden=YES;
        [lbl2 setTextColor:[UIColor blackColor]];
        lbl2.text =[categoryarray objectAtIndex:indexPath.row];
        [cell addSubview:lbl2];
        [lbl2 release];

        
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag=indexPath.row;
        button.frame = CGRectMake(0, 0, 320, 50);
        [button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        
        [button addTarget:self action:@selector(buttonaction:) forControlEvents:UIControlEventTouchDown];
        [cell.contentView addSubview:button];
        
    }
    return cell;

}
-(void)buttonaction:(id)sender
{
    
    btn=(UIButton *)sender;
    btn.frame = CGRectMake(0, 0, 310, 45);
    
    NSLog(@"btntag:%li",(long)btn.tag);
    
    if (btn.selected)
    {
        [btn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        NSString *tempstr=[NSString stringWithString:[sarrdata objectAtIndex:btn.tag]];
          NSLog(@"btntag:%@",tempstr);
        NSLog(@"remove");
        
        
        
        NSString *tempstr1=[NSString stringWithString:[categoryarray objectAtIndex:btn.tag]];
        NSLog(@"btntag:%@",tempstr1);
        NSLog(@"remove");
        
        btn.selected=FALSE;
    }
    else
    {
        [btn setImage:[UIImage imageNamed:@"pink_right.png"] forState:UIControlStateNormal];
        
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        
        btn.selected=TRUE;
        
        
        NSString *tempstr=[NSString stringWithString:[sarrdata objectAtIndex:btn.tag]];
        [sarrdata addObject:tempstr];
        
        
        NSString *tempstr1=[NSString stringWithString:[categoryarray objectAtIndex:btn.tag]];
        [categoryarray1 addObject:tempstr1];
         NSLog(@"id=%@",categoryarray1);
        loc.text=tempstr1;

    }
}


-(IBAction)btnDetailClick:(id)sender
{
}

-(IBAction)next:(id)sender
{
    
    [[Singleton sharedSingleton]setarrcat:categoryarray1];


    
//    if(btn==nil)
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SELECT CATEGORY" message:@"Please Select Category" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
//        [alert show];
//        
//    }
//    else
//    {
        [self.navigationController pushViewController:indicatorobj animated:YES];

//    }

    
     }
    
-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
