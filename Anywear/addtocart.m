//
//  addtocart.m
//  Anywear
//
//  Created by Pradip on 8/30/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "addtocart.h"

@interface addtocart ()

@end

@implementation addtocart
@synthesize scrollobj,chekoutobj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    [scrollobj setScrollEnabled:YES];
    [scrollobj setContentSize:CGSizeMake(320,680)];
        
    chekoutobj=[[checkout alloc]initWithNibName:@"checkout" bundle:nil];

    }
    else{
    
    [scrollobj setScrollEnabled:YES];
    [scrollobj setContentSize:CGSizeMake(320,780)];
   chekoutobj=[[checkout alloc]initWithNibName:@"checkoutiphone" bundle:nil];

    
    }

}
-(IBAction)checkout:(id)sender
{

    [self.navigationController pushViewController:chekoutobj animated:YES];

}
-(IBAction)btncntnu:(id)sender
{
    [self.navigationController pushViewController:chekoutobj animated:YES];


}
-(IBAction)back:(id)sender
{
//    NSArray *arr=self.navigationController.viewControllers;
//    [self.navigationController popToViewController:[arr objectAtIndex:15] animated:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
