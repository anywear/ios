//
//  mainexplorer.m
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "mainexplorer.h"
#import "SampleCell.h"

@interface mainexplorer ()

@end

@implementation mainexplorer
@synthesize img,viewObj,tblobj,arrData,imgbac,imgmapback,btngalary,btngalary1,start,start1,tag,obj;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"So you're ready to go shopping? We need to get to know you first" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Ok", nil];
    //   // tag=0;
    
        [alert show];

    
    obj=[[exploreinfo alloc]initWithNibName:@"exploreinfo" bundle:nil];
    // signobj=[[signup alloc]initWithNibName:@"signup" bundle:nil];
   // signupexploreobj=[[mainsignup alloc]initWithNibName:@"mainsignup" bundle:nil];
    
    btngalary1.hidden=YES;
   
    
    arrData =[[NSMutableArray alloc] init];
    [arrData addObject:@"Explore"];
    [arrData addObject:@"Change Personal Settings"];
    [arrData addObject:@"Boutiqall"];
    [arrData addObject:@"Notifications"];
    [arrData addObject:@"My Profile"];
     [arrData addObject:@"Sign Out"];
    }
    else{
    
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"So you're ready to go shopping? We need to get to know you first" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Ok", nil];
        //   // tag=0;
        
        [alert show];
        
        
        obj=[[exploreinfo alloc]initWithNibName:@"exploreinfo" bundle:nil];
        // signobj=[[signup alloc]initWithNibName:@"signup" bundle:nil];
        // signupexploreobj=[[mainsignup alloc]initWithNibName:@"mainsignup" bundle:nil];
        
        btngalary1.hidden=YES;
        
        
        arrData =[[NSMutableArray alloc] init];
        [arrData addObject:@"Explore"];
        [arrData addObject:@"Change Personal Settings"];
        [arrData addObject:@"Boutiqall"];
        [arrData addObject:@"Notifications"];
        [arrData addObject:@"My Profile"];
        [arrData addObject:@"Sign Out"];
    
    
    }

    // Do any additional setup after loading the view from its nib.
}
-(IBAction)btninfo:(id)sender
{
    [self.navigationController pushViewController:obj animated:YES];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{

    if (buttonIndex==1)
    {
        
        NSArray *arr=self.navigationController.viewControllers;
        [self.navigationController popToViewController:[arr objectAtIndex:3] animated:YES];
        
        
    }
}

-(IBAction)btngalaryclick:(id)sender;
{
    btngalary.hidden=YES;
    btngalary1.hidden=NO;
    [UIView animateWithDuration:0.10f delay:0.10f options:UIViewAnimationOptionTransitionFlipFromRight animations:^
     {
         viewObj.frame = CGRectMake(0,52, viewObj.bounds.size.width, viewObj.bounds.size.height);
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Animation is complete");
     }];
}
-(IBAction)btngalary1click:(id)sender
{
    btngalary.hidden=NO;
    btngalary1.hidden=YES;
    
    [UIView animateWithDuration:0.10f delay:0.10f options:UIViewAnimationOptionTransitionFlipFromRight animations:^
     {
         viewObj.frame = CGRectMake(320,47, viewObj.bounds.size.width, viewObj.bounds.size.height);
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Animation is complete");
     }];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrData count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifre =@"SampleCell";
    
    SampleCell *cell =(SampleCell *) [self.tblobj dequeueReusableCellWithIdentifier:cellIdentifre];
    
    if (cell==nil) {
        NSArray *arrNib=[[NSBundle mainBundle] loadNibNamed:cellIdentifre owner:self options:nil];
        cell= (SampleCell *)[arrNib objectAtIndex:0];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    //cell.imgicon.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
    
    cell.labelForPlace.text=[arrData objectAtIndex:indexPath.row];
    
    
    return cell;
    
}

-(IBAction)start:(id)sender
{
    
//    start.hidden=YES;
//    start1.hidden=NO;
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Anywear would like to use your current location" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: @"Cancel", nil];
    [alert show];
    
    
}
-(IBAction)start1:(id)sender
{
    start.hidden=NO;
    start1.hidden=YES;
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"So you're ready to go shopping? We need to get to know you first" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Ok", nil];
    // tag=0;
    
    [alert show];
    
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
