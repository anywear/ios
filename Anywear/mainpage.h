//
//  mainpage.h
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "info.h"
#import "login.h"
#import "signup.h"
#import "entrance.h"
#import "mainexplorer.h"
#import "signupexplore.h"
#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "style.h"

@class signup;
@class signupexplore;

@interface mainpage : UIViewController<FBLoginViewDelegate,UINavigationControllerDelegate>

{

    info *infoobj;
    login *loginobj;
    signup *signupobj;
    entrance *entobj;
    mainexplorer *mainexobj;
   
    UILabel *lblexplore;
    UIButton *btnexplore;
    
    signupexplore *signupexpobj;
    
    UIImageView *imgPhoto;
    
   
    style *stobj;
   
    
}
@property (nonatomic,strong)style *stobj;

@property (nonatomic,strong) IBOutlet UIImageView *imgPhoto;
@property (strong, nonatomic) IBOutlet FBLoginView *loginButton;

@property(nonatomic,strong)info *infoobj;
@property(nonatomic,strong)login *loginobj;
@property(nonatomic,strong)signup *signupobj;
@property(nonatomic,strong)entrance *entobj;
@property(nonatomic,strong) mainexplorer *mainexobj;
@property(nonatomic,strong) IBOutlet UILabel *lblexplore;
@property(nonatomic,strong) IBOutlet UIButton *btnexplore;

@property(nonatomic,strong)   signupexplore *signupexpobj;





@property (strong, nonatomic) id<FBGraphUser> loggedInUser;


-(IBAction)btninfo:(id)sender;
-(IBAction)btnlogin:(id)sender;
-(IBAction)btnsignup:(id)sender;
-(IBAction)btnentrance:(id)sender;
-(IBAction)btnfacebook:(id)sender;
-(IBAction)btnexplore:(id)sender;


@end
