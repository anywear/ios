//
//  thankyou.m
//  Anywear
//
//  Created by Pradip on 8/30/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "thankyou.h"

@interface thankyou ()

@end

@implementation thankyou

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(IBAction)back:(id)sender
{
    NSArray *arr=self.navigationController.viewControllers;
    [self.navigationController popToViewController:[arr objectAtIndex:15] animated:YES];


}
-(IBAction)anywear:(id)sender

{

    NSArray *arr=self.navigationController.viewControllers;
    [self.navigationController popToViewController:[arr objectAtIndex:1] animated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
