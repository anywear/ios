//
//  search.m
//  Anywear
//
//  Created by Pradip on 8/30/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "search.h"
#import "searchcell.h"

@interface search ()

@end

@implementation search
@synthesize tblobj,candyArray,candySearchBar,filteredCandyArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
    
   candyArray = [@[@"alpha beta",@"athletic/sweat",@"artwork",@"analog",@"adidas y-3"] mutableCopy];
    
    filteredCandyArray = [NSMutableArray arrayWithCapacity:[candyArray count]];
    }
    else{
    
        candyArray = [@[@"alpha beta",@"athletic/sweat",@"artwork",@"analog",@"adidas y-3"] mutableCopy];
        
        filteredCandyArray = [NSMutableArray arrayWithCapacity:[candyArray count]];
    
    
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (flgTemp==1)
	{
        return [filteredCandyArray count];
    }
	else
	{
        return [candyArray count];
    }
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    searchcell *cell = (searchcell*) [tableView dequeueReusableCellWithIdentifier:@"searchcell"];
    if(cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"searchcell" owner:[searchcell class] options:nil];
        cell = (searchcell *)[nib objectAtIndex:0];
        
    }
    
    if (flgTemp==0)
    {
       NSString *Candy  =[candyArray objectAtIndex:[indexPath row]];
        cell.place.text=Candy;
        
               
    }
    else
    {
        
       NSString  *candy =[filteredCandyArray objectAtIndex:[indexPath row]];
        
        cell.place.text=candy;
        
    }
    return cell;

}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [filteredCandyArray removeAllObjects];
    
	// Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
    NSArray *tempArray = [candyArray filteredArrayUsingPredicate:predicate];
    filteredCandyArray = [NSMutableArray arrayWithArray:tempArray];
    //[filteredCandyArray retain];
    [tblobj reloadData];
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = YES;
	return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = NO;
	return YES;
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText isEqualToString:@""]||searchText==nil ) {
        flgTemp=0;
        [tblobj reloadData];
    }
    else {
        flgTemp=1;
        [self filterContentForSearchText:searchText scope:@"Al"];
    }
    
    
    //  NSLog(@"se=%@",searchText);
}
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [searchBar resignFirstResponder];
}
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"])
    {
        [searchBar resignFirstResponder];
        return NO;
    }
    return YES;
}
-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];


}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
