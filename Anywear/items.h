//
//  items.h
//  Anywear
//
//  Created by Pradip on 8/27/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "addtocart.h"

@interface items : UIViewController
{
  UIScrollView *scrollobj;
    NSMutableArray *arrData;
    UITableView *tblobj;
    addtocart *addobj;
    
    

}
@property(nonatomic,strong) IBOutlet UIScrollView *scrollobj;
@property(strong,nonatomic) IBOutlet UITableView *tblobj;
@property (nonatomic,strong)NSMutableArray *arrData;
@property (nonatomic,strong) addtocart *addobj;



-(IBAction)back:(id)sender;
-(IBAction)addtocart:(id)sender;



@end
