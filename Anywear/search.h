//
//  search.h
//  Anywear
//
//  Created by Pradip on 8/30/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface search : UIViewController<UISearchBarDelegate,UISearchDisplayDelegate>

{
    
    UITableView *tblobj;
    int flgTemp;
    int flg;
    IBOutlet UISearchBar* search;


}
@property (retain,nonatomic)NSMutableArray *candyArray;
@property(retain,nonatomic)NSMutableArray *filteredCandyArray;
@property (retain,nonatomic)IBOutlet UISearchBar *candySearchBar;
@property(retain,nonatomic)IBOutlet UITableView *tblobj;


-(IBAction)back:(id)sender;

@end
