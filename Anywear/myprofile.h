//
//  myprofile.h
//  Anywear
//
//  Created by Pradip on 9/2/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myprofile : UIViewController
{
    NSMutableArray *image;
    UITableView *tblobj;
}

@property(nonatomic,strong)NSMutableArray *image;
@property(nonatomic,strong)  IBOutlet UITableView *tblobj;

-(IBAction)back:(id)sender;

@end
