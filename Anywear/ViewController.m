//
//  ViewController.m
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"
#import "style.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"


@interface ViewController ()

@end

@implementation ViewController
@synthesize sliderobj,splash,responseData,myresult,suc,tag,stobj1;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    if (g_IS_IPHONE_4_SCREEN)
    {
        
    stobj1=[[style alloc]initWithNibName:@"style" bundle:nil];
        sliderobj=[[Slider alloc]initWithNibName:@"Slider" bundle:nil];

        
    }
    else{
        
        stobj1=[[style alloc]initWithNibName:@"styleiphone" bundle:nil];

        sliderobj=[[Slider alloc]initWithNibName:@"slideriphone" bundle:nil];

    }
    
}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //[self PushView];
    [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(PushView)
                                   userInfo:nil
                                    repeats:NO];

     self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

-(void)PushView
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"uname"]==nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"pass"]==nil)
    {
        [self.navigationController pushViewController:sliderobj animated:YES];
    }
    else if ([[NSUserDefaults standardUserDefaults] objectForKey:@"uname"] || [[NSUserDefaults standardUserDefaults] objectForKey:@"pass"])
        
    {
        NSString *usename=  [[NSUserDefaults standardUserDefaults] objectForKey:@"uname"];
        NSString *pass=  [[NSUserDefaults standardUserDefaults] objectForKey:@"pass"];
        
        NSString *post = [NSString stringWithFormat:@"vEmail=%@&vPassword=%@",usename,pass];
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getUserLogin"]]];

        [request setHTTPMethod:@"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:postData];
        NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        if(conn)
        {
            NSLog(@"Connection Successful");
        }
        else
        {
            NSLog(@"Connection could not be made");
        }
        
    }
    NSUserDefaults *fetchDefaults = [NSUserDefaults standardUserDefaults];
    NSString *un = [fetchDefaults objectForKey:@"uname"];
    NSString *ps = [fetchDefaults objectForKey:@"pass"];
    if (un==nil || ps==nil)
    {
    }
    else
    {
        
    }
  }

- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Did Receive Response %@", response);
    responseData = [[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data
{
    
    NSLog(@"data=%@",data);
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"User Date=%@",str);
    
    NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"Result = %@",result);
    
    
    suc = [result objectForKey:@"message"];
    NSLog(@"Mess::%@",suc);
    
    
    NSDictionary *data1=[result objectForKey:@"data"];
    NSLog(@"data1=%@",data1);
    
    
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    
    arr=[NSMutableArray arrayWithObject:data1];
    
    for (int i; i<[arr count]; i ++)
    {
        NSLog(@"mss=%@",arr);
    }
    
    
    if ([suc isEqualToString:@"Client Login Successfully"])
    {
     [self.navigationController pushViewController:stobj1 animated:YES];
       
        
    }
    else if ([suc isEqualToString:@"Check Your E-Mail Address and Password."])
    {
       
    }

}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    NSLog(@"Connection failed! Error - %@ %@",[error localizedDescription],[[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Intenet Connection Error" message:@"Please Check Your Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    return;
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"hello1");
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
