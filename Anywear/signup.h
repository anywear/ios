//
//  signup.h
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "style.h"
#import "login.h"

@class style;
@class login;

@interface signup : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

{
    style *styleobj;
    UIImageView *imgPhoto;
    UIImagePickerController *imagPikar;
    UITextField *uname;
    UITextField *passwrd;
    UIScrollView *scroll;
    NSOperationQueue *_queue;
    UILabel *etype;
    NSString *fbid1;
    int tag;
    int imgtag;
    login *logobj;

}
@property(strong,nonatomic) NSString *fbid1;
@property (nonatomic) int tag;
@property (nonatomic) int imgtag;
@property(strong,nonatomic) IBOutlet UIScrollView *scroll;
@property (retain) NSOperationQueue *queue;
@property(strong,nonatomic) IBOutlet    UILabel *etype;
@property(strong,nonatomic) IBOutlet UITextField *uname;
@property(strong,nonatomic) IBOutlet UITextField *passwrd;
@property (nonatomic,strong) UIImagePickerController *imagPikar;
@property (nonatomic,strong) IBOutlet UIImageView *imgPhoto;

@property(nonatomic,strong)  style *styleobj;
@property(nonatomic,strong)  login *logobj;


-(IBAction)btnback:(id)sender;
-(IBAction)signup:(id)sender;

-(IBAction)btnProPhoto:(id)sender;


@end
