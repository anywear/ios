//
//  Slider.m
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "Slider.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"



@interface Slider ()

@end

@implementation Slider
@synthesize skip,obj1,imgobj,responseData,tag,suc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (g_IS_IPHONE_4_SCREEN)
   {
    
    obj1=[[explore alloc]initWithNibName:@"explore" bundle:nil];
    //chobj=[[choosstyle alloc]initWithNibName:@"choosstyle" bundle:nil];

       
 
    ALScrollViewPaging *scrollView = [[ALScrollViewPaging alloc] initWithFrame:CGRectMake(0,20,320,460)];
       
    NSMutableArray *views = [[NSMutableArray alloc] init];
    
    NSArray *image1 = [NSArray arrayWithObjects:[UIImage imageNamed:@"slider1-1.png"], [UIImage imageNamed:@"slider2-1.png"], [UIImage imageNamed:@"slider3-1.png"],[UIImage imageNamed:@"slider4-1.png"], nil];
       
       NSString *img=[image1 lastObject];
       NSLog(@"last=%@",img);
    
    for (int i = 0; i < 4; i++)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
        [views addObject:view];
       
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button addTarget:self
                   action:@selector(skip:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"" forState:UIControlStateNormal];
        button.frame = CGRectMake(185,430,60,30);
        [view addSubview:button];
    }
    for (int i = 0; i < image1.count; i++) {
        CGRect frame;
        frame.origin.x = scrollView.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = scrollView.frame.size;
       
        UIScrollView *subScrollView = [[UIScrollView alloc] initWithFrame:frame];
       
        UIImageView *subview = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
       
        UIImage *image = [image1 objectAtIndex:i];
       subScrollView.contentSize = image.size;
       
        [subview setImage:image];
        [subScrollView addSubview:subview];
        [scrollView addSubview:subScrollView];
        [subview release];
        [subScrollView release];
    }
    [scrollView addPages:views];
    [self.view addSubview:scrollView];
    [scrollView setHasPageControl:YES];
       
}
    else
    {
    
        obj1=[[explore alloc]initWithNibName:@"exploreiphone" bundle:nil];
        //chobj=[[choosstyle alloc]initWithNibName:@"choosstyleiphone" bundle:nil];

        
        ALScrollViewPaging *scrollView = [[ALScrollViewPaging alloc] initWithFrame:CGRectMake(0,20,320,548)];
        
        NSMutableArray *views = [[NSMutableArray alloc] init];
        
        NSArray *image1 = [NSArray arrayWithObjects:[UIImage imageNamed:@"slider1-1.png"], [UIImage imageNamed:@"slider2-1.png"], [UIImage imageNamed:@"slider3-1.png"],[UIImage imageNamed:@"slider4-1.png"], nil];
        
        for (int i = 0; i < 4; i++)
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
            [views addObject:view];
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [button addTarget:self
                       action:@selector(skip:)
             forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:@"" forState:UIControlStateNormal];
            button.frame = CGRectMake(180,510,70,35);
            [view addSubview:button];
        }
        
        for (int i = 0; i < image1.count; i++) {
            CGRect frame;
            frame.origin.x = scrollView.frame.size.width * i;
            frame.origin.y = 0;
            frame.size = scrollView.frame.size;
            
            UIScrollView *subScrollView = [[UIScrollView alloc] initWithFrame:frame];
            
            UIImageView *subview = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
            
            UIImage *image = [image1 objectAtIndex:i];
            subScrollView.contentSize = image.size;
            
            [subview setImage:image];
            [subScrollView addSubview:subview];
            [scrollView addSubview:subScrollView];
            [subview release];
            [subScrollView release];
        }
       
        [scrollView addPages:views];
        [self.view addSubview:scrollView];
        [scrollView setHasPageControl:YES];
     

}
    
}
-(IBAction)skip:(id)sender;
{
    [self.navigationController pushViewController:obj1 animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
