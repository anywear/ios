//
//  itemcell.h
//  Anywear
//
//  Created by Pradip on 8/27/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface itemcell : UITableViewCell
{}

@property (nonatomic,strong) IBOutlet UILabel *labelForPlace;
@property (nonatomic,strong) IBOutlet UIImageView *imgicon;
@end
