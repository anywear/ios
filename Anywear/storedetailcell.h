//
//  storedetailcell.h
//  Boutiqall
//
//  Created by Pradip on 9/24/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface storedetailcell : UITableViewCell
{
    UIImageView *imgphoto;
    UILabel *address;
    UILabel *price;
    UIButton *choose;
    
}


@property(nonatomic,strong) IBOutlet UIButton *choose;
@property(nonatomic,strong) IBOutlet UIImageView *imgphoto;
@property(nonatomic,strong) IBOutlet UILabel *address;
@property(nonatomic,strong) IBOutlet UILabel *price;


@end

