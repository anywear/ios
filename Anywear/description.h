//
//  description.h
//  Anywear
//
//  Created by Pradip on 8/27/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface description : UIViewController
{
    UIScrollView *scrollobj;
    NSMutableArray *arrData;
    UITableView *tblobj;
    
     NSArray *thumbnails;


}
@property(nonatomic,strong) IBOutlet UIScrollView *scrollobj;
@property(strong,nonatomic) IBOutlet UITableView *tblobj;
@property (nonatomic,strong)NSMutableArray *arrData;

-(IBAction)back:(id)sender;
@end
