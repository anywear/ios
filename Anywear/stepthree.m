//
//  stepthree.m
//  Anywear
//
//  Created by Pradip on 8/14/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "stepthree.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"
@interface stepthree ()

@end

@implementation stepthree
@synthesize btnpink1,btnpink2,btnpink3,btnpink4,btnwhite1,btnwhite2,btnwhite3,btnwhite4,cnfirmobj,storenext,lblstore,lbl1,lbl2,lbl3,lbl4,fid,spendmoney,b1,b2,b3,b4;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
    if (g_IS_IPHONE_4_SCREEN)
    {
        fid.hidden=YES;
        btnpink1.hidden=YES;
        btnpink2.hidden=YES;
        btnpink3.hidden=YES;
        btnpink4.hidden=YES;
        
        cnfirmobj=[[confirm alloc]initWithNibName:@"confirm" bundle:nil];
        
        
        storenext=[[NSMutableArray alloc]init];
        lblstore=[[NSMutableArray alloc]init];
        spendmoney=[[NSMutableArray alloc]init];
        
        
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"6" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"6" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-6" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-6" object:nil];
        [self RegisterCall];
    }
    else{
        
        fid.hidden=YES;
        btnpink1.hidden=YES;
        btnpink2.hidden=YES;
        btnpink3.hidden=YES;
        btnpink4.hidden=YES;
        
        cnfirmobj=[[confirm alloc]initWithNibName:@"confirmiphone" bundle:nil];
        
        
        storenext=[[NSMutableArray alloc]init];
        lblstore=[[NSMutableArray alloc]init];
        spendmoney=[[NSMutableArray alloc]init];

        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"6" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"6" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-6" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-6" object:nil];
        [self RegisterCall];
        
    }

    
}
-(void)RegisterCall {
    
    
    NSString *myRequestString = [NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getMoneySpendOn"];
    [[HttpQueue sharedSingleton] getItems:myRequestString:6];
}

- (void) registerCallComplete : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSArray *dictResult =(NSArray *)result;
    NSLog(@"re=%@",dictResult);
    
    for (int k=0; k<[dictResult count]; k++)
    {
        NSDictionary *dict_steps=[dictResult objectAtIndex:k];
        
        NSString *value=[dict_steps objectForKey:@"eSign"];
        NSLog(@"vTitle id== %@",value);
        NSString *value1=[dict_steps objectForKey:@"vTitle"];
        NSLog(@"eSign id== %@",value1);
        
        NSString *value2=[dict_steps objectForKey:@"iSpendId"];
        NSLog(@"eSign id== %@",value2);
        
        
        [lblstore addObject:value1];
        [storenext addObject:value];
        [spendmoney addObject:value2];

        
       
    }
    
    NSUInteger idx = 0;
    for ( NSString *string in storenext )
    {
        switch ( idx++ ) {
            case 0:
                [btnwhite1 setTitle:string forState:UIControlStateNormal];
                [btnpink1 setTitle:string forState:UIControlStateNormal];
                break;
            case 1:
                [btnwhite2 setTitle:string forState:UIControlStateNormal];
                [btnpink2 setTitle:string forState:UIControlStateNormal];
                break;
            case 2:
                [btnwhite3 setTitle:string forState:UIControlStateNormal];
                [btnpink3 setTitle:string forState:UIControlStateNormal];
                break;
            case 3:
                [btnwhite4 setTitle:string forState:UIControlStateNormal];
                [btnpink4 setTitle:string forState:UIControlStateNormal];
                break;
        }
    }
    NSUInteger idx1 = 0;
    
    for ( NSString *string1 in lblstore )
    {
        switch ( idx1++ ) {
            case 0:
                lbl1.text=string1;
                break;
            case 1:
                lbl2.text=string1;
                break;
            case 2:
                lbl3.text=string1;
                break;
            case 3:
                lbl4.text=string1;
                break;
        }
    }
    
    NSUInteger idx3 = 0;
    
    for ( NSString *string1 in spendmoney )
    {
        switch ( idx3++ )
        {
            case 0:
                fid.text=string1;
                NSLog(@"Size Id=%@",string1);
                b1=string1;
                break;
                
            case 1:
                fid.text=string1;
                NSLog(@"Size Id=%@",string1);
                b2=string1;
                break;
                
            case 2:
                fid.text=string1;
                NSLog(@"Size Id=%@",string1);
                b3=string1;
                break;
                
            case 3:
                fid.text=string1;
                NSLog(@"Size Id=%@",string1);
                b4=string1;
                break;
        }
    }
}

- (void) registerCallFail : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Fail=%@",dict);
    return;
}

-(void)viewWillAppear
{
    
}
-(IBAction)btnwhite1:(id)sender
{
    
    btnpink1.hidden=NO;
    btnpink2.hidden=YES;
    btnpink3.hidden=YES;
    btnpink4.hidden=YES;
    fid.text=b1;
    
}
-(IBAction)btnwhite2:(id)sender
{
    
    btnpink2.hidden=NO;
    btnpink1.hidden=YES;
    btnpink3.hidden=YES;
    btnpink4.hidden=YES;
    fid.text=b2;
    
}
-(IBAction)btnwhite3:(id)sender
{
    
    btnpink3.hidden=NO;
    btnpink1.hidden=YES;
    btnpink2.hidden=YES;
    btnpink4.hidden=YES;
    fid.text=b3;
    
    }

-(IBAction)btnwhite4:(id)sender
{
    
    btnpink1.hidden=YES;
    btnpink2.hidden=YES;
    btnpink3.hidden=YES;
    btnpink4.hidden=NO;
    fid.text=b4;
    
    
}
-(IBAction)btnpink1:(id)sender
{
   btnpink1.hidden=YES;
}

-(IBAction)btnpink2:(id)sender
{
   btnpink2.hidden=YES;
}
-(IBAction)btnpink3:(id)sender
{
   btnpink3.hidden=YES;
}
-(IBAction)btnpink4:(id)sender
{
   btnpink4.hidden=YES;
}

-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(IBAction)next:(id)sender

{
     [[Singleton sharedSingleton]setstrspmoney:fid.text];
    
    
    if ((btnpink1.hidden==YES && btnpink2.hidden==YES && btnpink3.hidden==YES && btnpink4.hidden==YES))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SELECT BUDGET" message:@"Please Select your Budget" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        [self.navigationController pushViewController:cnfirmobj animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
