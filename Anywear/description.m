//
//  description.m
//  Anywear
//
//  Created by Pradip on 8/27/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "description.h"
#import "descell.h"
@interface description ()

@end

@implementation description
@synthesize scrollobj,tblobj,arrData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //[scrollobj setScrollEnabled:YES];
    //[scrollobj setContentSize:CGSizeMake(320,850)];
    if (g_IS_IPHONE_4_SCREEN)
    {

    
    arrData =[[NSMutableArray alloc] init];
    [arrData addObject:@"15 shabazi st. Tel Aviv"];
    [arrData addObject:@"972(0)3 516 6975"];
    [arrData addObject:@"Sun-Thu 10:00-20:00,Fri 10:00"];
    
    
    thumbnails = [NSArray arrayWithObjects:@"10_Icon_30x30.png", @"8_Icon_30x30.png", @"9_Icon_30x30.png",  nil];
    }
    else{
        
        arrData =[[NSMutableArray alloc] init];
        [arrData addObject:@"15 shabazi st. Tel Aviv"];
        [arrData addObject:@"972(0)3 516 6975"];
        [arrData addObject:@"Sun-Thu 10:00-20:00,Fri 10:00"];
        
        
        thumbnails = [NSArray arrayWithObjects:@"10_Icon_30x30.png", @"8_Icon_30x30.png", @"9_Icon_30x30.png",  nil];
    
    
    
    }

    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrData count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifre =@"descell";
    
    descell *cell =(descell *) [self.tblobj dequeueReusableCellWithIdentifier:cellIdentifre];
    
    if (cell==nil) {
        NSArray *arrNib=[[NSBundle mainBundle] loadNibNamed:cellIdentifre owner:self options:nil];
        cell= (descell *)[arrNib objectAtIndex:0];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    cell.imgicon1.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
    
    cell.labelForPlace.text=[arrData objectAtIndex:indexPath.row];
    
    
    return cell;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
