//
//  Singleton.h
//  PocketRealty
//
//  Created by TISMobile on 5/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

@interface Singleton : NSObject {
    
    NSString *fbcheck;
    NSString *val;
    NSString *check;
    NSString *uname;
    NSString *paswrd;
    
    UIImageView *imgPhoto;
    
    UIImageView *img;
    
    NSString *fbusername;
    NSString *fcbid;
    NSString *t;
   
    NSString *category;
    
//style
    
    NSMutableArray *dayarr;
    NSMutableArray *nightarr;
    
    
    
 //size
    
    NSMutableArray *size;
    NSMutableArray *bsize;
    
//money
    
    NSString *strspmoney;
    
//location
    
      NSMutableArray *arrloc;
      NSString *allstring;
    

//category
    
    NSMutableArray *arrcat;

    
}
@property(nonatomic,strong)  NSString *fbcheck;
@property(nonatomic,strong) NSString *category;

@property(nonatomic,strong) NSString *fbusername;
@property(nonatomic,strong) NSString *fcbid;
@property(nonatomic,strong) NSString *t;

@property(nonatomic,strong)	UIImageView *imgPhoto;
@property(nonatomic,strong)	UIImageView *img;

@property(nonatomic,strong) NSString *val;
@property(nonatomic,strong) NSString *check;
@property(nonatomic,strong) NSString *uname;
@property(nonatomic,strong)  NSString *paswrd;

//style

@property(nonatomic,strong)   NSMutableArray *dayarr;
@property(nonatomic,strong)   NSMutableArray *nightarr;



//size

@property(nonatomic,strong)   NSMutableArray *size;
@property(nonatomic,strong)   NSMutableArray *bsize;

//money

@property(nonatomic,strong)  NSMutableArray *spmoney;
@property(nonatomic,strong)  NSString *strspmoney;

//location

@property(nonatomic,strong)  NSString *strlocation;
@property(nonatomic,strong)   NSMutableArray *arrloc;
@property(nonatomic,strong)    NSString *allstring;


//category
@property(nonatomic,strong)   NSMutableArray *arrcat;






+ (Singleton *) sharedSingleton;
-(void)setMyvalue : (NSString *)values;
-(NSString *)getMyvalue;


-(void)setMycheck : (NSString *)chk;
-(NSString *)getMycheck;


-(void)setMyfbcheck : (NSString *)fchk;
-(NSString *)getMyfbcheck;



- (NSString *) getBaseURL;


-(void)setMyuname : (NSString *)unm;
-(NSString *)getMyuname ;


-(void)setMypasswrd : (NSString *)psw;
-(NSString *)getMypasswrd ;

-(void)setMyPhotoImage : (UIImageView *)iimg;
-(UIImageView *)getMyPhotoImage ;

-(void)setMyImage :(UIImageView *)img1;
-(UIImageView *)getMyImage;

-(void)setFBname : (NSString *)fbnm;
-(NSString *)getFBname ;

-(void)setFBid : (NSString *)fbud;
-(NSString *)getFBid ;

-(void)settag : (NSString *)tg;
-(NSString *)gettag ;

-(void)setcategory:(NSString *)cat;
-(NSString *)getcategory;

//style

-(void)setdayarr:(NSMutableArray *)da;
-(NSMutableArray *)getdayarr;

-(void)setnightarr:(NSMutableArray *)na;
-(NSMutableArray *)getnightarr;


//size
-(void)setsize:(NSMutableArray *)sz;
-(NSMutableArray *)getsize;

-(void)setbsize:(NSMutableArray *)bsz;
-(NSMutableArray *)getbsize;

//money

-(void)setstrspmoney:(NSString *)spm;
-(NSString *)getstrspmoney ;

//location

-(void)setarrloc:(NSMutableArray *)lc;
-(NSMutableArray *)getarrloc;


-(void)setallstring : (NSString *)all;
-(NSString *)getallstring ;


//category

-(void)setarrcat:(NSMutableArray *)ac;
-(NSMutableArray *)getarrcat;

@end
