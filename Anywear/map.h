//
//  map.h
//  Anywear
//
//  Created by Pradip on 8/26/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "myprofile.h"
#import "explore.h"
@class explore;

@interface map : UIViewController<CLLocationManagerDelegate,
UIGestureRecognizerDelegate>
{
    UIView *viewObj;
    
    UIButton *btngalary;
    UIButton *btngalary1;
    
    NSMutableArray *arrData;
    UITableView *tblobj;
    myprofile *proobj;
    
    
 //map
    
    GMSMapView *mapView;
    GMSMarker *currentPlaceMarker;
    CLLocationManager *locationManager;
    NSString *latitude;
    NSString *longitude;
    
    CLLocationDegrees lat_dest1;
    CLLocationDegrees lng_dest1;
    
    CLLocationDegrees Curr_lat;
    CLLocationDegrees Curr_lng;

    CLLocation *currentLocation;

    explore *newex;
   

  

}


+(CLLocationCoordinate2D) getLocationFromAddressString:(NSString*) addressStr;


 @property (nonatomic,strong) explore *newex;
@property (nonatomic,strong) myprofile *proobj;


@property (nonatomic,strong)IBOutlet UIView *viewObj;
@property (nonatomic,strong)IBOutlet UIButton *btngalary;
@property (nonatomic,strong)IBOutlet UIButton *btngalary1;
@property(strong,nonatomic) IBOutlet UITableView *tblobj;
@property (nonatomic,strong)NSMutableArray *arrData;
@property (nonatomic,strong)IBOutlet UIView *mapviewobj;
@property(nonatomic,strong) GMSPolyline *polyline;


@property(nonatomic,strong) CLLocationManager *locationManager;

@property(nonatomic,strong) GMSMarker *currentPlaceMarker;




-(IBAction)btngalaryclick:(id)sender;
-(IBAction)btngalary1click:(id)sender;
-(IBAction)back:(id)sender;



@end
