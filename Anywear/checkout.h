//
//  checkout.h
//  Anywear
//
//  Created by Pradip on 8/30/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "thankyou.h"

@interface checkout : UIViewController
{
    thankyou *thnsobj;
    
  
}
@property(nonatomic,strong) thankyou *thnsobj;

-(IBAction)pay:(id)sender;
-(IBAction)back:(id)sender;
-(IBAction)paypal:(id)sender;
-(IBAction)cell:(id)sender;
-(IBAction)visa:(id)sender;
-(IBAction)secure:(id)sender;
-(IBAction)master:(id)sender;
-(IBAction)google:(id)sender;

@end
