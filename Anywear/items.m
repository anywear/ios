//
//  items.m
//  Anywear
//
//  Created by Pradip on 8/27/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "items.h"
#import "itemcell.h"

@interface items ()

@end

@implementation items
@synthesize scrollobj,tblobj,arrData,addobj;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (g_IS_IPHONE_4_SCREEN)
    {
    arrData =[[NSMutableArray alloc] init];
    [arrData addObject:@"Size & Fit"];
    [arrData addObject:@"Composition & Care"];
    [arrData addObject:@"Boutique"];
    [arrData addObject:@"Designer"];
    [arrData addObject:@"Shipping & Free Returns"];
    
    [scrollobj setScrollEnabled:YES];
    [scrollobj setContentSize:CGSizeMake(320,850)];
        
    addobj=[[addtocart alloc]initWithNibName:@"addtocart" bundle:nil];
        
    }
    else{
    
        arrData =[[NSMutableArray alloc] init];
        [arrData addObject:@"Size & Fit"];
        [arrData addObject:@"Composition & Care"];
        [arrData addObject:@"Boutique"];
        [arrData addObject:@"Designer"];
        [arrData addObject:@"Shipping & Free Returns"];
        
        [scrollobj setScrollEnabled:YES];
        [scrollobj setContentSize:CGSizeMake(320,950)];
        
         addobj=[[addtocart alloc]initWithNibName:@"addtocartiphone" bundle:nil];
    
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrData count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifre =@"itemcell";
    
    itemcell *cell =(itemcell *) [self.tblobj dequeueReusableCellWithIdentifier:cellIdentifre];
    
    if (cell==nil) {
        NSArray *arrNib=[[NSBundle mainBundle] loadNibNamed:cellIdentifre owner:self options:nil];
        cell= (itemcell *)[arrNib objectAtIndex:0];
        cell.backgroundColor =[UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    //cell.imgicon.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
    
    cell.labelForPlace.text=[arrData objectAtIndex:indexPath.row];
    
    
    return cell;
    
}

-(IBAction)addtocart:(id)sender
{

[self.navigationController pushViewController:addobj animated:YES];
}

-(IBAction)back:(id)sender
{
    
  [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
