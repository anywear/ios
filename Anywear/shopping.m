//
//  shopping.m
//  Anywear
//
//  Created by Pradip on 8/25/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "shopping.h"
#import "shopcell.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"
#import "DejalActivityView.h"


@interface shopping ()

@end

@implementation shopping
@synthesize tblobj,arrdata,shopingoneobj,isSelectAllBtnClicked,tempFlag,btn,aImageView,loc,locationarray,locationarray1,address,arrtag1,btn1,imgView,allobj,strall,tag;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isSelectAllBtnClicked=NO;

    if (g_IS_IPHONE_4_SCREEN)
    {
        arrdata =[[NSMutableArray alloc] init];
         locationarray =[[NSMutableArray alloc] init];
        locationarray1 =[[NSMutableArray alloc] init];
        
        loc.hidden=YES;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"9" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"9" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-9" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-9" object:nil];
        
        
        [self RegisterCall];

    
    shopingoneobj=[[shoppingone alloc]initWithNibName:@"shoppingone" bundle:nil];
    }
    else
    {
        arrdata =[[NSMutableArray alloc] init];
        locationarray =[[NSMutableArray alloc] init];
        locationarray1 =[[NSMutableArray alloc] init];
        loc.hidden=YES;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"9" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"9" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-9" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-9" object:nil];
        
        
        [self RegisterCall];
        
        shopingoneobj=[[shoppingone alloc]initWithNibName:@"shoppingoneiphone" bundle:nil];
    
    }
 }

-(void)RegisterCall {
    
    
    NSString *myRequestString = [NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getStoreLocation"];
    
    [[HttpQueue sharedSingleton] getItems:myRequestString:9];
}

- (void) registerCallComplete : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSMutableArray *dictResult =(NSMutableArray *)result;
    NSLog(@"re=%@",dictResult);
    
    for (int i=0; i<[dictResult count]; i++)
    {
        NSDictionary *dict_steps=[dictResult objectAtIndex:i];
        
        address=[dict_steps objectForKey:@"vTitle"];
        NSLog(@"address== %@",address);
        [arrdata addObject:address];
        
        NSString *locid=[dict_steps objectForKey:@"iStoreLocationId"];
        NSLog(@"address== %@",locid);
        [locationarray addObject:locid];
        
        
        allobj=[arrdata lastObject];
        
    }
    
    [tblobj reloadData];
    

}

- (void) registerCallFail : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Fail=%@",dict);
    return;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return arrdata.count;
    return locationarray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell=nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
//        cell.textLabel.text=[arrdata objectAtIndex:indexPath.row];
//        cell.textLabel.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:18];
        //cell.textLabel.text=[locationarray objectAtIndex:indexPath.row];
        
        
        UILabel *lbl1 =  [[UILabel alloc] initWithFrame: CGRectMake(10,0,320,50)];
        [lbl1 setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:19]];
        [lbl1 setTextColor:[UIColor blackColor]];
        lbl1.text =[arrdata objectAtIndex:indexPath.row];
        [cell addSubview:lbl1];
        [lbl1 release];
        
        UILabel *lbl2 =  [[UILabel alloc] initWithFrame: CGRectMake(0,0,320,50)];
        [lbl2 setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:199]];
        lbl2.hidden=YES;
        [lbl2 setTextColor:[UIColor blackColor]];
        lbl2.text =[locationarray objectAtIndex:indexPath.row];
        [cell addSubview:lbl2];
        [lbl2 release];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag=indexPath.row;
        button.frame = CGRectMake(0, 0, 320, 50);
        [button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [button addTarget:self action:@selector(buttonaction:) forControlEvents:UIControlEventTouchDown];
        [cell.contentView addSubview:button];
       

        
    }
      
      return cell;
    


    
    
    
    
    
//    static NSString *CellIdentifier = @"shopcell";
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];
//        UIButton *checkBox = [[UIButton alloc] init];
//        checkBox.tag = 111;
//        [cell.contentView addSubview:checkBox];
//        [checkBox setFrame:CGRectMake(6,14,20,20)];
//        [checkBox release];
        
    
        
   // }
    //UIButton *checkBox = (UIButton *)[cell.contentView viewWithTag:checkBox.tag];
    
//    if(isActivDeactivButton)
//    {
//        [checkBox setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//    }
//    else{
//        [checkBox setImage:[UIImage imageNamed:@"pink_right.png"] forState:UIControlStateNormal];
//    }
    //return cell;
}


-(void)buttonaction:(id)sender
{
    
if([address isEqual:@"All"])
{
    arrtag1=0;
    strall=allobj;
        
}
    

    btn=(UIButton *)sender;
    btn.frame = CGRectMake(0, 0, 310, 45);
   
    NSLog(@"btntag:%i",btn.tag);
    
    if (btn.selected)
    {
        [btn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        NSString *tempstr=[NSString stringWithString:[arrdata objectAtIndex:btn.tag]];
        NSLog(@"btntag:%@",tempstr);
               NSLog(@"remove");
        NSString *tempstr1=[NSString stringWithString:[locationarray objectAtIndex:btn.tag]];
        NSLog(@"btntag:%@",tempstr1);
        NSLog(@"remove");
        btn.selected=FALSE;
    }
    else
    {
        
        [btn setImage:[UIImage imageNamed:@"pink_right.png"] forState:UIControlStateNormal];
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        btn.selected=TRUE;
        NSString *tempstr=[NSString stringWithString:[arrdata objectAtIndex:btn.tag]];
        [arrdata addObject:tempstr];
        NSString *tempstr1=[NSString stringWithString:[locationarray objectAtIndex:btn.tag]];
        [locationarray1 addObject:tempstr1];
        loc.text=tempstr1;
        arrtag1=1;
        
    }
       
}
-(IBAction)btnDetailClick:(id)sender
{
    
    
}

-(IBAction)next:(id)sender
{
    if (arrtag1==0)
    {
         [[Singleton sharedSingleton]setallstring:strall];
        
    }
    
    else
    {
    
    [[Singleton sharedSingleton]setarrloc:locationarray1];
     if(btn==nil)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"SELECT COUNTRY" message:@"Please Select Country" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    else
    {
      [self.navigationController pushViewController:shopingoneobj animated:YES];
    }
    }
    
}
-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];


}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
