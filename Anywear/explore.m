//
//  explore.m
//  Anywear
//
//  Created by Pradip on 9/4/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "explore.h"
#import "SampleCell.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"
#import "DejalActivityView.h"
#import "storecell.h"



@interface explore ()

@end

@implementation explore
@synthesize tblobj,arrData,start,explobj,btngalary,btngalary1,tag,mainobj,img,imgbac,imgmapback,viewObj,scrollobj,imagephoto,image1,arraystore,storetabble,thumbnails,imgpic,image2,mypro,styobj;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (g_IS_IPHONE_4_SCREEN)
    {
        
        UIView *viewToUse = self.view;
        viewToUse = self.navigationController.navigationBar.superview;
        [DejalBezelActivityView activityViewForView:viewToUse];

        
        image1=[[NSMutableArray alloc]init];
        image2=[[NSMutableArray alloc]init];
        imagephoto.hidden=YES;
        imgpic.hidden=YES;
        
      
        
        arraystore =[[NSMutableArray alloc] init];

        
        [scrollobj setScrollEnabled:YES];
        [scrollobj setContentSize:CGSizeMake(320,550)];
      
        btngalary1.hidden=YES;
        arrData =[[NSMutableArray alloc] init];
        [arrData addObject:@"Explore"];
        [arrData addObject:@"Change Personal Settings"];
        [arrData addObject:@"Boutiqall"];
        [arrData addObject:@"Notifications"];
        [arrData addObject:@"My Profile"];
        [arrData addObject:@"Sign Out"];
        

        styobj=[[style alloc]initWithNibName:@"style" bundle:nil];

        mypro=[[myprofile alloc]initWithNibName:@"myprofile" bundle:nil];
    
        mainobj=[[mainpage alloc]initWithNibName:@"mainpage" bundle:nil];
        explobj=[[exploreinfo alloc]initWithNibName:@"exploreinfo" bundle:nil];

        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"2" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"2" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-2" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-2" object:nil];

        
        [self RegisterCall];
        
       [[NSNotificationCenter defaultCenter] removeObserver:self name:@"5" object:nil];
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete1:) name:@"5" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-5" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail1:) name:@"-5" object:nil];
 }
    
    else{
        
        UIView *viewToUse = self.view;
        viewToUse = self.navigationController.navigationBar.superview;
        [DejalBezelActivityView activityViewForView:viewToUse];
        
        
        image1=[[NSMutableArray alloc]init];
        image2=[[NSMutableArray alloc]init];
        imagephoto.hidden=YES;
        imgpic.hidden=YES;
        
        
        
        arraystore =[[NSMutableArray alloc] init];
        
        
        [scrollobj setScrollEnabled:YES];
        [scrollobj setContentSize:CGSizeMake(320,550)];
        
        btngalary1.hidden=YES;
        arrData =[[NSMutableArray alloc] init];
        [arrData addObject:@"Explore"];
        [arrData addObject:@"Change Personal Settings"];
        [arrData addObject:@"Boutiqall"];
        [arrData addObject:@"Notifications"];
        [arrData addObject:@"My Profile"];
        [arrData addObject:@"Sign Out"];

        
        styobj=[[style alloc]initWithNibName:@"styleiphone" bundle:nil];
        mypro=[[myprofile alloc]initWithNibName:@"myprofileiphone" bundle:nil];

        mainobj=[[mainpage alloc]initWithNibName:@"mainpageiphone" bundle:nil];
        explobj=[[exploreinfo alloc]initWithNibName:@"infoexploreiphone" bundle:nil];
        
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"2" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"2" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-2" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-2" object:nil];
        
        
        [self RegisterCall];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"5" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete1:) name:@"5" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-5" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail1:) name:@"-5" object:nil];
        }
}

-(void)RegisterCall {
    
    
    NSString *myRequestString = [NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=getAppHomeSlider"];
    [[HttpQueue sharedSingleton] getItems:myRequestString:2];
}

- (void) registerCallComplete : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSMutableArray *dictResult =(NSMutableArray *)result;
    NSLog(@"re=%@",dictResult);
    
    for (int i=0; i<[dictResult count]; i++)
    {
        NSDictionary *dict_steps=[dictResult objectAtIndex:i];
        
        NSString *value=[dict_steps objectForKey:@"iSliderId"];
        NSLog(@"Slider id== %@",value);
        
        NSString *img1=[dict_steps objectForKey:@"vImage"];
        NSLog(@"img== %@",img1);
        
        UIImage* myImage = [UIImage imageWithData:
                            [NSData dataWithContentsOfURL:
                             [NSURL URLWithString:img1]]];
        
        
        imagephoto.image=myImage;
        [image1 addObject:imagephoto.image];
}
   [self call];
    
    
}
-(void)call
{
    
    
    ALScrollViewPaging *scrollView = [[ALScrollViewPaging alloc] initWithFrame:CGRectMake(0,54,320,195)];
    
  
    NSMutableArray *views = [[NSMutableArray alloc] init];
    
    NSString *imge=[image1 lastObject];
    NSLog(@"last=%@",imge);
    
    for (int i = 0; i < image1.count; i++)
    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
        [views addObject:view];
    }
    
    for (int i = 0; i < image1.count; i++) {
        CGRect frame;
        frame.origin.x = scrollView.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = scrollView.frame.size;
        
        UIScrollView *subScrollView = [[UIScrollView alloc] initWithFrame:frame];
        
        UIImageView *subview = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
        
        UIImage *image = [image1 objectAtIndex:i];
        subScrollView.contentSize = image.size;
        
        [subview setImage:image];
        [subScrollView addSubview:subview];
        [scrollView addSubview:subScrollView];
        [subview release];
        [subScrollView release];
    }
    [scrollView addPages:views];
    [self.view addSubview:scrollView];
    [scrollView setHasPageControl:YES];
    
    [self RegisterCall1];
    
   
}
- (void) registerCallFail : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Fail=%@",dict);
    return;
}



-(void)RegisterCall1 {
    
    
    NSString *myRequestString = [NSString stringWithFormat:@"http://techiestownhosting.com/php/anywear/webservice?action=getStore"];
    [[HttpQueue sharedSingleton] getItems:myRequestString:5];
}

- (void) registerCallComplete1 : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    
    NSMutableArray *dictResult =(NSMutableArray *)result;
    NSLog(@"re=%@",dictResult);
    
    for (int i=0; i<[dictResult count]; i++)
    {
        NSDictionary *dict_steps=[dictResult objectAtIndex:i];
        
        
        
        NSString *img1=[dict_steps objectForKey:@"vShopImage"];
        NSLog(@"img== %@",img1);
        
        UIImage* myImage = [UIImage imageWithData:
                            [NSData dataWithContentsOfURL:
                             [NSURL URLWithString:img1]]];
        
        
        imgpic.image=myImage;
        [image2 addObject:imgpic.image];
        
        
    }

   [storetabble reloadData];
   [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
}

- (void) registerCallFail1 : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Fail=%@",dict);
    return;
}

-(IBAction)btngalaryclick:(id)sender;
{
    btngalary.hidden=YES;
    btngalary1.hidden=NO;
      viewObj.hidden=NO;
    [UIView animateWithDuration:0.10f delay:0.10f options:UIViewAnimationOptionTransitionFlipFromRight animations:^
     {
         viewObj.frame = CGRectMake(0,50, viewObj.bounds.size.width, viewObj.bounds.size.height);
         [self.view addSubview:viewObj];
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Animation is complete");
     }];
}
-(IBAction)btngalary1click:(id)sender
{
    btngalary.hidden=NO;
    btngalary1.hidden=YES;
    viewObj.hidden=NO;

    
    [UIView animateWithDuration:0.10f delay:0.10f options:UIViewAnimationOptionTransitionFlipFromRight animations:^
     {
         viewObj.frame = CGRectMake(320,50, viewObj.bounds.size.width, viewObj.bounds.size.height);
     }
                     completion:^(BOOL finished)
     {
         NSLog(@"Animation is complete");
     }];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    btngalary.hidden=NO;
    btngalary1.hidden=YES;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   if(tableView==tblobj)
   {
    return [arrData count];
   }
   else if (tableView==storetabble)
   {
       return [image2 count];
   }
   return YES;
    
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView==storetabble)
    {
        
        NSString *cellIdentifre1 =@"storecell";
        
        storecell *cell1 =(storecell *) [self.storetabble dequeueReusableCellWithIdentifier:cellIdentifre1];
        
        if (cell1==nil)
        {
            NSArray *arrNib1=[[NSBundle mainBundle] loadNibNamed:cellIdentifre1 owner:self options:nil];
            cell1= (storecell *)[arrNib1 objectAtIndex:0];
            cell1.backgroundColor =[UIColor clearColor];
            cell1.selectionStyle=UITableViewCellSelectionStyleNone;
            
            //cell1.choose.tag=indexPath.row;
            //[cell1.choose addTarget:self action:@selector(btnDetailClick:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
        if (image2.count==0)
        {
            NSLog(@"no Image");
        }
        else
        {
            cell1.storeimg.image = [image2 objectAtIndex:indexPath.row];
        }
       
        
        return cell1;

}
    
    else if(tableView==tblobj)
    {
        NSString *cellIdentifre =@"SampleCell";
        
        SampleCell *cell =(SampleCell *) [self.tblobj dequeueReusableCellWithIdentifier:cellIdentifre];
        
        if (cell==nil)
        {
            NSArray *arrNib=[[NSBundle mainBundle] loadNibNamed:cellIdentifre owner:self options:nil];
            cell= (SampleCell *)[arrNib objectAtIndex:0];
            cell.backgroundColor =[UIColor clearColor];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
        }
        
        
        cell.labelForPlace.text=[arrData objectAtIndex:indexPath.row];
        
        return cell;
            }
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row==0)
    {
        viewObj.hidden=YES;
        btngalary.hidden=NO;
        btngalary1.hidden=YES;
        
    }
    else if (indexPath.row==1)
    {
        
        
    }
    else if (indexPath.row==2)
    {
        
        viewObj.hidden=YES;
        btngalary.hidden=NO;
        btngalary1.hidden=YES;
        
    }
    else if (indexPath.row==3)
    {
        
    }
    else if (indexPath.row==4)
    {
        [self.navigationController pushViewController:mypro animated:YES];
        
        
    }
    else if (indexPath.row==5)
    {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"uname"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"pass"];
        viewObj.hidden=YES;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    
}

-(IBAction)btnDetailClick:(id)sender
{
    
   // UIButton * btn = (UIButton *)sender;
   // [self.navigationController pushViewController:mainobj animated:YES];
}

-(IBAction)start:(id)sender
{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"uname"]==nil || [[NSUserDefaults standardUserDefaults] objectForKey:@"pass"]==nil)
    {
        [self.navigationController pushViewController:mainobj animated:YES];
    }
    else if ([[NSUserDefaults standardUserDefaults] objectForKey:@"uname"] || [[NSUserDefaults standardUserDefaults] objectForKey:@"pass"])
        
    {
         [self.navigationController pushViewController:styobj animated:YES];
        
    }
    NSUserDefaults *fetchDefaults = [NSUserDefaults standardUserDefaults];
    NSString *un = [fetchDefaults objectForKey:@"uname"];
    NSString *ps = [fetchDefaults objectForKey:@"pass"];
    if (un==nil || ps==nil)
    {
    }
    else
    {
        
    }


    
}

-(IBAction)btninfo:(id)sender
{
  [self.navigationController pushViewController:explobj animated:YES];

}
- (void)removeActivityView;
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    [[self class] cancelPreviousPerformRequestsWithTarget:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
