//
//  stepthree.h
//  Anywear
//
//  Created by Pradip on 8/14/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "confirm.h"
@class confirm;

@interface stepthree : UIViewController
{

//white
    
    UIButton *btnwhite1;
    UIButton *btnwhite2;
    UIButton *btnwhite3;
    UIButton *btnwhite4;
  
    
//pink
    UIButton *btnpink1;
    UIButton *btnpink2;
    UIButton *btnpink3;
    UIButton *btnpink4;
    
    
    confirm *cnfirmobj;
    
    //new
    NSMutableArray *storenext;
    NSMutableArray *lblstore;
    NSMutableArray *spendmoney;

    
    //newlbl
    UILabel *lbl1;
    UILabel *lbl2;
    UILabel *lbl3;
    UILabel *lbl4;
    
    
    UILabel *fid;
    
    NSString *b1;
    NSString *b2;
    NSString *b3;
    NSString *b4;
    
       
}


//white
@property(nonatomic,strong) IBOutlet UIButton *btnwhite1;
@property(nonatomic,strong) IBOutlet UIButton *btnwhite2;
@property(nonatomic,strong) IBOutlet UIButton *btnwhite3;
@property(nonatomic,strong) IBOutlet UIButton *btnwhite4;


//pink
@property(nonatomic,strong) IBOutlet UIButton *btnpink1;
@property(nonatomic,strong) IBOutlet UIButton *btnpink2;
@property(nonatomic,strong) IBOutlet UIButton *btnpink3;
@property(nonatomic,strong) IBOutlet UIButton *btnpink4;

@property(nonatomic,strong) confirm *cnfirmobj;



//white
-(IBAction)btnwhite1:(id)sender;
-(IBAction)btnwhite2:(id)sender;
-(IBAction)btnwhite3:(id)sender;
-(IBAction)btnwhite4:(id)sender;


//pink
-(IBAction)btnpink1:(id)sender;
-(IBAction)btnpink2:(id)sender;
-(IBAction)btnpink3:(id)sender;
-(IBAction)btnpink4:(id)sender;

-(IBAction)btnback:(id)sender;
-(IBAction)next:(id)sender;

//new
@property (strong, nonatomic) NSMutableArray *storenext;
@property (strong, nonatomic) NSMutableArray *lblstore;
@property (strong, nonatomic) NSMutableArray *spendmoney;



//newlbl
@property (strong, nonatomic) IBOutlet UILabel *lbl1;
@property (strong, nonatomic) IBOutlet UILabel *lbl2;
@property (strong, nonatomic) IBOutlet UILabel *lbl3;
@property (strong, nonatomic) IBOutlet UILabel *lbl4;



@property (strong, nonatomic) IBOutlet UILabel *fid;


@property (strong, nonatomic)  NSString *b1;
@property (strong, nonatomic)  NSString *b2;
@property (strong, nonatomic)  NSString *b3;
@property (strong, nonatomic)  NSString *b4;




@end
