//
//  exploreinfo.h
//  Anywear
//
//  Created by Pradip on 9/4/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

@interface exploreinfo : UIViewController<MFMessageComposeViewControllerDelegate>
{
    UITableView *tblinfo;
    NSMutableArray *infoarray;
    NSArray *thumbnails;
    UIView *aboutobj;
    UIView *faqobj;
    UIView *privacyobj;
    UIView *termsobj;


}
@property(nonatomic,strong)NSMutableArray *infoarray;
@property(nonatomic,strong) NSArray *thumbnails;
@property(nonatomic,strong)IBOutlet UITableView *tblinfo;
@property(nonatomic,strong)IBOutlet UIView *aboutobj;
@property(nonatomic,strong)IBOutlet UIView *faqobj;
@property(nonatomic,strong)IBOutlet UIView *privacyobj;
@property(nonatomic,strong)IBOutlet  UIView *termsobj;





-(IBAction)btnback:(id)sender;
-(IBAction)abtback:(id)sender;
-(IBAction)faqback:(id)sender;
-(IBAction)privacyback:(id)sender;
-(IBAction)termsback:(id)sender;


@end
