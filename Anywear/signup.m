//
//  signup.m
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 Pradip. All rights reserved.
//

#import "signup.h"
#import "Singleton.h"
#import "HttpQueue.h"
#import "NXJsonParser.h"
#import "DejalActivityView.h"
#import "ASIFormDataRequest.h"

@interface signup ()

@end

@implementation signup
@synthesize styleobj,imagPikar,imgPhoto,uname,passwrd,scroll,etype,tag,fbid1,imgtag,logobj;
@synthesize queue = _queue;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *name1=[[Singleton sharedSingleton] getFBname];
    NSLog(@"%@",name1);

    if (g_IS_IPHONE_4_SCREEN)
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Anywear is currently only available for women. Leave us your email and we will notify you once the app is available for men too! \n Email:__________________" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Ok", nil];
        // tag=0;
        
        [alert show];
   
        styleobj=[[style alloc]initWithNibName:@"style" bundle:nil];
         logobj=[[login alloc]initWithNibName:@"login" bundle:nil];
        
        imagPikar= [[UIImagePickerController alloc] init];
        imagPikar.delegate=self;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"8" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"7" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-8" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-7" object:nil];
        
        self.queue = [[NSOperationQueue alloc] init];
        
      
        
        self.imgPhoto.layer.cornerRadius = self.imgPhoto.frame.size.width / 2;
        self.imgPhoto.clipsToBounds = YES;

    }
    else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Anywear is currently only available for women. Leave us your email and we will notify you once the app is available for men too! \n Email:__________________" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: @"Ok", nil];
        // tag=0;
        
        [alert show];
        
        logobj=[[login alloc]initWithNibName:@"loginiphone" bundle:nil];

    
        styleobj=[[style alloc]initWithNibName:@"styleiphone" bundle:nil];
        imagPikar= [[UIImagePickerController alloc] init];
        imagPikar.delegate=self;
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"8" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallComplete:) name:@"8" object:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"-8" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerCallFail:) name:@"-8" object:nil];
        
        self.queue = [[NSOperationQueue alloc] init];
        
        

        self.imgPhoto.layer.cornerRadius = self.imgPhoto.frame.size.width / 2;
        self.imgPhoto.clipsToBounds = YES;

    
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *strtag=[[Singleton sharedSingleton]gettag];
    

    
    if ([strtag isEqualToString:@"1"])
    {
        uname.text=@"";
        passwrd.text=@"";
        [imgPhoto setImage:[UIImage imageNamed:@"add_photo-1.png"]];
    }
    else if([strtag isEqualToString:@"0"])
    {
    fbid1=[[Singleton sharedSingleton] getFBid];
    NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",fbid1];
    NSLog(@"User Image==%@",userImageURL);
    
    
    
    UIImage* myImage = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString:userImageURL]]];
    
    
        imgPhoto.image=myImage;
        uname.text=@"";
        passwrd.text=@"";
        NSString *name1=[[Singleton sharedSingleton] getFBname];
        NSLog(@"%@",name1);
        uname.text=name1;
            
    }
    else{}
    
    
    
    
    //imgPhoto=[[Singleton sharedSingleton]getMyImage];
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
   
   
    imgPhoto.image=image;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btnProPhoto:(id)sender
{
    [[Singleton sharedSingleton]settag:@"3"];
    imagPikar.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self  presentViewController:imagPikar animated:YES completion:nil];
   
    
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField==uname)
    {
        scroll.contentOffset=CGPointMake(0, 0);
    }
    else if(textField==passwrd)
        
    {
        scroll.contentOffset=CGPointMake(0, 50);
    }
    
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if(textField==uname)
    {
        [uname resignFirstResponder];
        scroll.contentOffset=CGPointMake(0, 0);
    }
    else if (textField==passwrd)
    {
        [passwrd resignFirstResponder];
        scroll.contentOffset=CGPointMake(0, 0);
    }
    return YES;
}

-(IBAction)btnback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
-(IBAction)signup:(id)sender
{
    UIView *viewToUse = self.view;
    viewToUse = self.navigationController.navigationBar.superview;
    [DejalBezelActivityView activityViewForView:viewToUse];
    
    NSString *fb=[[Singleton sharedSingleton]getMyfbcheck];
    if([fb isEqualToString:@"0"])
    {
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     NSString *user1 = self.uname.text;
     NSString *password1 = self.passwrd.text;
     [userDefaults setObject:user1 forKey:@"uname"];
     [userDefaults setObject:password1 forKey:@"pass"];
     [userDefaults synchronize];
       
   }
    
       if ([uname.text isEqualToString:@""] || uname.text==nil)
    {
        UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"User Name" message:@"Please Enter Your Email Id." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
        [alt1 show];
        tag=0;

        return;
    }
    
    if ([passwrd.text isEqualToString:@""] || passwrd.text==nil)
    {
        UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Password" message:@"Please Enter Your Password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
        [alt1 show];
        tag=0;

        return;
    }
    
    [[Singleton sharedSingleton] setMyPhotoImage:imgPhoto];
    [[Singleton sharedSingleton] setMyuname:uname.text];
    [[Singleton sharedSingleton] setMypasswrd:passwrd.text];
    
      
     NSString *strMyUrl=@"";
    
     strMyUrl =[NSString stringWithFormat:@"http://api.boutiqall.com/webservice?action=setUserRegistration"];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:@"http://api.boutiqall.com/webservice?action=setUserRegistration"]];
    
    NSData *imageData1 = UIImagePNGRepresentation([[Singleton sharedSingleton] getMyPhotoImage].image);
    NSLog(@"imageData = %@",imageData1);
    
     NSString *code=etype.text;
    
    request.tag =1;
	[request setUseKeychainPersistence:YES];
    [request addPostValue:[[Singleton sharedSingleton] getMyuname] forKey:@"vEmail"];
    [request addPostValue:[[Singleton sharedSingleton] getMypasswrd] forKey:@"vPassword"];
     [request addPostValue:code forKey:@"eType"];
    [request setData:imageData1 withFileName:@"myPhoto.png" andContentType:@"image/png" forKey:@"vImage"];
	[request setDelegate:self];
	[_queue addOperation:request];
    
   
}
- (void)requestFinished:(ASIHTTPRequest *)response {
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSDictionary *dictResult =(NSDictionary *)result;
    NSLog(@"re=%@",dictResult);
    UIAlertView *alt1=[[UIAlertView alloc] initWithTitle:@"Registration" message:@"Your Registation has been Completed Successfully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];

    [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
    [alt1 show];
    tag=1;

    return;
}
- (void)requestFailed:(ASIHTTPRequest *)response {
}

- (void) registerCallComplete : (NSNotification *)notification {
    
    NSDictionary* dict = [notification userInfo];
    ASIHTTPRequest *response = [dict objectForKey:@"index"];
    NXJsonParser* parser = [[NXJsonParser alloc] initWithData:[response responseData]];
    id result  = [parser parse:nil ignoreNulls:NO];
    
    NSDictionary *dictResult =(NSDictionary *)result;
    NSLog(@"re=%@",dictResult);
    
}

- (void) registerCallFail : (NSNotification *)notification {
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(tag==0)
    {
        if (buttonIndex==0)
        {
            
            
        }
    }
    else if(tag==1)
    {
        if (buttonIndex==0)
        {
            [self.navigationController pushViewController:styleobj animated:YES];
        }
    }
    
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData*)data
{
    NSLog(@"responseData: %@", data);
    
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",str);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    NSLog(@"Connection failed! Error - %@ %@",
          
          [error localizedDescription],
          
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Intenet Connection Error" message:@"Please Check Your Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [self performSelector:@selector(removeActivityView) withObject:nil afterDelay:1.0];
    [alert show];
    return;
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"hello1");
    
}
- (void)removeActivityView;
{
    [DejalBezelActivityView removeViewAnimated:YES];
    
    [[self class] cancelPreviousPerformRequestsWithTarget:self];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
