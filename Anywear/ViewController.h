//
//  ViewController.h
//  Anywear
//
//  Created by Pradip on 8/11/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Slider.h"
#import "style.h"


@interface ViewController : UIViewController
{
    Slider *sliderobj;
    UIView *splash;
    NSMutableData *responseData;
    NSMutableArray *myresult;
    NSString *suc;
    int tag;
    style *stobj1;
    
    
}

@property(nonatomic,strong) style *stobj1;

@property (nonatomic) int tag;
@property (nonatomic,strong) NSString *suc;
@property(nonatomic,strong) NSMutableArray *myresult;
@property(nonatomic,strong) NSMutableData *responseData;

@property(nonatomic,strong)   Slider *sliderobj;
@property(nonatomic,strong)   UIView *splash;





@end
